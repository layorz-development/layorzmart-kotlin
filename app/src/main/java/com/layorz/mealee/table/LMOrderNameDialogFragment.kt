package com.layorz.mealee.table

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMNewOrderEvent
import kotlinx.android.synthetic.main.lm_dialog_order_name.*
import org.greenrobot.eventbus.EventBus

class LMOrderNameDialogFragment : LMDialogFragment() {
    private var orderName: String? = null

    companion object {
        private const val ORDER_NAME = "order_name"
        fun newInstance(orderName: String?) = LMOrderNameDialogFragment().apply {
            arguments = bundleOf(ORDER_NAME to orderName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderName = it.getString(ORDER_NAME, null)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_dialog_order_name, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    private fun initView() {
        orderName?.let {
            tie_order_name.setText(it)
        }
    }

    private fun setListener() {
        tie_order_name.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(orderName: Editable?) {
                if (TextUtils.isEmpty(orderName?.toString()?.trim())) {
                    til_order_name.error = getString(R.string.order_name_empty)
                } else {
                    til_order_name.error = null
                }
            }
        })
        iv_cancel.setOnClickListener {
            dismiss()
        }
        btn_confirm_order.setOnClickListener {
            val orderName = tie_order_name.text?.toString()?.trim()
            if (TextUtils.isEmpty(orderName)) {
                til_order_name.error = getString(R.string.order_name_empty)
            } else {
                EventBus.getDefault().post(LMNewOrderEvent().apply {
                    newOrderName = orderName
                })
                dismiss()
            }
        }

    }
}