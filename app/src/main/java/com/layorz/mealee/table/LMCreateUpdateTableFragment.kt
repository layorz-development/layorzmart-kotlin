package com.layorz.mealee.table

import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.listener.LMOrderItemClickListener
import com.layorz.mealee.common.listener.LMSelectedTableItemListener
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMUtilise
import kotlinx.android.synthetic.main.lm_fragment_create_update_table.*

class LMCreateUpdateTableFragment : LMFragment(), LMOrderItemClickListener,
    LMSelectedTableItemListener {
    private val tableViewModel by lazy {
        ViewModelProvider(this).get(LMCreateUpdateTableViewModel::class.java)
    }

    private val tableList by lazy {
        mutableListOf<LMTableListData>()
    }

    private val tableItemAdapter by lazy {
        LMTableOrderItemAdapter(
            currentContext,
            tableItem = tableList, orderClickListener = this,
            selectedTableItemListener = this, tableOrOrder = true,
            isTableDeleteOptionNeeded = true
        )
    }

    companion object {
        private const val DELETE_TABLE_TAG = "delete_table"
        fun newInstance() = LMCreateUpdateTableFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_fragment_create_update_table, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun orderClick(orderName: String) {

    }

    override fun selectTable(
        tableDetail: LMTableListData,
        adapterPosition: Int
    ) {
        tableViewModel.deleteTableId = tableDetail.id
        LMAlertUtil.showAlertDialog(
            fragmentActivity = currentContext as FragmentActivity,
            message = getString(R.string.delete_alert) + tableDetail.tableName,
            title = getString(R.string.delete),
            isCancelNeeded = true,
            alertKey = DELETE_TABLE_TAG
        )
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == DELETE_TABLE_TAG) {
            tableViewModel.deleteTableId?.let { tableViewModel.deleteTable(it) }
        }
    }

    private fun initView() {
        setToolbar(getString(R.string.manage_table),resources.configuration.orientation== Configuration.ORIENTATION_PORTRAIT)
        rv_available_table.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(
                currentContext,
                3
            )
            adapter = tableItemAdapter
        }
    }

    private fun setListener() {
        btn_add_new_table.setOnClickListener {
            tableViewModel.canCreateTable()
        }
        tie_new_table_name.addTextChangedListener {
            tableViewModel.tableName = it.toString()
        }
    }

    private fun observerViewModel() {
        with(tableViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            tableAvailable.observe(viewLifecycleOwner, Observer {
                tableList.clear()
                tableList.addAll(it ?: mutableListOf())
                tableItemAdapter.notifyDataSetChanged()
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            clearTableName.observe(viewLifecycleOwner, Observer {
                it?.let {
                    LMUtilise.hideKeyboard(currentContext as Activity)
                    tie_new_table_name.apply {
                        text?.clear()
                        clearFocus()
                    }
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            tableNameErrorId.observe(viewLifecycleOwner, Observer {
                tv_order_name.apply {
                    this.isErrorEnabled = it?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            tableNameError.observe(viewLifecycleOwner, Observer {
                tv_order_name.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
        }
    }
}