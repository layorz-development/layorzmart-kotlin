package com.layorz.mealee.table

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMOrderItemClickListener
import com.layorz.mealee.common.listener.LMSelectedTableItemListener
import com.layorz.mealee.common.model.LMTableListData
import kotlinx.android.synthetic.main.lm_item_order.view.*
import kotlinx.android.synthetic.main.lm_item_table.view.*

class LMTableOrderItemAdapter(
    private val context: Context,
    private val tableItem: MutableList<LMTableListData> = mutableListOf(),
    private val orderItem: MutableList<String> = mutableListOf(),
    private val orderClickListener: LMOrderItemClickListener?,
    private val selectedTableItemListener: LMSelectedTableItemListener?,
    private val tableOrOrder: Boolean = false,
    private val isTableDeleteOptionNeeded: Boolean = true
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        if (tableOrOrder) {
            LMTableItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.lm_item_table, parent, false),
                selectedTableItemListener
            )
        } else {
            LMOrderItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.lm_item_order, parent, false),
                orderClickListener
            )
        }

    override fun getItemCount() = if (tableOrOrder) {
        tableItem.size
    } else {
        orderItem.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (tableOrOrder) {
            tableBindView(holder as? LMTableItemViewHolder)
        } else {
            orderBindView(holder as? LMOrderItemViewHolder)
        }
    }

    private fun tableBindView(tableItemViewHolder: LMTableItemViewHolder?) {
        tableItemViewHolder?.apply {
            tableItem[adapterPosition].apply {
                tvTableName.text = tableName
                ivDeleteTable.visibility =
                    if (isTableDeleteOptionNeeded) View.VISIBLE else View.GONE
                if (isTableDeleteOptionNeeded) {
                    ivDeleteTable.setOnClickListener {
                        tableItemListener?.selectTable(this, adapterPosition)
                    }
                } else {
                    clTableItem.background =
                        context.getDrawable(if (isSelected) R.drawable.bg_item_seletected else R.drawable.bg_field_background)
                    tvTableName.setTextColor(
                        ContextCompat.getColor(
                            context,
                            if (isSelected) R.color.colorPrimaryText else R.color.colorPrimary
                        )
                    )
                    clTableItem.setOnClickListener {
                        tableItemListener?.selectTable(this, adapterPosition)
                    }
                }
            }
        }
    }

    private fun orderBindView(orderItemViewHolder: LMOrderItemViewHolder?) {
        orderItemViewHolder?.apply {
            orderItem[adapterPosition].apply {
                tvOrderName.text = this
                clItemOrder.setOnClickListener {
                    orderClickListener?.orderClick(this)
                }
            }
        }
    }

    class LMTableItemViewHolder(
        itemView: View,
        val tableItemListener: LMSelectedTableItemListener?
    ) : RecyclerView.ViewHolder(itemView) {
        val ivDeleteTable: ImageView = itemView.iv_delete_table;
        val tvTableName: TextView = itemView.tv_table_name
        val clTableItem: ConstraintLayout = itemView.cl_table_item
    }

    class LMOrderItemViewHolder(
        itemView: View,
        val orderClickListener: LMOrderItemClickListener?
    ) : RecyclerView.ViewHolder(itemView) {
        val tvOrderName: TextView = itemView.tv_order_name
        val clItemOrder: ConstraintLayout = itemView.cl_item_order
    }
}