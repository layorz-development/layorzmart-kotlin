package com.layorz.mealee.table

import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.events.LMNewOrderEvent
import com.layorz.mealee.common.listener.LMOrderItemClickListener
import com.layorz.mealee.common.listener.LMSelectedTableItemListener
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.navigation.LMNavigationActivity
import kotlinx.android.synthetic.main.lm_fragment_table.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class LMTableFragment : LMFragment(), LMOrderItemClickListener, LMSelectedTableItemListener {
    private val tableViewModel by lazy {
        ViewModelProvider(this).get(LMTableViewModel::class.java)
    }
    private val tableList by lazy {
        mutableListOf<LMTableListData>()
    }
    private val orderList by lazy {
        mutableListOf<String>()
    }
    private val orderItemAdapter by lazy {
        LMTableOrderItemAdapter(
            currentContext,
            tableList, orderList,
            orderClickListener = this, selectedTableItemListener = this
        )
    }
    private val tableItemAdapter by lazy {
        LMTableOrderItemAdapter(
            currentContext,
            tableList, orderList, orderClickListener = this,
            selectedTableItemListener = this, tableOrOrder = true, isTableDeleteOptionNeeded = false
        )
    }

    companion object {
        private const val ORDER_ALREADY_EXIST = "order_already_exist"
        private const val NEW_ORDER = "new_order"
        private const val ORDER_DETAIL = "order_detail"
        fun newInstance() = LMTableFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun onResume() {
        super.onResume()
        tableViewModel.getAllOrderDetail()
    }

    override fun orderClick(orderName: String) {
        if (tableViewModel.tableSelected != null) {
            tableViewModel.currentOrderName = orderName
            showOrderActive(orderName)
        } else {
            Toast.makeText(currentContext, getString(R.string.please_any_table), Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun selectTable(
        tableDetail: LMTableListData,
        adapterPosition: Int
    ) {
        with(tableViewModel) {
            tableAvailable.value?.let { table ->
                if (adapterPosition != currentPosition) {
                    currentPosition?.let {
                        table[it].isSelected = !table[it].isSelected
                    }
                    table[adapterPosition].isSelected = !table[adapterPosition].isSelected
                    currentPosition = adapterPosition
                    tableSelected = tableDetail
                } else {
                    currentPosition?.let {
                        table[it].isSelected = !table[it].isSelected
                    }
                    tableSelected = null
                    currentPosition = null
                }
            }
            tableAvailable.value = tableAvailable.value
            getOrderAvailable()
        }
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        tableViewModel.currentOrderName?.let {
            if (alertKey == ORDER_ALREADY_EXIST) {
                showOrderActive(it)
            } else if (alertKey == ORDER_DETAIL) {
                createUpdateOrder(it, tableViewModel.tableSelected?.tableName, true)
            }
        }
    }

    override fun onNegativeButtonClick(alertKey: String) {
        super.onNegativeButtonClick(alertKey)
        tableViewModel.currentOrderName?.let {
            if (alertKey == ORDER_ALREADY_EXIST) {
                getOrderName(it)
            } else {
                createUpdateOrder(it, tableViewModel.tableSelected?.tableName, false)
            }
        }
    }

    private fun initView() {
        setToolbar(
            getString(R.string.take_order),
            resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
        )
        rv_available_order.apply {
            setHasFixedSize(true)
            adapter = orderItemAdapter
        }
        rv_available_table.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(
                currentContext,
                4
            )
            adapter = tableItemAdapter
        }
    }

    private fun setListener() {
        btn_add_new_order.setOnClickListener {
            if (tableViewModel.tableSelected != null) {
                getOrderName(null)
            } else {
                Toast.makeText(
                    currentContext,
                    getString(R.string.please_any_table),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun showOrderActive(orderName: String) {
        LMAlertUtil.showAlertDialog(
            fragmentActivity = currentContext as FragmentActivity,
            message = getString(R.string.order_action, orderName),
            title = getString(R.string.info),
            isCancelNeeded = true,
            positiveButtonText = getString(R.string.make_order),
            negativeButtonText = getString(R.string.view_order),
            alertKey = ORDER_DETAIL
        )
    }


    private fun createUpdateOrder(
        orderName: String?,
        tableName: String?,
        createMenu: Boolean
    ) {
        LMNavigation.switchActivityForResult(
            activity = currentContext as Activity,
            destination = LMNavigationActivity::class.java,
            requestCode = LMNavigationActivity.ORDER_OPERATION,
            bundle = bundleOf(
                LMNavigationActivity.NAVIGATION_TYPE to if (createMenu) LMNavigationActivity.CREATE_ORDER else LMNavigationActivity.UPDATE_ORDER,
                LMNavigationActivity.TABLE_DETAIL to tableName,
                LMNavigationActivity.ORDER_DETAIL to orderName
            )
        )
    }

    private fun observerViewModel() {
        with(tableViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            tableAvailable.observe(viewLifecycleOwner, Observer {
                tableList.clear()
                tableList.addAll(it ?: mutableListOf())
                tableItemAdapter.notifyDataSetChanged()
            })
            orderAvailable.observe(viewLifecycleOwner, Observer {
                orderList.clear()
                orderList.addAll(it ?: mutableListOf())
                iv_no_order.visibility = if (orderList.size > 0) View.GONE else View.VISIBLE
                orderItemAdapter.notifyDataSetChanged()
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun getOrderName(orderName: String?) {
        LMOrderNameDialogFragment.newInstance(orderName).show(parentFragmentManager, NEW_ORDER)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun getOrderName(orderDetail: LMNewOrderEvent) {
        orderDetail.newOrderName?.let {
            var isOrderExist = false
            for (order in tableViewModel.allOrderDetail) {
                if (order.toLowerCase(Locale.getDefault()) == it.toLowerCase(Locale.getDefault())) {
                    isOrderExist = true
                    break
                }
            }
            if (isOrderExist) {
                LMAlertUtil.showAlertDialog(
                    fragmentActivity = currentContext as FragmentActivity,
                    message = "\" $it \" ${getString(R.string.order_name_exist)}",
                    title = getString(R.string.already_exist),
                    isCancelNeeded = true,
                    alertKey = ORDER_ALREADY_EXIST
                )
                tableViewModel.currentOrderName = it
            } else {
                tableViewModel.currentOrderName = it
                createUpdateOrder(it, tableViewModel.tableSelected?.tableName, true)
            }
        }
    }
}