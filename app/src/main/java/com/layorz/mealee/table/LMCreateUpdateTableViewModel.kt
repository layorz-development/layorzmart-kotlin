package com.layorz.mealee.table

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMCreateTableRequest
import com.layorz.mealee.repository.LMTableRepository

class LMCreateUpdateTableViewModel(application: Application) : LMViewModel(application) {
    private val tableRepository by lazy {
        LMTableRepository
    }
    val tableAvailable by lazy {
        MutableLiveData<MutableList<LMTableListData>>()
    }
    val tableNameErrorId by lazy {
        MutableLiveData<Int>()
    }
    val tableNameError by lazy {
        MutableLiveData<String>()
    }
    val clearTableName by lazy {
        MutableLiveData<String>()
    }
    var deleteTableId: Int? = null
    var tableName: String? = null


    init {
        getTableAvailable()
    }

    private fun getTableAvailable() {
        isProgressShowing.value = true
        tableRepository.getAllTableList(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                tableAvailable.value = data as MutableList<LMTableListData>
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun canCreateTable() {
        tableNameErrorId.value = if (tableName != null && !TextUtils.isEmpty(tableName?.trim())) {
            createTable()
            null
        } else {
            R.string.table_name_empty
        }
    }

    private fun createTable() {
        isProgressShowing.value = true
        tableRepository.createTable(
            LMCreateTableRequest(tableName),
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    tableAvailable.value = data as MutableList<LMTableListData>
                    tableName = null
                    clearTableName.value = ""
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    val errors = errorMessage as? JsonObject
                    errors?.let {
                        if (errors.has("name")) {
                            tableNameError.value = errors.getAsJsonArray("name").get(0).toString()
                        }
                        isProgressShowing.value = false
                    }
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun deleteTable(tableId: Int) {
        isProgressShowing.value = true
        tableRepository.deleteTable(tableId, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                tableAvailable.value = data as MutableList<LMTableListData>
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}