package com.layorz.mealee.table

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.repository.LMPendingOrderRepository
import com.layorz.mealee.repository.LMTableRepository

class LMTableViewModel(application: Application) : LMViewModel(application) {
    private val pendingOrderRepository by lazy {
        LMPendingOrderRepository
    }
    private val tableRepository by lazy {
        LMTableRepository
    }
    val tableAvailable by lazy {
        MutableLiveData<MutableList<LMTableListData>>()
    }
    val allOrderDetail by lazy {
        mutableListOf<String>()
    }
    val orderAvailable by lazy {
        MutableLiveData<MutableList<String>>()
    }
    var tableName: String? = null
    var currentOrderName: String? = null
    var tableSelected: LMTableListData? = null
    var currentPosition: Int? = null

    fun getAllOrderDetail() {
        isProgressShowing.value = true
        pendingOrderRepository.getAllPendingOrderName(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                allOrderDetail.clear()
                allOrderDetail.addAll(data as MutableList<String>)
                getTableAvailable()
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getTableAvailable() {
        tableRepository.getAllTableList(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                tableAvailable.value = (data as MutableList<LMTableListData>).apply {
                    tableSelected?.let {
                        for (data in this) {
                            if (data.tableName == it.tableName) {
                                data.isSelected = true
                                break
                            }
                        }
                    }
                    getOrderAvailable()
                }
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getOrderAvailable() {
        isProgressShowing.value = true
        if (tableSelected?.tableName != null) {
            tableSelected?.tableName?.let {
                pendingOrderRepository.getAllPendingOrderName(it, object : LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        orderAvailable.value = data as MutableList<String>
                        isProgressShowing.value = false
                    }

                    override fun <T> onError(errorMessage: T) {
                        handleApiResponse(errorMessage as? String)
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                })
            }
        } else {
            orderAvailable.value = mutableListOf()
            isProgressShowing.value = false
        }
    }
}