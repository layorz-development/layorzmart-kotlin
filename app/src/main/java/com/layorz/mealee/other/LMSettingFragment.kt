package com.layorz.mealee.other

import android.app.Activity
import android.app.AlarmManager
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.model.LMSettingTimeData
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMLocaleUtil
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.dashboard.activities.LMDashboardActivity
import kotlinx.android.synthetic.main.lm_fragment_setting.*
import java.util.*

class LMSettingFragment : LMFragment() {
    private val settingViewModel by lazy { ViewModelProvider(this).get(LMSettingViewModel::class.java) }

    companion object {
        fun newInstance() = LMSettingFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        setToolbar(getString(R.string.setting),resources.configuration.orientation== Configuration.ORIENTATION_PORTRAIT)
        cl_app_setting.visibility =
            if (settingViewModel.appSetting) View.VISIBLE else View.GONE
        cl_report_setting.visibility =
            if (settingViewModel.changeReportGeneration) View.VISIBLE else View.GONE
        cl_menu_setting.visibility =
            if (settingViewModel.changeMenuSetting) View.VISIBLE else View.GONE
    }

    private fun setListener() {
        LMLocaleUtil.getLanguage(currentContext).let {
            rb_english.isChecked = it == LMConstant.LMLanguages.DEFAULT_LANGUAGE
            rb_tamil.isChecked = it == LMConstant.LMLanguages.TAMIL_LANGUAGE
        }
        sw_time_menu.setOnCheckedChangeListener { _, isChecked ->
            settingViewModel.menuTimingEvent.value = isChecked
            settingViewModel.saveMenuSetting(isChecked)
        }
        vw_morning_start.setOnClickListener {
            timePicker(
                tv_morning_starting,
                settingViewModel.menuTimingDataEvent.value?.morning,
                true
            )
        }
        vw_morning_ending.setOnClickListener {
            timePicker(
                tv_morning_ending,
                settingViewModel.menuTimingDataEvent.value?.morning,
                false
            )
        }
        vw_afternoon_start.setOnClickListener {
            timePicker(
                tv_afternoon_starting,
                settingViewModel.menuTimingDataEvent.value?.afternoon,
                true
            )
        }
        vw_afternoon_ending.setOnClickListener {
            timePicker(
                tv_afternoon_ending,
                settingViewModel.menuTimingDataEvent.value?.afternoon,
                false
            )
        }
        vw_evening_start.setOnClickListener {
            timePicker(
                tv_evening_starting,
                settingViewModel.menuTimingDataEvent.value?.evening,
                true
            )
        }
        vw_evening_ending.setOnClickListener {
            timePicker(
                tv_evening_ending,
                settingViewModel.menuTimingDataEvent.value?.evening,
                false
            )
        }
        vw_night_start.setOnClickListener {
            timePicker(
                tv_night_starting,
                settingViewModel.menuTimingDataEvent.value?.night,
                true
            )
        }
        vw_night_ending.setOnClickListener {
            timePicker(
                tv_night_ending,
                settingViewModel.menuTimingDataEvent.value?.night,
                false
            )
        }
        tv_schedule_time.setOnClickListener {
            setReportGenerateTime()
        }
        rg_language.setOnCheckedChangeListener { group, checkedId ->
            LMLocaleUtil.setLocale(
                currentContext as Activity,
                if (checkedId == R.id.rb_english) LMConstant.LMLanguages.DEFAULT_LANGUAGE else LMConstant.LMLanguages.TAMIL_LANGUAGE
            )
            LMNavigation.switchActivity(
                currentContext as Activity,
                LMDashboardActivity::class.java,
                flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            )
        }
    }

    private fun setReportGenerateTime() {
        val calendar = Calendar.getInstance().apply {
            LMApplication.getInstance().getPreference().reportGenerationTime?.let { it ->
                LMUtilise.localTime.parse(it)?.let {
                    time = it
                }
            }
        }
        TimePickerDialog(currentContext, OnTimeSetListener { _, hourOfDay, minute ->
            val railwayTimeData =
                "${if (hourOfDay == 0) 12 else if (hourOfDay > 12) if (hourOfDay % 12 >= 10) hourOfDay % 12 else "0${hourOfDay % 12}" else if (hourOfDay >= 10) hourOfDay else "0$hourOfDay"}:${if (minute < 10) "0$minute" else minute} ${getString(
                    if (hourOfDay >= 12) R.string.pm else R.string.am
                )}"
            tv_schedule_time.text = railwayTimeData
            settingViewModel.setReportGeneration(railwayTimeData)
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false).show()
    }

    private fun observerViewModel() {
        with(settingViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                /*srl_setting.isRefreshing = false*/
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let { Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show() }
            })

            reportGenerationEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    sw_time_report.isChecked = it
                    sw_time_report.text =
                        getString(if (it) R.string.switch_on else R.string.switch_off)
                    sw_time_report.setOnCheckedChangeListener { _, isChecked ->
                        settingViewModel.reportGenerationEvent.value = isChecked
                        if (isChecked) {
                            setReportGenerateTime()
                            settingViewModel.reportGenerationDataEvent.value =
                                LMApplication.getInstance().getPreference().reportGenerationTime
                                    ?: LMUtilise.getLocalTime(null)
                        } else {
                            settingViewModel.setReportGeneration(null)
                        }
                    }
                    mcv_report_generation.visibility = if (it) View.VISIBLE else View.GONE
                }
            })
            menuTimingEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    sw_time_menu.isChecked = it
                    sw_time_menu.text =
                        getString(if (it) R.string.switch_on else R.string.switch_off)
                    cl_menu_timing.visibility = if (it) View.VISIBLE else View.GONE
                }
            })
            reportGenerationDataEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    settingViewModel.setReportGeneration(it)
                    tv_schedule_time.text = it
                }
            })
            menuTimingDataEvent.observe(viewLifecycleOwner, Observer { settingDetail ->
                settingDetail?.let { detail ->
                    detail.morning.let {
                        tv_morning_starting.text =
                            if (!TextUtils.isEmpty(it?.from?.trim())) it?.from else "05.00 AM"
                        tv_morning_ending.text =
                            if (!TextUtils.isEmpty(it?.to?.trim())) it?.to else "12.00 PM"
                    }
                    detail.afternoon.let {
                        tv_afternoon_starting.text =
                            if (!TextUtils.isEmpty(it?.from?.trim())) it?.from else "12.00 PM"
                        tv_afternoon_ending.text =
                            if (!TextUtils.isEmpty(it?.to?.trim())) it?.to else "03.00 PM"
                    }
                    detail.evening.let {
                        tv_evening_starting.text =
                            if (!TextUtils.isEmpty(it?.from?.trim())) it?.from else "03.01 PM"
                        tv_evening_ending.text =
                            if (!TextUtils.isEmpty(it?.to?.trim())) it?.to else "07.00 PM"
                    }
                    detail.night.let {
                        tv_night_starting.text =
                            if (!TextUtils.isEmpty(it?.from?.trim())) it?.from else "07.01 PM"
                        tv_night_ending.text =
                            if (!TextUtils.isEmpty(it?.to?.trim())) it?.to else "11.00 PM"
                    }
                }
            })
            errorMessageEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            fieldValidation.observe(viewLifecycleOwner, Observer {
                if (it != null) {
                    for (error in it) {
                        when (error) {
                            "morning.from" -> setColor(vw_morning_start)
                            "morning.to" -> setColor(vw_morning_ending)
                            "afternoon.from" -> setColor(vw_afternoon_start)
                            "afternoon.to" -> setColor(vw_afternoon_ending)
                            "evening.from" -> setColor(vw_evening_start)
                            "evening.to" -> setColor(vw_evening_ending)
                            "night.from" -> setColor(vw_night_start)
                            "night.to" -> setColor(vw_night_ending)
                        }
                    }
                } else {
                    vw_morning_start.background = null
                    vw_morning_ending.background = null
                    vw_afternoon_start.background = null
                    vw_afternoon_ending.background = null
                    vw_evening_start.background = null
                    vw_evening_ending.background = null
                    vw_night_start.background = null
                    vw_night_ending.background = null
                }
            })
            alertReportGeneration.observe(viewLifecycleOwner, Observer {
                it?.let {
                    LMApplication.getInstance().getAlarmManager()
                        .cancel(LMApplication.getInstance().getAlarmPendingIntent());
                    LMApplication.getInstance().apply {
                        getAlarmManager().setInexactRepeating(
                            AlarmManager.RTC,
                            it,
                            AlarmManager.INTERVAL_DAY,
                            getAlarmPendingIntent()
                        )
                    }
                    alertReportGeneration.value = null
                }
            })
            cancelAlarmManager.observe(viewLifecycleOwner, Observer {
                it?.let {
                    LMApplication.getInstance().getAlarmManager()
                        .cancel(LMApplication.getInstance().getAlarmPendingIntent());
                    cancelAlarmManager.value = null
                }
            })
        }
    }

    private fun timePicker(
        setTimePlaceHolder: TextView,
        settingTimeData: LMSettingTimeData?,
        isStartTime: Boolean
    ) {
        val menuTiming = if (isStartTime) settingTimeData?.from else settingTimeData?.to
        TimePickerDialog(currentContext, OnTimeSetListener { _, hourOfDay, minute ->
            val railwayTimeData =
                "${if (hourOfDay == 0) 12 else if (hourOfDay > 12) if (hourOfDay % 12 >= 10) hourOfDay % 12 else "0${hourOfDay % 12}" else if (hourOfDay >= 10) hourOfDay else "0$hourOfDay"}:${if (minute < 10) "0$minute" else minute} ${getString(
                    if (hourOfDay >= 12) R.string.pm else R.string.am
                )}"
            setTimePlaceHolder.text = railwayTimeData
            if (isStartTime) {
                settingTimeData?.from = railwayTimeData
            } else {
                settingTimeData?.to = railwayTimeData
            }
            settingViewModel.saveMenuSetting(true)
        }, LMUtilise.getHourData(menuTiming), LMUtilise.getMinuteData(menuTiming), false).show()
    }

    private fun setColor(errorPosition: View) {
        errorPosition.background =
            ContextCompat.getDrawable(
                currentContext,
                R.drawable.bg_error_border
            )
    }
}