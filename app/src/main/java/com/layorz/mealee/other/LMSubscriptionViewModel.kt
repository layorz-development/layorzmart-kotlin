package com.layorz.mealee.other

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.response.LMUserResponse
import retrofit2.Call
import retrofit2.Response

class LMSubscriptionViewModel(application: Application):LMViewModel(application) {
    val navigationToDashboard by lazy { 
        MutableLiveData<Boolean>()
    }
    val showToast by lazy { 
        MutableLiveData<String>()
    }
    fun checkSubscription(){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().currentUser(object : LMRetrofitCallback<LMUserResponse> {
                override fun onNoNetwork() {
                    isProgressShowing.value=false
                  this@LMSubscriptionViewModel.showToast.value="You are offline"
                }

                override fun onFailure(call: Call<LMUserResponse>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<LMUserResponse>,
                    response: Response<LMUserResponse>
                ) {
                    if (response.isSuccessful){
                        var userData=response.body()
                        if (userData?.subscriptionStatus=="active"){
                            isProgressShowing.value=false
                            this@LMSubscriptionViewModel.showToast.value="Subscription active"
                            this@LMSubscriptionViewModel.navigationToDashboard.value=true
                        }else{
                            isProgressShowing.value=false
                            this@LMSubscriptionViewModel.showToast.value="Your subscription pack is currently not active"
                        }
                    }
                }
            })
        }
    }
}