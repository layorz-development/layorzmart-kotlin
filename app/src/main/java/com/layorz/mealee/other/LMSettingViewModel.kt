package com.layorz.mealee.other

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMSettingDetail
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.repository.LMSettingRepository
import java.util.*

class LMSettingViewModel(application: Application) : LMViewModel(application) {
    val fieldValidation by lazy { MutableLiveData<MutableList<String>>() }
    val errorMessageEvent by lazy { MutableLiveData<String>() }
    val reportGenerationEvent by lazy { MutableLiveData<Boolean>() }
    val reportGenerationDataEvent by lazy { MutableLiveData<String>() }
    val menuTimingEvent by lazy { MutableLiveData<Boolean>() }
    val menuTimingDataEvent by lazy { MutableLiveData<LMSettingDetail>() }
    val alertReportGeneration by lazy { MutableLiveData<Long>() }
    val cancelAlarmManager by lazy { MutableLiveData<String>() }
    private val userDetail by lazy { LMApplication.getInstance().getPreference().userData }
    var changeMenuSetting = false
    var changeReportGeneration = false
    var appSetting = false

    init {
        getReportGeneration()
        userDetail?.permissions?.let {
            appSetting = it.contains(LMConstant.CHANGE_LANGUAGE_SETTING)
            changeMenuSetting = it.contains(LMConstant.CHANGE_MENU_SETTING)
            changeReportGeneration = it.contains(LMConstant.CHANGE_REPORT_SETTING)
        }
        if(changeMenuSetting){
            getMenuSetting(true)
        }
    }

    fun getMenuSetting(isProgressbarNeed: Boolean) {
        if (isProgressbarNeed) {
            isProgressShowing.value = true
        }
        LMSettingRepository.getSettingTime(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                val timing = data as? LMSettingDetail
                isProgressShowing.value = false
                isPullToRefresh.value = false
                timing?.apply {
                    menuTimingEvent.value = timing.settingNeed
                    menuTimingDataEvent.value = timing
                }
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    private fun getReportGeneration() {
        val reportGenerationTime = LMApplication.getInstance().getPreference().reportGenerationTime
        reportGenerationEvent.value = reportGenerationTime != null
        reportGenerationDataEvent.value = reportGenerationTime
    }

    fun setReportGeneration(time: String?) {
        LMApplication.getInstance().getPreference().reportGenerationTime = time
        if (time != null) {
            val timeInMillisecond = Calendar.getInstance()
            timeInMillisecond.set(
                timeInMillisecond.get(Calendar.YEAR),
                timeInMillisecond.get(Calendar.MONTH),
                timeInMillisecond.get(Calendar.DAY_OF_MONTH),
                LMUtilise.getHourData(time),
                LMUtilise.getMinuteData(time),
                0
            )
            alertReportGeneration.value = timeInMillisecond.timeInMillis
        } else {
            cancelAlarmManager.value = " "
        }
    }

    fun saveMenuSetting(menuTimingNeed: Boolean) {
        menuTimingDataEvent.value?.let {
            isProgressShowing.value = true
            it.settingNeed = menuTimingNeed
            LMSettingRepository.createSettingTime(menuTimingDataEvent.value,
                object : LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        handleApiResponse((data as? LMCommonResponse)?.message)
                        isProgressShowing.value = false
                        isPullToRefresh.value = false
                        errorMessageEvent.value = null
                        fieldValidation.value = null
                    }

                    override fun <T> onError(errorMessage: T) {
                        val errors = errorMessage as? JsonObject
                        errors?.let {
                            val error by lazy { mutableListOf<String>() }
                            if (errors.has("morning.from")) {
                                error.add("morning.from")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("morning.from").get(0).toString()
                            }
                            if (errors.has("morning.to")) {
                                error.add("morning.to")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("morning.to").get(0).toString()
                            }
                            if (errors.has("afternoon.from")) {
                                error.add("afternoon.from")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("afternoon.from").get(0).toString()
                            }
                            if (errors.has("afternoon.to")) {
                                error.add("afternoon.to")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("afternoon.to").get(0).toString()
                            }
                            if (errors.has("evening.from")) {
                                error.add("evening.from")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("evening.from").get(0).toString()
                            }
                            if (errors.has("evening.to")) {
                                error.add("evening.to")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("evening.to").get(0).toString()
                            }
                            if (errors.has("night.from")) {
                                error.add("night.from")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("night.from").get(0).toString()
                            }
                            if (errors.has("night.to")) {
                                error.add("night.to")
                                errorMessageEvent.value =
                                    errors.getAsJsonArray("night.to").get(0).toString()
                            }
                            fieldValidation.value = error
                            isProgressShowing.value = false
                        }
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                })
        }
    }
}