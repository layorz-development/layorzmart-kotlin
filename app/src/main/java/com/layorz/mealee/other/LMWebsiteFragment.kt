package com.layorz.mealee.other

import android.content.res.Configuration
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.widget.LMWebView

class LMWebsiteFragment : LMFragment(), LMWebView.LMWebViewListener {
    private var wvWebsite: LMWebView? = null

    companion object {
        fun newInstance() = LMWebsiteFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.lm_fragment_website, container, false)
        updateProgress(true)
        wvWebsite = inflate.findViewById(R.id.wv_privacy_policy)
        wvWebsite?.setListener(this, this)
        wvWebsite?.loadUrl("https://mealee.layorz.com")
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onResume() {
        super.onResume()
        wvWebsite?.onResume()
    }

    override fun onPause() {
        super.onPause()
        wvWebsite?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        wvWebsite?.onDestroy()
    }

    private fun initView() {
        setToolbar(
            getString(R.string.website),
            resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
        )
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {

    }

    override fun onPageFinished(url: String?) {
        updateProgress(false)
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        updateProgress(false)
        Toast.makeText(currentContext, description, Toast.LENGTH_SHORT).show()
    }
}