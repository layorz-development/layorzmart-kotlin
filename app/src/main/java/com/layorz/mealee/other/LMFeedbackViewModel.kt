package com.layorz.mealee.other

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMFeedbackRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.repository.LMFeedbackRepository

class LMFeedbackViewModel(application: Application) : LMViewModel(application) {
    val dashboardScreen by lazy { MutableLiveData<Boolean>() }
    val subjectFieldError by lazy { MutableLiveData<Int>() }
    val subjectFieldApiError by lazy { MutableLiveData<String>() }
    val bugFieldError by lazy { MutableLiveData<Int>() }
    val descriptionFieldError by lazy { MutableLiveData<Int>() }
    val descriptionFieldApiError by lazy { MutableLiveData<String>() }
    var feedBackSubject: String? = null
    var feedBackBug: String? = null
    var feedBackDescription: String? = null
    var selectedPosition = 0

    fun sendFeedBack() {
        isProgressShowing.value = true
        var isAllFieldAreValid = true
        if (TextUtils.isEmpty(feedBackSubject?.trim())) {
            isAllFieldAreValid = false
            subjectFieldError.value = R.string.subject_empty
        } else {
            subjectFieldError.value = null
        }
        if (TextUtils.isEmpty(feedBackBug?.trim())) {
            isAllFieldAreValid = false
            bugFieldError.value = R.string.bug_empty
        } else {
            bugFieldError.value = null
        }
        if (TextUtils.isEmpty(feedBackDescription?.trim())) {
            isAllFieldAreValid = false
            descriptionFieldError.value = R.string.description_empty
        } else {
            descriptionFieldError.value = null
        }
        if (isAllFieldAreValid) {
            LMFeedbackRepository.createFeedBack(
                LMFeedbackRequest(
                    feedBackSubject,
                    feedBackBug,
                    feedBackDescription
                ), object :
                    LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        (data as? LMCommonResponse)?.message?.let {
                            dashboardScreen.value = true
                            handleApiResponse(it)
                        }
                        isProgressShowing.value = false
                    }

                    override fun <T> onError(errorMessage: T) {
                        val errors = errorMessage as? JsonObject
                        errors?.let {
                            if (errors.has("type")) {
                                apiResponseMessage.value =
                                    errors.getAsJsonArray("type").get(0).toString()
                            }
                            if (errors.has("description")) {
                                descriptionFieldApiError.value =
                                    errors.getAsJsonArray("description").get(0).toString()
                            }
                            if (errors.has("title")) {
                                subjectFieldApiError.value =
                                    errors.getAsJsonArray("title").get(0).toString()
                            }
                            isProgressShowing.value = false
                        }
                        if (errors == null) {
                            handleApiResponse(errorMessage as? String)
                        }
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                })
        } else {
            isProgressShowing.value = false
        }
    }
}