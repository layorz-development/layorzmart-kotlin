package com.layorz.mealee.other

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.dashboard.activities.LMDashboardActivity
import kotlinx.android.synthetic.main.lm_subscription_check_page.*

class LMSubscriptionActivity : LMActivity() {
    val subscriptionViewModel by lazy {
        ViewModelProvider(this).get(LMSubscriptionViewModel::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.lm_subscription_check_page)
        var company_id=LMApplication.getInstance().getPreference().userData?.branch?.id
        company_id_text.text=getString(R.string.company_id) + " $company_id"
        setListener()
        observeViewModel()
        super.onCreate(savedInstanceState)
    }
    private fun setListener(){
        btn_check_subs.setOnClickListener {
            subscriptionViewModel.checkSubscription()
        }
    }
    private fun observeViewModel(){
        with(subscriptionViewModel) {
            isProgressShowing.observe(this@LMSubscriptionActivity, Observer {
                updateProgressBar(it)
            })
            navigationToDashboard.observe(this@LMSubscriptionActivity, Observer {
                if (it){
                    LMNavigation.switchActivity(this@LMSubscriptionActivity,
                        LMDashboardActivity::class.java,
                        flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))

                }
            })
            showToast.observe(this@LMSubscriptionActivity, Observer {
                Toast.makeText(this@LMSubscriptionActivity,it,
                    Toast.LENGTH_LONG).show()
            })

        }
    }
}