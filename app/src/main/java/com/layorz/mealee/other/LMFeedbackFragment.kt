package com.layorz.mealee.other

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.events.LMBackPressEvent
import kotlinx.android.synthetic.main.lm_fragment_feedback.*
import org.greenrobot.eventbus.EventBus

class LMFeedbackFragment : LMFragment() {
    private val feedbackViewModel by lazy {
        ViewModelProvider(this).get(LMFeedbackViewModel::class.java);
    }

    companion object {
        fun newInstance() = LMFeedbackFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_feedback, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        setToolbar(
            getString(R.string.feedback_menu),
            resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
        )
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner_dropdown,
            resources.getStringArray(R.array.feedback_type).toList()
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_feedback_type.adapter = this
            acp_feedback_type.setSelection(feedbackViewModel.selectedPosition)
        }
    }

    private fun setListener() {
        tie_subject.addTextChangedListener {
            feedbackViewModel.feedBackSubject = it.toString().trim()
        }
        acp_feedback_type.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(feedbackViewModel) {
                            feedBackBug = if (position == 0) null else it
                            selectedPosition = position
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        tie_feedback_description.addTextChangedListener {
            feedbackViewModel.feedBackDescription = it.toString().trim()
        }
        btn_send_feedback.setOnClickListener {
            feedbackViewModel.sendFeedBack()
        }
    }

    private fun observerViewModel() {
        with(feedbackViewModel) {
            isProgressShowing.observe(
                viewLifecycleOwner,
                Observer { it?.let { updateProgress(it) } })
            apiResponseMessage.observe(
                viewLifecycleOwner,
                Observer {
                    it?.let {
                        Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                    }
                })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            dashboardScreen.observe(viewLifecycleOwner, Observer {
                if (it) {
                    EventBus.getDefault().post(LMBackPressEvent())
                }
            })
            subjectFieldError.observe(
                viewLifecycleOwner,
                Observer {
                    til_subject.apply {
                        this.isErrorEnabled = it?.let {
                            error = getString(it)
                            true
                        } ?: false
                    }
                })
            bugFieldError.observe(
                viewLifecycleOwner,
                Observer {
                    it?.let {
                        Toast.makeText(currentContext, getString(it), Toast.LENGTH_SHORT).show()
                    }
                })
            descriptionFieldError.observe(viewLifecycleOwner, Observer {
                til_feedback_description.apply {
                    this.isErrorEnabled = it?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            subjectFieldApiError.observe(viewLifecycleOwner, Observer {
                til_subject.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
            descriptionFieldApiError.observe(viewLifecycleOwner, Observer {
                til_feedback_description.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
        }
    }
}