package com.layorz.mealee.menu.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.listener.LMMenuImagePreviewListener
import com.layorz.mealee.common.listener.LMMenuItemClickListener
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_item_menu_data.view.*

class LMMenuAvailableAdapter(
    private val context: Context,
    private val menuAvailable: MutableList<LMMenuResponse>,
    private val menuClickListener: LMMenuItemClickListener,
    private val imagePreviewListener: LMMenuImagePreviewListener
) : RecyclerView.Adapter<LMMenuAvailableAdapter.LMMenuAvailableViewModel>() {

    private val userDetail = LMApplication.getInstance().getPreference().userData
    private val updateMenu = userDetail?.permissions?.contains(LMConstant.UPDATE_MENU) ?: false
    private val deleteMenu = userDetail?.permissions?.contains(LMConstant.DELETE_MENU) ?: false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LMMenuAvailableViewModel(
            LayoutInflater.from(parent.context).inflate(
                R.layout.lm_item_menu_data, parent, false
            ), menuClickListener, imagePreviewListener
        )

    override fun getItemCount() = menuAvailable.size

    override fun onBindViewHolder(holder: LMMenuAvailableViewModel, position: Int) {
        holder.apply {
            ivStatus.visibility = if (updateMenu) View.VISIBLE else View.INVISIBLE
            ivEditMenu.visibility = if (updateMenu) View.VISIBLE else View.INVISIBLE
            ivDeleteMenu.visibility = if (deleteMenu) View.VISIBLE else View.GONE
            menuAvailable.get(adapterPosition).apply {
                foodName?.let { foodName ->
                    tvMenuName.text = foodName
                    tvMenuPrice.text = "\u20B9 ${LMUtilise.priceFormat.format(price ?: 0F)}"
                    ivStatus.setImageResource(if (status) R.drawable.ic_visible else R.drawable.ic_not_visible)
                    Glide.with(context)
                        .load(itemPreviewUrl) // File of the picture
                        .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(16)))
                        .error(Glide.with(ivPreviewMenu).load(R.mipmap.ic_launcher))
                        .into(ivPreviewMenu)
                    ivEditMenu.setOnClickListener {
                        menuClickListener.menuItem(this, editButtonClicked = true)
                    }
                    ivDeleteMenu.setOnClickListener {
                        menuClickListener.menuItem(this, deleteButtonClicked = true)
                    }
                    ivStatus.setOnClickListener {
                        menuClickListener.statusChanged(this.also { status = !status },adapterPosition)
                    }
                    ivPreviewMenu.setOnClickListener {
                        itemPreviewUrl?.let {
                            imagePreviewListener.imageUrl(it)
                        }
                    }
                }
            }
        }
    }

    class LMMenuAvailableViewModel(
        itemView: View,
        val menuClickListener: LMMenuItemClickListener,
        val imagePreviewListener: LMMenuImagePreviewListener
    ) : RecyclerView.ViewHolder(itemView) {
        val tvMenuName: TextView = itemView.tv_menu_name
        val tvMenuPrice: TextView = itemView.tv_menu_price
        val ivPreviewMenu = itemView.iv_preview_menu
        val ivEditMenu: ImageView = itemView.iv_edit_menu
        val ivDeleteMenu: ImageView = itemView.iv_delete_menu
        val ivStatus: ImageView = itemView.iv_status
    }
}