package com.layorz.mealee.menu.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMMenuFilterData
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMFilterMenuRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import com.layorz.mealee.repository.LMMenuRepository
import java.util.*

class LMMenuAvailableViewModel(application: Application) : LMViewModel(application) {
    val menuTypeList by lazy {
        MutableLiveData<LMMenuFilterResponse>()
    }
    val menuAvailableList by lazy {
        MutableLiveData<MutableList<LMMenuResponse>>()
    }
    val menuVisibilityChanged by lazy {
        MutableLiveData<String>()
    }
    var allMenuAvailable: MutableList<LMMenuResponse>? = null
    var menuType: Int? = null
    var menuCategory: Int? = null
    var searchQuery: String = ""
    var menuStatusChangedPosition: Int? = null

    init {
        getMenuFilterType()
    }

    fun getMenuFilterType() {
        isProgressShowing.value = true
        LMMenuRepository.getMenuFilter(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuTypeList.value = data as LMMenuFilterResponse
                getFilterMenuAvailable(false)
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getFilterMenuAvailable(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMMenuRepository.getMenuList(LMFilterMenuRequest().apply {
            withMenuTimings = false
            type = if (menuType != -1) {
                menuType
            } else {
                null
            }
            category = if (menuCategory != -1) {
                menuCategory
            } else {
                null
            }
        },
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    allMenuAvailable = data as? MutableList<LMMenuResponse>
                    getFoodForMenu(searchQuery)
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as? String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun deleteMenuItem(menuId: Int) {
        isProgressShowing.value = true
        LMMenuRepository.deleteMenu(menuId, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                allMenuAvailable = data as? MutableList<LMMenuResponse>
                getFoodForMenu(searchQuery)
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun updateMenuItem(
        menuDetail: LMMenuResponse,
        position: Int
    ) {
        isProgressShowing.value = true
        menuDetail.id?.let {
            menuStatusChangedPosition = position
            LMMenuRepository.changeMenuStatus(
                it,
                menuDetail.status,
                object : LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        menuVisibilityChanged.value = ""
                        handleApiResponse((data as? LMCommonResponse)?.message)
                    }

                    override fun <T> onError(errorMessage: T) {
                        handleApiResponse(errorMessage as? String)
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                })
        }
    }

    fun getMenuDropDown(menuFilter: List<LMMenuFilterData>?) =
        mutableListOf<String>().apply {
            var loop = 0
            menuFilter?.let {
                while (loop < it.size) {
                    it[loop].name?.let { it1 -> add(it1) }
                    loop += 1
                }
            }
        }

    fun getMenuFilterId(
        filterType: String,
        filterData: List<LMMenuFilterData>?
    ): Int? {
        filterData?.let {
            var loop = 0
            while (loop < it.size) {
                it[loop].name?.let { name ->
                    if (name == filterType) {
                        return it[loop].id
                    }
                }
                loop += 1
            }
        }
        return null
    }

    fun getFoodForMenu(searchQuery: String) {
        this.searchQuery = searchQuery
        val filterData by lazy {
            mutableListOf<LMMenuResponse>()
        }
        allMenuAvailable?.let { it ->
            for (data in it) {
                data.foodName?.let { foodName ->
                    if (foodName.toLowerCase(Locale.getDefault()).contains(searchQuery)) {
                        filterData.add(data)
                    }
                }
            }
        }
        menuAvailableList.value = filterData
    }

    fun getPositionMenu(menuId: Int?, filterData: List<LMMenuFilterData>?): Int {
        menuId?.let { id ->
            var loop = 1
            filterData?.let {
                for (data in it) {
                    data.id?.let {
                        if (it == id) {
                            return loop
                        }
                    }
                    loop += 1
                }
            }
        }
        return 0
    }
}