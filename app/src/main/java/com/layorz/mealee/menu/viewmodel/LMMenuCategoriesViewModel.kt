package com.layorz.mealee.menu.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.request.LMMenuTypesRequest
import com.layorz.mealee.network.response.LMMenuCategoriesResponse
import retrofit2.Call
import retrofit2.Response

class LMMenuCategoriesViewModel(application: Application):LMViewModel(application){
    val categories by lazy {
        MutableLiveData<ArrayList<LMMenuCategoriesResponse>>()
    }
    var newCategoryName:String=""
    var id=0
    var name=""
    fun setDetail(id:Int,name:String){
        this.id=id
        this.name=name
    }
    fun editCategory(type_name:String){
        this.name=type_name

    }
    fun completeEditCategoryName(id:Int,name: LMMenuTypesRequest){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().editCategory(id,name,object :LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuCategoriesResponse>>, t: Throwable) {
                   handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuCategoriesResponse>>,
                    response: Response<ArrayList<LMMenuCategoriesResponse>>
                ) {
                    if(response.isSuccessful) {
                        var categories = response.body()
                        setCategories(categories!!)
                    }

                }
            })
        }

    }
    fun getCategories(){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().getMenuCategories(object :
                LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>> {
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuCategoriesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuCategoriesResponse>>,
                    response: Response<ArrayList<LMMenuCategoriesResponse>>
                ) {
                    if (response.isSuccessful){
                        var categories=response.body()
                        this@LMMenuCategoriesViewModel.categories.value=categories
                        setCategories(categories!!)

                    }
                }
            })
        }
    }
    fun setCategories(categories:ArrayList<LMMenuCategoriesResponse>){
        this.categories.value=categories
        isProgressShowing.value=false
    }
    fun deleteCategory(id: Int){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().deleteCategory(id,object :LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuCategoriesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuCategoriesResponse>>,
                    response: Response<ArrayList<LMMenuCategoriesResponse>>
                ) {
                    if (response.isSuccessful){
                        setCategories(response.body()!!)
                    }
                }
            })
        }
    }
    fun createCategory(name:String){
        isProgressShowing.value=true
        var menuCategoryRequest=LMMenuTypesRequest().apply {
            this.name=name
        }
        LMApplication.getInstance().apply {
            getApiManager().createCategory(menuCategoryRequest,object :LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuCategoriesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuCategoriesResponse>>,
                    response: Response<ArrayList<LMMenuCategoriesResponse>>
                ) {
                    if (response.isSuccessful){
                        setCategories(response.body()!!)
                    }
                }
            })
        }
    }

}