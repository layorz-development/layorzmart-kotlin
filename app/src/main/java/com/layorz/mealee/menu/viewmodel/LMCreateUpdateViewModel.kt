package com.layorz.mealee.menu.viewmodel

import android.app.Application
import android.text.TextUtils
import android.webkit.MimeTypeMap
import androidx.lifecycle.MutableLiveData
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMMenuFilterData
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import com.layorz.mealee.repository.LMMenuRepository
import java.io.File
import java.util.*

class LMCreateUpdateViewModel(application: Application) : LMViewModel(application) {
    private val TAG = LMCreateUpdateViewModel::class.java.simpleName
    val menuTitle by lazy {
        MutableLiveData<Int>()
    }
    val menuButton by lazy {
        MutableLiveData<Int>()
    }
    val menuFilter by lazy {
        MutableLiveData<LMMenuFilterResponse>()
    }
    val menuCreateUpdateEvent by lazy {
        MutableLiveData<Int>()
    }
    val foodNameErrorEvent by lazy {
        MutableLiveData<Int>()
    }
    val apiFoodNameErrorEvent by lazy {
        MutableLiveData<String>()
    }
    val apiPriceErrorEvent by lazy {
        MutableLiveData<String>()
    }
    val priceErrorEvent by lazy {
        MutableLiveData<Int>()
    }
    val additionalFieldErrorEvent by lazy {
        MutableLiveData<MutableList<Int>>()
    }
    var menuDetails: LMMenuResponse? = null
    var menuType: Boolean = true
    var menuCategory: Boolean = true
    var id: Int? = null
    var newMenuType: String? = null
    var newMenuCategory: String? = null

    fun getMenuTypeCategory(menuDetail: LMMenuResponse) {
        isProgressShowing.value = true
        this.menuDetails = menuDetail
        this.menuDetails?.type = menuDetail.type
        this.menuDetails?.category = menuDetail.category
        menuTitle.value = menuDetail.id?.let {
            R.string.update_menu
        } ?: R.string.create_menu
        menuButton.value = menuDetail.id?.let {
            R.string.save
        } ?: R.string.add_menu
        menuType = menuDetail.type.id == null
        menuCategory = menuDetail.category.id == null
        LMMenuRepository.getMenuFilter(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuFilter.value = data as LMMenuFilterResponse
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    private fun createNewMenu(menuDetail: LMMenuResponse) {
        LMMenuRepository.createMenu(menuDetail, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuCreateUpdateEvent.value = R.string.create_menu_successfully
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("food_name")) {
                        apiFoodNameErrorEvent.value =
                            errors.getAsJsonArray("food_name").get(0).toString()
                    }
                    if (errors.has("category")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("password").get(0).toString()
                    }
                    if (errors.has("description")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("description").get(0).toString()
                    }
                    if (errors.has("price")) {
                        apiPriceErrorEvent.value =
                            errors.getAsJsonArray("price").get(0).toString()
                    }
                    if (errors.has("image")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("image").get(0).toString()
                    }
                    if (errors.has("time_of_day")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("time_of_day").get(0).toString()
                    }
                    isProgressShowing.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    private fun updateExistMenu(menuId: Int, menuDetail: LMMenuResponse) {
        LMMenuRepository.updateMenuDetail(menuId, menuDetail, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuCreateUpdateEvent.value = R.string.update_menu_successfully
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("food_name")) {
                        apiFoodNameErrorEvent.value =
                            errors.getAsJsonArray("food_name").get(0).toString()
                    }
                    if (errors.has("category")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("password").get(0).toString()
                    }
                    if (errors.has("description")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("description").get(0).toString()
                    }
                    if (errors.has("price")) {
                        apiPriceErrorEvent.value =
                            errors.getAsJsonArray("price").get(0).toString()
                    }
                    if (errors.has("image")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("image").get(0).toString()
                    }
                    if (errors.has("time_of_day")) {
                        apiResponseMessage.value =
                            errors.getAsJsonArray("time_of_day").get(0).toString()
                    }
                    isProgressShowing.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getMenuDropDown(menuFilter: List<LMMenuFilterData>?) =
        mutableListOf<String>().apply {
            var loop = 0
            menuFilter?.let {
                while (loop < it.size) {
                    it[loop].name?.let { add(it) }
                    loop += 1
                }
            }
        }

    fun getMenuFilterId(
        filterType: String,
        filterData: List<LMMenuFilterData>?
    ): LMMenuFilterData {
        filterData?.let {
            var loop = 1
            for (data in it) {
                data.name?.let {
                    if (filterType == it) {
                        return data
                    }
                }
                loop += 1
            }
        }
        return LMMenuFilterData()
    }

    fun getPositionMenu(menuId: Int?, filterData: List<LMMenuFilterData>?): Int {
        menuId?.let { id ->
            var loop = 1
            filterData?.let {
                for (data in it) {
                    data.id?.let {
                        if (it == id) {
                            return loop
                        }
                    }
                    loop += 1
                }
            }
        }
        return 0
    }

    fun checkCreateUpdateMenu() {
        var validate = true
        val addFieldEmpty by lazy { mutableListOf<Int>() }
        val apiData = LMMenuResponse().apply {
            menuDetails?.let {
                if (TextUtils.isEmpty(it.foodName?.trim())) {
                    validate = false
                    foodNameErrorEvent.value = R.string.food_name_empty
                } else {
                    foodNameErrorEvent.value = null
                    foodName = it.foodName
                }
                if (TextUtils.isEmpty(it.price.toString().trim())) {
                    validate = false
                    priceErrorEvent.value = R.string.price_empty
                } else {
                    priceErrorEvent.value = null
                    price = it.price
                }
                if (it.timeOfDay.night || it.timeOfDay.evening || it.timeOfDay.afternoon || it.timeOfDay.morning) {
                    timeOfDay = it.timeOfDay
                } else {
                    validate = false
                    addFieldEmpty.add(R.string.time_empty)
                }
                description = it.description
                if (menuType) {
                    if (TextUtils.isEmpty(newMenuType?.trim())) {
                        validate = false
                        addFieldEmpty.add(R.string.new_type_empty)
                    } else {
                        type = LMMenuFilterData().apply { name = newMenuType }
                    }
                } else {
                    if (it.type.id != null) {
                        type = it.type
                    } else {
                        validate = false
                        addFieldEmpty.add(R.string.existing_type_empty)
                    }
                }
                if (menuCategory) {
                    if (TextUtils.isEmpty(newMenuCategory?.trim())) {
                        validate = false
                        addFieldEmpty.add(R.string.new_category_empty)
                    } else {
                        category = LMMenuFilterData().apply {
                            name = newMenuCategory
                        }
                    }
                } else {
                    if (it.category.id != null) {
                        category = it.category
                    } else {
                        validate = false
                        addFieldEmpty.add(R.string.existing_category_empty)
                    }
                }
                it.id?.let { id = it }
                itemPreviewUrl = it.itemPreviewUrl
            }
        }
        if (validate) {
            isProgressShowing.value = true
            apiData.apply {
                val apiDataId = id
                if (itemPreviewUrl != null) {
                    itemPreviewUrl?.let {
                        if (!(it.startsWith(LMConstant.HTTPS) || TextUtils.isEmpty(it.trim()))) {
                            val file = File(it)
                            val instance = LMApplication.getInstance()
                            val userData = instance.getPreference().userData
                            val fileName =
                                userData?.name + "_" + userData?.company + "_" + Calendar.getInstance().timeInMillis.toString() + "." + MimeTypeMap.getFileExtensionFromUrl(
                                    file.name
                                )
                            val upload = instance.getS3UploadManager()?.upload(
                                instance.getBucketName(),
                                fileName,
                                file,
                                CannedAccessControlList.PublicRead
                            )
                            if (upload != null) {
                                upload.setTransferListener(object : TransferListener {
                                    override fun onStateChanged(
                                        id: Int,
                                        newState: TransferState
                                    ) {
                                        if (LMApplication.isNetworkConnected()) {
                                            if (newState == TransferState.COMPLETED) {
                                                itemPreviewUrl = fileName
                                                createUpdateMenuDetail(apiDataId, apiData)
                                            } else if (newState == TransferState.FAILED) {
                                                handleApiResponse(R.string.upload_failed)
                                            } else if (newState == TransferState.WAITING_FOR_NETWORK) {
                                                instance.getS3UploadManager()?.cancel(id)
                                                handleApiResponse(R.string.network_problem)
                                            }
                                        } else {
                                            showNoNetworkToast()
                                        }
                                    }

                                    override fun onProgressChanged(
                                        id: Int,
                                        bytesCurrent: Long,
                                        bytesTotal: Long
                                    ) {
                                        LMLog.d(TAG, "image")
                                    }

                                    override fun onError(id: Int, exception: Exception) {
                                        handleApiResponse(exception.message.toString())
                                    }
                                })
                            } else {
                                apiResponseInId.value = R.string.upload_failed
                                createUpdateMenuDetail(apiDataId, apiData)
                            }
                        } else {
                            createUpdateMenuDetail(apiDataId, apiData)
                        }
                    }
                } else {
                    createUpdateMenuDetail(apiDataId, apiData)
                }
            }
        } else {
            if (addFieldEmpty.isNotEmpty()) {
                additionalFieldErrorEvent.value = addFieldEmpty
            } else {
                additionalFieldErrorEvent.value = null
            }
        }
    }

    private fun createUpdateMenuDetail(
        apiDataId: Int?,
        apiData: LMMenuResponse
    ) {
        if (apiDataId != null) {
            apiData.itemPreviewUrl?.let {
                if (it.startsWith(LMConstant.HTTPS)) {
                    apiData.itemPreviewUrl = null
                }
            }
            updateExistMenu(apiDataId, apiData)
        } else {
            createNewMenu(apiData)
        }
    }

    fun setImageUrl(imagePath: String?) {
        menuDetails?.itemPreviewUrl = imagePath
    }
}