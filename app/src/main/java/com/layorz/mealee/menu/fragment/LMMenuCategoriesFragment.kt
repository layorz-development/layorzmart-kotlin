package com.layorz.mealee.menu.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.events.LMAlertButtonClick
import com.layorz.mealee.common.events.LMEditTypeNameEvent
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.menu.adapter.LMMenuCategoriesAdapter
import com.layorz.mealee.menu.viewmodel.LMMenuCategoriesViewModel
import com.layorz.mealee.network.request.LMMenuTypesRequest
import kotlinx.android.synthetic.main.lm_fragment_menu_categories.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LMMenuCategoriesFragment : LMFragment() {
    private val menuCategoriesViewModel by lazy {
        ViewModelProvider(this).get(LMMenuCategoriesViewModel::class.java)
    }
    companion object{
        fun newInstance()=
            LMMenuCategoriesFragment()
    }
    var isDeleteVisible:Boolean=false
    var isEditVisible=false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setToolbar("Manage Categories",true)
        var view=inflater.inflate(R.layout.lm_fragment_menu_categories, container, false)
        menuCategoriesViewModel.getCategories()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
        setListener()
        checkPermissions()
    }
    private fun observeViewModel(){
        with(menuCategoriesViewModel) {
            categories.observe(viewLifecycleOwner, Observer {
                if (it.size>0) {
                    iv_no_data.visibility=View.GONE
                }else{
                    iv_no_data.visibility=View.VISIBLE
                }
                menu_categories_recycler_view.adapter =
                        LMMenuCategoriesAdapter(
                            requireContext(),
                            it!!,
                            parentFragmentManager,
                            menuCategoriesViewModel,isDeleteVisible,isEditVisible
                        )
                    menu_categories_recycler_view.layoutManager =
                        LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                    tie_new_category_name.setText("")
            })
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })

        }
    }
    fun setListener(){
        btn_add_new_category.setOnClickListener {
            menuCategoriesViewModel.createCategory(menuCategoriesViewModel.newCategoryName)
        }
        tie_new_category_name.addTextChangedListener {
            menuCategoriesViewModel.newCategoryName=it.toString()
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun editTypeName(editTypeEvent: LMEditTypeNameEvent){
        var request= LMMenuTypesRequest().apply {
            name=editTypeEvent.name
        }
        menuCategoriesViewModel.completeEditCategoryName(editTypeEvent.id!!,request)
    }
    fun checkPermissions(){
        var permissions= LMApplication.getInstance().getPreference().userData?.permissions
        permissions.apply {
            if (this?.contains(LMConstant.CREATE_FILTER)!!){
                tv_order_name.visibility=View.VISIBLE
            }
            if (this.contains(LMConstant.CREATE_FILTER)){
                btn_add_new_category.visibility=View.VISIBLE
            }
            if(this.contains(LMConstant.DELETE_FILTER))
                isDeleteVisible=true
            if (this.contains(LMConstant.UPDATE_FILTER))
                isEditVisible=true
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDeleteCAtegory(alertButtonClickEvent: LMAlertButtonClick){
        var id=alertButtonClickEvent.alertKey.toInt()
        val buttonType: String = alertButtonClickEvent.buttonType
        if (LMConstant.ALERT_POSITIVE_BUTTON == buttonType) {
           menuCategoriesViewModel.deleteCategory(id)
        } else if (LMConstant.ALERT_NEGATIVE_BUTTON == buttonType) {
            onNegativeButtonClick(alertButtonClickEvent.alertKey)
        }
    }

}