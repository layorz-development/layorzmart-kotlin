package com.layorz.mealee.menu.dialogFragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import kotlinx.android.synthetic.main.lm_dialog_menu_image.*

class LMMenuImageDialogFragment : LMDialogFragment() {
    private lateinit var imageUrl: String

    companion object {
        private const val IMAGE_URL = "order_name"
        fun newInstance(imageUrl: String) = LMMenuImageDialogFragment()
            .apply {
            arguments = bundleOf(IMAGE_URL to imageUrl)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            imageUrl = getString(IMAGE_URL, null)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(currentContext).apply {
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_dialog_menu_image, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        Glide.with(currentContext)
            .load(imageUrl) // File of the picture
            .apply(RequestOptions().transform(RoundedCorners(16)))
            .error(Glide.with(iv_photo_image).load(R.mipmap.ic_launcher))
            .into(iv_photo_image)
    }
}