package com.layorz.mealee.menu.dialogFragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMEditTypeNameEvent
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.menu.viewmodel.LMMenuTypesViewModel
import kotlinx.android.synthetic.main.lm_dialog_complete_order.iv_close
import kotlinx.android.synthetic.main.lm_dialog_menu_types.*
import org.greenrobot.eventbus.EventBus

class LMMenuTypesDialogFragment:LMDialogFragment() {
    private val menuTypesViewModel by lazy {
        ViewModelProvider(this).get(LMMenuTypesViewModel::class.java)
    }

    companion object{
        private const val ID = "id"
        private const val NAME = "name"
        fun newInstance(id:Int,type_mame:String)=
            LMMenuTypesDialogFragment().apply {
                arguments= bundleOf(
                  ID to id,
                    NAME to type_mame
                )
            }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            menuTypesViewModel.setDetail(
              getInt(ID),
                getString(NAME)!!

            )
        }
    }
    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(currentContext).apply {
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.lm_dialog_menu_types, container, false)
        var name = view.findViewById<TextView>(R.id.et_name)
       name.setText(menuTypesViewModel.name.toString())
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        LMUtilise.hideKeyboard(currentContext as Activity)
    }

    private fun setListener() {
        et_name.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                if (it.toString().length < 10)
                    menuTypesViewModel.editType(it.toString())
            }
        }

        et_name.setOnClickListener {
            et_name.isFocusableInTouchMode = true;
            et_name.isFocusable = true
            val inputMethodManager =
                currentContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(et_name, InputMethodManager.SHOW_IMPLICIT)
            et_name.requestFocus()
        }
        iv_close.setOnClickListener {
            dismiss()
        }
        edit_btn.setOnClickListener {
            dismiss()
            EventBus.getDefault().post(LMEditTypeNameEvent(
                menuTypesViewModel.id,
                menuTypesViewModel.name
            ))
        }
    }

    private fun observerViewModel() {
        with(menuTypesViewModel) {
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(
                        currentContext,
                        getString(it),
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    android.widget.Toast.makeText(currentContext, it, android.widget.Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}