package com.layorz.mealee.menu.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.menu.viewmodel.LMCreateUpdateViewModel
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_fragment_new_menu.*
import kotlinx.android.synthetic.main.lm_item_image_preview.*
import java.io.File

class LMCreateEditMenuFragment : LMPermissionFragment() {
    private val createUpdateViewModel by lazy {
        ViewModelProvider(this).get(LMCreateUpdateViewModel::class.java)
    }

    companion object {
        private const val MENU_DETAIL = "menu_detail"
        fun newInstance(menuDetail: String?) = LMCreateEditMenuFragment().apply {
            arguments = bundleOf(MENU_DETAIL to menuDetail)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            arguments?.apply {
                createUpdateViewModel.getMenuTypeCategory(
                    Gson().fromJson(
                        getString(MENU_DETAIL),
                        LMMenuResponse::class.java
                    )
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_new_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setListener()
        observeViewModel()
    }

    override fun imageUrl(imagePath: String) {
        if (!TextUtils.isEmpty(imagePath)) {
            if (imagePath.startsWith(LMConstant.HTTPS)) {
                setImage(imagePath)
            } else {
                createUpdateViewModel.setImageUrl(imagePath)
                val imgFile = File(imagePath)
                if (imgFile.exists()) {
                    setImage(imgFile)
                }
            }
        }
    }

    override fun completeFragement() {}

    private fun initView() {
        with(createUpdateViewModel)
        {
            menuDetails?.apply {
                foodName?.let {
                    tie_food_name.setText(it)
                }
                price.let {
                    if (it != 0) {
                        tie_food_price.setText(it.toString())
                    }
                }
                description?.let {
                    et_description.setText(it)
                }
                timeOfDay.let {
                    cb_morning.isChecked = it.morning
                    cb_afternoon.isChecked = it.afternoon
                    cb_evening.isChecked = it.evening
                    cb_night.isChecked = it.night
                }
                type.name?.let {
                    et_new_menu_type.setText(it)
                }
                category.name?.let {
                    et_new_menu_category.setText(it)
                }
                itemPreviewUrl?.let {
                    setImage(it)
                }
            }
            if (menuType) {
                rb_new_menu_type.isChecked = true
                et_new_menu_type.visibility = View.VISIBLE
                acp_menu_exist_type.visibility = View.GONE
            } else {
                rb_already_exist_type.isChecked = true
                et_new_menu_type.visibility = View.GONE
                acp_menu_exist_type.visibility = View.VISIBLE
            }
            if (menuCategory) {
                rb_new_menu_category.isChecked = true
                et_new_menu_category.visibility = View.VISIBLE
                acp_menu_exist_cateroy.visibility = View.GONE
            } else {
                rb_already_exist_category.isChecked = true
                et_new_menu_category.visibility = View.GONE
                acp_menu_exist_cateroy.visibility = View.VISIBLE
            }
        }
    }

    private fun setListener() {
        btn_create_edit_menu.setOnClickListener {
            createUpdateViewModel.checkCreateUpdateMenu()
        }
        rg_menu_type.setOnCheckedChangeListener { _, checkedId ->
            with(createUpdateViewModel) {
                menuType = checkedId == R.id.rb_new_menu_type
                if (menuType) {
                    et_new_menu_type.setText(newMenuType)
                    et_new_menu_type.visibility = View.VISIBLE
                    acp_menu_exist_type.visibility = View.GONE
                } else {
                    acp_menu_exist_type.setSelection(
                        getPositionMenu(
                            menuDetails?.type?.id,
                            menuFilter.value?.type
                        )
                    )
                    et_new_menu_type.visibility = View.GONE
                    acp_menu_exist_type.visibility = View.VISIBLE
                }
            }
        }
        rg_menu_category.setOnCheckedChangeListener { _, checkedId ->
            with(createUpdateViewModel) {
                menuCategory = checkedId == R.id.rb_new_menu_category
                if (menuCategory) {
                    et_new_menu_category.setText(newMenuCategory)
                    et_new_menu_category.visibility = View.VISIBLE
                    acp_menu_exist_cateroy.visibility = View.GONE
                } else {
                    acp_menu_exist_cateroy.setSelection(
                        getPositionMenu(
                            menuDetails?.category?.id,
                            menuFilter.value?.category
                        )
                    )
                    et_new_menu_category.visibility = View.GONE
                    acp_menu_exist_cateroy.visibility = View.VISIBLE
                }
            }
        }
        tie_food_name.addTextChangedListener { foodName ->
            createUpdateViewModel.menuDetails?.foodName = foodName?.toString()?.trim()
        }
        tie_food_price.addTextChangedListener { foodPrice ->
            val prise = foodPrice.toString().trim()
            if (!TextUtils.isEmpty(prise)) {
                createUpdateViewModel.menuDetails?.price = prise.toInt()
            }
        }
        et_description.addTextChangedListener { description ->
            createUpdateViewModel.menuDetails?.description = description?.toString()?.trim()
        }
        et_new_menu_type.addTextChangedListener { typeName ->
            createUpdateViewModel.newMenuType = typeName?.toString()?.trim()
        }
        et_new_menu_category.addTextChangedListener { categoryName ->
            createUpdateViewModel.newMenuCategory = categoryName?.toString()?.trim()
        }
        cb_morning.setOnCheckedChangeListener { _, isChecked ->
            createUpdateViewModel.menuDetails?.timeOfDay?.morning = isChecked
        }
        cb_afternoon.setOnCheckedChangeListener { _, isChecked ->
            createUpdateViewModel.menuDetails?.timeOfDay?.afternoon = isChecked
        }
        cb_evening.setOnCheckedChangeListener { _, isChecked ->
            createUpdateViewModel.menuDetails?.timeOfDay?.evening = isChecked
        }
        cb_night.setOnCheckedChangeListener { _, isChecked ->
            createUpdateViewModel.menuDetails?.timeOfDay?.night = isChecked
        }
        acp_menu_exist_type.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(createUpdateViewModel) {
                            val menuFilterId = getMenuFilterId(it, menuFilter.value?.type)
                            menuFilterId.id?.let {
                                menuDetails?.type = menuFilterId
                            }
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        acp_menu_exist_cateroy.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(createUpdateViewModel) {
                            val menuFilterId = getMenuFilterId(it, menuFilter.value?.category)
                            menuFilterId.id?.let {
                                menuDetails?.category = menuFilterId
                            }
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        cl_add_photograph.setOnClickListener {
            pickImage()
        }
        iv_cancel.setOnClickListener {
            createUpdateViewModel.setImageUrl("")
            cl_add_photograph.visibility = View.VISIBLE
            cl_image_view.visibility = View.GONE
        }
    }

    private fun observeViewModel() {
        with(createUpdateViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            menuCreateUpdateEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_SHORT).show()
                    (currentContext as Activity).apply {
                        setResult(Activity.RESULT_OK, Intent())
                        finish()
                    }
                }
            })
            menuTitle.observe(viewLifecycleOwner, Observer {
                it?.let {
                    setToolbar(getString(it))
                }
            })
            menuButton.observe(viewLifecycleOwner, Observer {
                it?.let {
                    btn_create_edit_menu.text = getString(it)
                }
            })
            foodNameErrorEvent.observe(viewLifecycleOwner, Observer {
                til_food_name.apply {
                    this.isErrorEnabled = it?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            apiFoodNameErrorEvent.observe(viewLifecycleOwner, Observer {
                til_food_name.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
            priceErrorEvent.observe(viewLifecycleOwner, Observer {
                til_food_price.apply {
                    this.isErrorEnabled = it?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            apiPriceErrorEvent.observe(viewLifecycleOwner, Observer {
                til_food_price.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
            additionalFieldErrorEvent.observe(viewLifecycleOwner, Observer {
                it?.let {
                    var errorMessage: String? = null
                    for (error in it) {
                        errorMessage = errorMessage?.let { it + ", " + getString(error) }
                            ?: getString(error)
                    }
                    Toast.makeText(currentContext, errorMessage, Toast.LENGTH_LONG).show()
                }
            })
            menuFilter.observe(viewLifecycleOwner, Observer {
                ArrayAdapter(
                    currentContext,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(getString(R.string.select))
                        addAll(createUpdateViewModel.getMenuDropDown(it.type))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_menu_exist_type.adapter = this
                    acp_menu_exist_type.setSelection(
                        getPositionMenu(
                            menuDetails?.type?.id,
                            menuFilter.value?.type
                        )
                    )
                }
                ArrayAdapter(
                    currentContext,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(getString(R.string.select))
                        addAll(createUpdateViewModel.getMenuDropDown(it.category))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_menu_exist_cateroy.adapter = this
                    acp_menu_exist_cateroy.setSelection(
                        getPositionMenu(
                            menuDetails?.category?.id,
                            menuFilter.value?.category
                        )
                    )
                }

            })
        }
    }

    private fun setImage(imgFile: Any) {
        cl_add_photograph.visibility = View.GONE
        cl_image_view.visibility = View.VISIBLE
        Glide.with(currentContext).load(imgFile).apply(
            RequestOptions().transform(CenterCrop(), RoundedCorners(16))
        ).into(iv_photo_image)
    }
}
