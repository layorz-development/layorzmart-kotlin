package com.layorz.mealee.menu.fragment

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.listener.LMMenuImagePreviewListener
import com.layorz.mealee.common.listener.LMMenuItemClickListener
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.menu.adapter.LMMenuAvailableAdapter
import com.layorz.mealee.menu.dialogFragment.LMMenuImageDialogFragment
import com.layorz.mealee.menu.viewmodel.LMMenuAvailableViewModel
import com.layorz.mealee.navigation.LMNavigationActivity
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_fragment_menu_available.*


class LMMenuAvailableFragment : LMFragment(), LMMenuItemClickListener, LMMenuImagePreviewListener {
    private val menuAvailableViewModel by lazy {
        ViewModelProvider(this).get(LMMenuAvailableViewModel::class.java)
    }
    private val menuAvailable by lazy {
        mutableListOf<LMMenuResponse>()
    }
    private val menuAvailableAdapter by lazy {
        LMMenuAvailableAdapter(
            currentContext,
            menuAvailable,
            this, this
        )
    }
    private var menuTypeSetFirst = false
    private var menuCategoryFirst = false
    private var menuClicked: LMMenuResponse? = null

    companion object {
        private const val DELETE_MENU_TAG = "delete_menu"
        private const val PREVIEW_IMAGE = "preview_image"
        fun newInstance() =
            LMMenuAvailableFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_menu_available, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbar(
            getString(R.string.menu_available),
            resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
        )
        initView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setListener()
        observerViewModel()
    }

    override fun menuItem(
        menuClicked: LMMenuResponse,
        editButtonClicked: Boolean,
        deleteButtonClicked: Boolean
    ) {
        this.menuClicked = menuClicked
        menuClicked.id?.let {
            if (deleteButtonClicked) {
                LMAlertUtil.showAlertDialog(
                    currentContext as FragmentActivity,
                    getString(R.string.delete_message),
                    getString(R.string.delete_menu),
                    true,
                    getString(R.string.delete),
                    getString(R.string.no),
                    DELETE_MENU_TAG
                )
            } else if (editButtonClicked) {
                LMNavigation.switchActivityForResult(
                    activity = currentContext as Activity,
                    destination = LMNavigationActivity::class.java,
                    requestCode = LMNavigationActivity.MENU_OPERATION,
                    bundle = bundleOf(
                        LMNavigationActivity.NAVIGATION_TYPE to LMNavigationActivity.CREATE_MENU,
                        LMNavigationActivity.MENU_DETAIL to Gson().toJson(menuClicked)
                    )
                )
            }
        }
    }

    override fun statusChanged(menuClicked: LMMenuResponse, position: Int) {
        menuAvailableViewModel.updateMenuItem(menuClicked, position)
    }

    private fun initView() {
        val userDetail = LMApplication.getInstance().getPreference().userData
        rv_menu_available.apply {
            adapter = menuAvailableAdapter
            viewTreeObserver.addOnGlobalLayoutListener(object :
                OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    layoutManager = GridLayoutManager(
                        currentContext,
                        LMUtilise.numberOfMenuInList(currentContext,width)
                    )
                }
            })
        }
        et_menu_search.isFocusableInTouchMode = false
        et_menu_search.isFocusable = false
        fab_add_menu.visibility =
            if (userDetail?.permissions?.contains(LMConstant.CREATE_MENU) == true) View.VISIBLE else View.GONE
    }

    private fun setListener() {
        et_menu_search.setOnClickListener {
            et_menu_search.isFocusableInTouchMode = true
            et_menu_search.isFocusable = true
        }
        fab_add_menu.setOnClickListener {
            LMNavigation.switchActivityForResult(
                activity = currentContext as Activity,
                destination = LMNavigationActivity::class.java,
                requestCode = LMNavigationActivity.MENU_OPERATION,
                bundle = bundleOf(LMNavigationActivity.NAVIGATION_TYPE to LMNavigationActivity.CREATE_MENU)
            )
        }
        et_menu_search.addTextChangedListener { searchQuery ->
            menuAvailableViewModel.getFoodForMenu(searchQuery.toString())
        }
        iv_reload_menu_filter.setOnClickListener {
            menuAvailableViewModel.getMenuFilterType()
        }
        iv_reload_menu_available.setOnClickListener {
            menuAvailableViewModel.getFilterMenuAvailable(true)
        }
        acp_menu_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuTypeSetFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(menuAvailableViewModel) {
                            menuCategory = getMenuFilterId(it, menuTypeList.value?.category)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuTypeSetFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        acp_menu_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuCategoryFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(menuAvailableViewModel) {
                            menuType = getMenuFilterId(it, menuTypeList.value?.type)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuCategoryFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun observerViewModel() {
        with(menuAvailableViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            menuTypeList.observe(viewLifecycleOwner, Observer {
                it?.let {
                    setDropDownData(it)
                }
            })
            menuAvailableList.observe(viewLifecycleOwner, Observer {
                setAdapter(it)
            })
            menuVisibilityChanged.observe(viewLifecycleOwner, Observer { it ->
                it?.let {
                    menuStatusChangedPosition?.let {
                        menuAvailableAdapter.notifyItemChanged(it)
                    }
                    menuVisibilityChanged.value = null
                }
            })
        }
    }

    private fun setDropDownData(menuFilter: LMMenuFilterResponse) {
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner_dropdown,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(menuAvailableViewModel.getMenuDropDown(menuFilter.type))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_type.adapter = this
            menuTypeSetFirst = false
            acp_menu_type.setSelection(
                menuAvailableViewModel.getPositionMenu(
                    menuAvailableViewModel.menuType,
                    menuFilter.type
                )
            )
        }
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(menuAvailableViewModel.getMenuDropDown(menuFilter.category))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_category.adapter = this
            menuCategoryFirst = false
            acp_menu_category.setSelection(
                menuAvailableViewModel.getPositionMenu(
                    menuAvailableViewModel.menuCategory,
                    menuFilter.category
                )
            )
        }
    }

    private fun setAdapter(menuAvailableData: MutableList<LMMenuResponse>) {
        rv_menu_available.visibility = View.VISIBLE
        iv_no_data.visibility = if (!menuAvailableData.isNullOrEmpty()) View.GONE else View.VISIBLE
        menuAvailable.clear()
        menuAvailable.addAll(menuAvailableData)
        rv_menu_available.notifyDataSetChanged()
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        if (DELETE_MENU_TAG == alertKey) {
            menuClicked?.id?.let {
                menuAvailableViewModel.deleteMenuItem(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LMNavigationActivity.MENU_OPERATION && resultCode == Activity.RESULT_OK) {
            menuAvailableViewModel.getFilterMenuAvailable(true)
        }
    }

    override fun imageUrl(url: String) {
        LMMenuImageDialogFragment.newInstance(url).show(parentFragmentManager, PREVIEW_IMAGE)
    }
}
