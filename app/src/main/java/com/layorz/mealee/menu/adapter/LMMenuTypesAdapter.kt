package com.layorz.mealee.menu.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.menu.dialogFragment.LMMenuTypesDialogFragment
import com.layorz.mealee.menu.viewmodel.LMMenuTypesViewModel
import com.layorz.mealee.network.response.LMMenuTypesResponse

class LMMenuTypesAdapter(var context:Context, var types:ArrayList<LMMenuTypesResponse>, var parentFragmentManager: FragmentManager, var menuTypesViewModel: LMMenuTypesViewModel,var isDeleteVisible:Boolean,var isEditVisible: Boolean):RecyclerView.Adapter<LMMenuTypesAdapter.ViewHolder>(){
    companion object{
        private const val DELETE_TYPE_TAG="delete_type"
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(context).inflate(R.layout.lm_menu_types_card,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return types.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var type=types[position]
        holder.type_name.text=type.name
        if (isDeleteVisible)
            holder.delete_type_name_btn.visibility=View.VISIBLE
        if(isEditVisible)
            holder.edit_type_name_btn.visibility=View.VISIBLE
        holder.edit_type_name_btn.setOnClickListener {
            LMMenuTypesDialogFragment.newInstance(type.id,type.name).show(parentFragmentManager,"")
        }
        holder.delete_type_name_btn.setOnClickListener {
            LMAlertUtil.showAlertDialog(
                context as FragmentActivity,context.getString(R.string.delete_message),context.getString(R.string.delete_type),true,context.getString(R.string.delete),
                context.getString(R.string.no),type.id
                    .toString(),
                DELETE_TYPE_TAG
                )
        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var type_name=itemView.findViewById<TextView>(R.id.type_name)
        var edit_type_name_btn=itemView.findViewById<ImageView>(R.id.iv_edit_type)
        var delete_type_name_btn=itemView.findViewById<ImageView>(R.id.iv_delete_type)
    }
}