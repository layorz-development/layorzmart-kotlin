package com.layorz.mealee.menu.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.events.LMAlertButtonClick
import com.layorz.mealee.common.events.LMEditTypeNameEvent
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.menu.adapter.LMMenuTypesAdapter
import com.layorz.mealee.menu.viewmodel.LMMenuTypesViewModel
import com.layorz.mealee.network.request.LMMenuTypesRequest
import kotlinx.android.synthetic.main.lm_fragment_menu_types.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class LMMenuTypesFragment :LMFragment() {
    private val menuTypesViewModel by lazy {
        ViewModelProvider(this).get(LMMenuTypesViewModel::class.java)
    }
    companion object{
        fun newInstance()= LMMenuTypesFragment()
    }
    var isDeleteVisible:Boolean=false
    var isEditVisible=false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setToolbar("Manage Types",true)
        var view=inflater.inflate(R.layout.lm_fragment_menu_types, container, false)
        menuTypesViewModel.getTypes()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
        setListener()
        checkPermissions()
    }
    private fun observeViewModel(){
        with(menuTypesViewModel) {
            types.observe(viewLifecycleOwner, Observer {
                if(it!=null) {
                    if(it.size>0) {
                        iv_no_data.visibility=View.GONE
                    }else{
                        iv_no_data.visibility=View.VISIBLE
                    }
                    menu_types_recycler_view.adapter =
                            LMMenuTypesAdapter(
                                requireContext(),
                                it!!,
                                parentFragmentManager,
                                menuTypesViewModel,isDeleteVisible,isEditVisible
                            )
                        menu_types_recycler_view.layoutManager =
                            LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                        tie_new_type_name.setText("")
                }
            })
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                Toast.makeText(requireContext(),it,Toast.LENGTH_LONG).show()
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
        }
    }
    fun setListener(){
        btn_add_new_type.setOnClickListener {
            menuTypesViewModel.createType(menuTypesViewModel.newTypeName)
        }
        tie_new_type_name.addTextChangedListener {
            menuTypesViewModel.newTypeName=it.toString()
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun editTypeName(editTypeEvent:LMEditTypeNameEvent){
        var request=LMMenuTypesRequest().apply {
            name=editTypeEvent.name
        }
        menuTypesViewModel.completeEditTypeName(editTypeEvent.id!!,request)
    }
    fun checkPermissions(){
        var permissions=LMApplication.getInstance().getPreference().userData?.permissions
        permissions.apply {
            if (this?.contains(LMConstant.CREATE_FILTER)!!){
                tv_order_name.visibility=View.VISIBLE
            }
            if (this.contains(LMConstant.CREATE_FILTER)){
                btn_add_new_type.visibility=View.VISIBLE
            }
            if(this.contains(LMConstant.DELETE_FILTER))
                isDeleteVisible=true
            if (this.contains(LMConstant.UPDATE_FILTER))
                isEditVisible=true
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDeleteType(alertButtonClickEvent:LMAlertButtonClick){
        var id=alertButtonClickEvent.alertKey.toInt()
        val buttonType: String = alertButtonClickEvent.buttonType
        if (LMConstant.ALERT_POSITIVE_BUTTON == buttonType) {
            menuTypesViewModel.deleteType(id)
        } else if (LMConstant.ALERT_NEGATIVE_BUTTON == buttonType) {
            onNegativeButtonClick(alertButtonClickEvent.alertKey)
        }
    }

}