package com.layorz.mealee.menu.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.menu.dialogFragment.LMMenuCategoriesDialogFragment
import com.layorz.mealee.menu.viewmodel.LMMenuCategoriesViewModel
import com.layorz.mealee.network.response.LMMenuCategoriesResponse

class LMMenuCategoriesAdapter(var context: Context, var categories:ArrayList<LMMenuCategoriesResponse>, var parentFragmentManager: FragmentManager, var menuCategoriesViewModel: LMMenuCategoriesViewModel,var isDeleteVisible:Boolean,var isEditVisible: Boolean):
    RecyclerView.Adapter<LMMenuCategoriesAdapter.ViewHolder>(){
    companion object{
        private const val DELETE_CATEGORY_TAG="delete_category"
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view= LayoutInflater.from(context).inflate(R.layout.lm_menu_categories_card,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var category=categories[position]
        holder.category_name.text=category.name
        if (isDeleteVisible)
            holder.delete_category_name_btn.visibility=View.VISIBLE
        if(isEditVisible)
            holder.edit_category_name_btn.visibility=View.VISIBLE
        holder.edit_category_name_btn.setOnClickListener {
            LMMenuCategoriesDialogFragment.newInstance(category.id,category.name).show(parentFragmentManager,"")
        }
        holder.delete_category_name_btn.setOnClickListener {
            LMAlertUtil.showAlertDialog(
                context as FragmentActivity,context.getString(R.string.delete_message),context.getString(R.string.delete_category),true,context.getString(R.string.delete),
                context.getString(R.string.no),category.id
                    .toString(),
                DELETE_CATEGORY_TAG
            )
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var category_name=itemView.findViewById<TextView>(R.id.category_name)
        var edit_category_name_btn=itemView.findViewById<ImageView>(R.id.iv_edit_category)
        var delete_category_name_btn=itemView.findViewById<ImageView>(R.id.iv_delete_category)
    }
}