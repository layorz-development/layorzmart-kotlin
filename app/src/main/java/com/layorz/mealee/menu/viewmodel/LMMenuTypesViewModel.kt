package com.layorz.mealee.menu.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.request.LMMenuTypesRequest
import com.layorz.mealee.network.response.LMMenuTypesResponse
import retrofit2.Call
import retrofit2.Response

class LMMenuTypesViewModel(application: Application):LMViewModel(application) {
    val types by lazy {
        MutableLiveData<ArrayList<LMMenuTypesResponse>>()
    }
    var newTypeName:String=""
    var id=0
    var name=""
    fun setDetail(id:Int,name:String){
        this.id=id
        this.name=name
    }
    fun editType(type_name:String){
        this.name=type_name

    }
    fun completeEditTypeName(id:Int,name:LMMenuTypesRequest){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().editType(id,name,object :LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuTypesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuTypesResponse>>,
                    response: Response<ArrayList<LMMenuTypesResponse>>
                ) {
                    if(response.isSuccessful) {
                        var types = response.body()
                       setTypes(types!!)
                    }

                }
            })
        }

    }
    fun getTypes(){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().getMenuTypes(object :
                LMRetrofitCallback<ArrayList<LMMenuTypesResponse>> {
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuTypesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuTypesResponse>>,
                    response: Response<ArrayList<LMMenuTypesResponse>>
                ) {
                    if (response.isSuccessful){
                        var types=response.body()
                        setTypes(types!!)

                    }
                }
            })
        }
    }
    fun setTypes(types:ArrayList<LMMenuTypesResponse>){
        this.types.value=types
        isProgressShowing.value=false
    }
    fun deleteType(id: Int){
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().deleteType(id,object :LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuTypesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuTypesResponse>>,
                    response: Response<ArrayList<LMMenuTypesResponse>>
                ) {
                    if (response.isSuccessful){
                        setTypes(response.body()!!)
                    }
                }
            })
        }
    }
    fun createType(name:String){
        isProgressShowing.value=true
        var menuTypesRequest=LMMenuTypesRequest().apply {
            this.name=name
        }
        LMApplication.getInstance().apply {
            getApiManager().createType(menuTypesRequest,object :LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>{
                override fun onNoNetwork() {
                    showNoNetworkToast()
                }

                override fun onFailure(call: Call<ArrayList<LMMenuTypesResponse>>, t: Throwable) {
                    handleApiResponse(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ArrayList<LMMenuTypesResponse>>,
                    response: Response<ArrayList<LMMenuTypesResponse>>
                ) {
                    if (response.isSuccessful){
                        setTypes(response.body()!!)

                    }
                }
            })
        }
    }
}