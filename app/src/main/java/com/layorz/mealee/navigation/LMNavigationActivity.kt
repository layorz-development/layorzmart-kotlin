package com.layorz.mealee.navigation

import android.os.Bundle
import android.view.MenuItem
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.menu.fragment.LMCreateEditMenuFragment
import com.layorz.mealee.network.response.LMMenuResponse
import com.layorz.mealee.order.tableorder.LMTakeOrderFragment
import com.layorz.mealee.order.takemenu.LMTakeOrderTableFragment

class LMNavigationActivity : LMActivity() {

    companion object {
        const val NAVIGATION_TYPE = "navigation_type"
        const val CREATE_MENU = "create_menu"
        const val CREATE_ORDER = "create_order"
        const val UPDATE_ORDER = "update_order"
        const val MENU_DETAIL = "menu_detail"
        const val ORDER_DETAIL = "order_detail"
        const val TABLE_DETAIL = "table_detail"
        const val MENU_OPERATION = 1000
        const val ORDER_OPERATION = 1001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lm_activity_navigation)
        if (savedInstanceState == null) {
            intent?.extras?.apply { ->
                val navigationScreen =
                    when (getString(NAVIGATION_TYPE)) {
                        CREATE_MENU -> LMCreateEditMenuFragment.newInstance(
                            getString(
                                MENU_DETAIL,
                                Gson().toJson(LMMenuResponse())
                            )
                        )
                        CREATE_ORDER -> LMTakeOrderTableFragment.newInstance(
                            getString(ORDER_DETAIL, null),
                            getString(TABLE_DETAIL, null)
                        )
                        UPDATE_ORDER -> LMTakeOrderFragment.newInstance(
                            getString(ORDER_DETAIL, null),
                            getString(TABLE_DETAIL, null)
                        )
                        else -> null
                    }
                navigationScreen?.let {
                    replaceCurrent(it, R.id.fl_navigation, false)
                }
                if (navigationScreen == null) {
                    finish()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        updateProgressBar(false)
        return when (item.itemId) {
            android.R.id.home -> {
                popFragment()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}
