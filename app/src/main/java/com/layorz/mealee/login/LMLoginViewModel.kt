package com.layorz.mealee.login

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.events.LMSubscriptionEndEvent
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMUserResponse
import com.layorz.mealee.repository.LMUserRepository
import org.greenrobot.eventbus.EventBus

class LMLoginViewModel(application: Application) : LMViewModel(application) {
    companion object{
        private const val ACTIVE="active"
        private const val INACTIVE="inactive"
    }

    val navigationToDashboard by lazy {
        MutableLiveData<Boolean>()
    }
    val emailFieldError by lazy {
        MutableLiveData<Int>()
    }
    val passwordFieldError by lazy {
        MutableLiveData<Int>()
    }
    val apiEmailFieldError by lazy {
        MutableLiveData<String>()
    }
    val apiPasswordFieldError by lazy {
        MutableLiveData<String>()
    }

    var userEmailId: String = ""
    var userPassword: String = ""

    private fun emailFieldValidation() = userEmailId.let {
        if (!TextUtils.isEmpty(it.trim())) {
            /*if (!Patterns.EMAIL_ADDRESS.matcher(it).matches()) {
                emailFieldError.value = R.string.email_field_not_valid
                false
            } else {
                true
            }*/
            true
        } else {
            emailFieldError.value = R.string.email_field_empty
            false
        }
    }

    private fun passwordFieldValidation() = userPassword.let {
        if (!TextUtils.isEmpty(it.trim())) {
            true
        } else {
            passwordFieldError.value = R.string.pasword_field_empty
            false
        }
    }

    fun loginUser() {
        if (emailFieldValidation() && passwordFieldValidation()) {
            isProgressShowing.value = true
            LMUserRepository.loginUser(
                userEmailId,
                userPassword,
                object : LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        LMApplication.getInstance().apply {
                            getPreference().apply {
                                emailId = userEmailId
                            }
                            (data as? LMUserResponse)?.s3Credentials?.let {
                                setTransferUtility(it)
                            }
                        }
                        var preference=LMApplication.getInstance().getPreference()
                        with(preference){
                            if (subscriptionStatus==ACTIVE){
                                navigationToDashboard.value = true
                            }else {
                                isProgressShowing.value=false
                                EventBus.getDefault().post(LMSubscriptionEndEvent())
                                apiResponseMessage.value="Subscrption ended"
                            }
                        }

                    }

                    override fun <T> onError(errorMessage: T) {
                        val errors = errorMessage as? JsonObject
                        errors?.let {
                            if (errors.has("email")) {
                                apiEmailFieldError.value =
                                    errors.getAsJsonArray("email").get(0).toString()
                            }
                            if (errors.has("password")) {
                                apiPasswordFieldError.value =
                                    errors.getAsJsonArray("password").get(0).toString()
                            }
                            isProgressShowing.value = false
                        }
                        if (errors == null) {
                            handleApiResponse(errorMessage as? String)
                        }
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                })
        }
    }
}