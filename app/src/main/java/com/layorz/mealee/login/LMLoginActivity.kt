package com.layorz.mealee.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.dashboard.activities.LMDashboardActivity
import com.layorz.mealee.registration.LMRegistrationActivity
import kotlinx.android.synthetic.main.lm_activity_login.*

class LMLoginActivity : LMActivity() {
    private val loginViewModel by lazy {
        ViewModelProvider(this).get(LMLoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lm_activity_login)
        initView()
        setListener()
        observerViewModel()
    }

    private fun initView() {
        LMApplication.getInstance().getPreference().apply {
            loginViewModel.apply {
                userEmailId = emailId
            }
            tie_username_login.setText(emailId)
            tie_username_login.setSelection(emailId.length)
        }
    }

    private fun setListener() {
        tie_username_login.addTextChangedListener {
            loginViewModel.userEmailId = it.toString().trim()
        }

        tie_password_login.addTextChangedListener {
            loginViewModel.userPassword = it.toString().trim()
        }

        btn_login.setOnClickListener {
            loginViewModel.loginUser()
        }
        tv_register.setOnClickListener {
            LMNavigation.switchActivity(this, LMRegistrationActivity::class.java)
        }
    }

    private fun observerViewModel() {
        with(loginViewModel) {
            emailFieldError.observe(this@LMLoginActivity, Observer { emailFieldError ->
                til_username_login.apply {
                    this.isErrorEnabled = emailFieldError?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            passwordFieldError.observe(this@LMLoginActivity, Observer {
                til_password_login.apply {
                    this.isErrorEnabled = it?.let {
                        error = getString(it)
                        true
                    } ?: false
                }
            })
            apiEmailFieldError.observe(this@LMLoginActivity, Observer { emailFieldError ->
                til_username_login.apply {
                    this.isErrorEnabled = emailFieldError?.let {
                        error = it
                        true
                    } ?: false
                }
            })
            apiPasswordFieldError.observe(this@LMLoginActivity, Observer {
                til_password_login.apply {
                    this.isErrorEnabled = it?.let {
                        error = it
                        true
                    } ?: false
                }
            })
            isProgressShowing.observe(this@LMLoginActivity, Observer {
                updateProgressBar(it ?: false)
            })
            apiResponseMessage.observe(this@LMLoginActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMLoginActivity, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(this@LMLoginActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMLoginActivity, it, Toast.LENGTH_SHORT).show()
                }
            })
            navigationToDashboard.observe(this@LMLoginActivity, Observer {
                it.let {
                    if (it) {
                        LMUtilise.hideKeyboard(this@LMLoginActivity)
                        LMNavigation.switchActivity(
                            this@LMLoginActivity,
                            LMDashboardActivity::class.java,
                            flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        )
                    }
                }
            })
        }
    }
}
