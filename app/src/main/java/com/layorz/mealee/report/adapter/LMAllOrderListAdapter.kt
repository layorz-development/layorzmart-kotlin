package com.layorz.mealee.report.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMPrintTableOrderItem
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMOrderResponse
import kotlinx.android.synthetic.main.lm_item_all_order.view.*

class LMAllOrderListAdapter(
    private val orderList: MutableList<LMOrderResponse>,
    private val printTableOrder: LMPrintTableOrderItem
) :
    RecyclerView.Adapter<LMAllOrderListAdapter.LMAllOrderListViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LMAllOrderListViewModel(
        LayoutInflater.from(parent.context).inflate(
            R.layout.lm_item_all_order, parent, false
        ), printTableOrder
    )

    override fun getItemCount() = orderList.size

    override fun onBindViewHolder(holder: LMAllOrderListViewModel, position: Int) {
        holder.apply {
            orderList[adapterPosition].apply {
                tvBillNo.text = billNo
                tvAmount.text = "\u20B9 ${amount?.let { total ->
                    LMUtilise.priceFormat.format(total)
                } ?: 0F}"
                ivOrderView.setImageResource(if (isExpandedOrChecked) R.drawable.ic_collapse_menu else R.drawable.ic_expand_menu)
                clOrderDetail.visibility = if (isExpandedOrChecked) View.VISIBLE else View.GONE
                tvOrderPrice.text = "\u20B9 ${LMUtilise.priceFormat.format(total ?: 0F)}"
                tvOrderDiscount.text = "\u20B9 ${LMUtilise.priceFormat.format(discount ?: 0F)}"
                Gson().fromJson<MutableList<LMOrderItemData>>(
                    order,
                    object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                )?.let {
                    rvTableDetail.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = LMFoodListAdapter(it)
                    }
                }
                vwItemClickHere.setOnClickListener {
                    isExpandedOrChecked = !isExpandedOrChecked
                    ivOrderView.setImageResource(if (isExpandedOrChecked) R.drawable.ic_collapse_menu else R.drawable.ic_expand_menu)
                    clOrderDetail.visibility = if (isExpandedOrChecked) View.VISIBLE else View.GONE
                }
                btnPrintBill.setOnClickListener {
                    printTableOrder.printTableOrder(this)
                }
            }
        }
    }

    class LMAllOrderListViewModel(
        itemView: View,
        val printTableOrder: LMPrintTableOrderItem
    ) : RecyclerView.ViewHolder(itemView) {
        val ivOrderView: ImageView = itemView.iv_order_view
        val tvBillNo: TextView = itemView.tv_bill_no
        val tvAmount: TextView = itemView.tv_amount
        val clOrderDetail: ConstraintLayout = itemView.cl_order_detail
        val rvTableDetail: RecyclerView = itemView.rv_table_detail
        val btnPrintBill: Button = itemView.btn_print_bill
        val tvOrderPrice: TextView = itemView.tv_order_price
        val tvOrderDiscount: TextView = itemView.tv_order_discount
        val vwItemClickHere = itemView.vw_item_click_here
    }
}