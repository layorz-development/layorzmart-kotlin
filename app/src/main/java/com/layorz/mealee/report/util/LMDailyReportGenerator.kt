package com.layorz.mealee.report.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.manager.LMApiManager
import com.layorz.mealee.network.response.LMSalesReportResponse
import com.layorz.mealee.repository.LMUserRepository
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.util.*

val preference = LMApplication.getInstance().getPreference()

class LMDailyReportGenerator(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    private var currentUser = preference.userData
    private val todayDate by lazy { LMUtilise.localData.parse(LMUtilise.localData.format(Date())) }
    private var lateReportGenerator = preference.lastSalesReportGeneratedDate
    private var isFirstTime = true
    var status: Result? = null
    override fun doWork(): Result {
        status = Result.failure()
        status = loadOrders()
        return status as Result
    }

    private fun loadOrders(): Result {
        lateReportGenerator = preference.lastSalesReportGeneratedDate
        if (currentUser != null) {
            val roles = currentUser?.roles
            val permission = currentUser?.permissions
            if (roles != null && permission != null) {
                if (!(roles.contains(LMConstant.ADMIN) || permission.contains(LMConstant.GENERATE_SALES_REPORT))) {
                    LMLog.d("LMDailyReportGenerator", "return on roles and permission check")
                    return Result.failure()
                }
            } else {
                LMLog.d("LMDailyReportGenerator", "return on roles and permission null check")
                return Result.failure()
            }
            val generateReportDate: Date? = lateReportGenerator?.let {
                Calendar.getInstance().apply {
                    time = it
                    add(Calendar.DATE, 1)
                }.time.apply {
                    LMUtilise.localData.parse(LMUtilise.localData.format(this))
                        ?.let { if (it < todayDate || it.compareTo(todayDate) == 0) this else null }
                } ?: null
            } ?: Calendar.getInstance().time
            if (generateReportDate != null) {
                val call: Call<LMSalesReportResponse> =
                    LMApiManager.getAuthorisedClient()
                        .getSalesReport(LMUtilise.localData.format(generateReportDate))
                return try {
                    reportResponse(call.execute(), generateReportDate)
                } catch (e: IOException) {
                    e.printStackTrace()
                    LMLog.d("LMDailyReportGenerator", "return on api Failure")
                    Result.failure()
                }
            }
            LMLog.d("LMDailyReportGenerator", "return on Date Failure")
            return Result.failure()
        } else {
            LMLog.d("LMDailyReportGenerator", "return on user Failure")
            return Result.failure()
        }
    }

    private fun reportResponse(
        response: Response<LMSalesReportResponse>?,
        generateReportDate: Date
    ): Result {
        response?.let {
            if (response.isSuccessful) {
                val billUtility = LMApplication.getInstance().getPdfManage()
                status = response.body()?.let {
                    billUtility.createDailySalesBill(
                        "create and notify",
                        context,
                        it,
                        LMUtilise.localData.format(generateReportDate)
                    )
                    if (ContextCompat.checkSelfPermission(
                            context,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        preference.lastSalesReportGeneratedDate = generateReportDate
                        LMLog.d("LMDailyReportGenerator", "return on api Success")
                        Result.success()
                    } else {
                        Result.failure()
                    }
                } ?: Result.failure()
            } else {
                if (response.code() == 404 && isFirstTime) {
                    preference.refreshToken?.let {
                        val call: Call<ResponseBody> =
                            LMApiManager.getRefreshToken().refreshToken()
                        try {
                            val refreshToken = call.execute()
                            if (refreshToken.isSuccessful) {
                                LMUserRepository.setTokens(refreshToken.headers())
                                if (preference.refreshToken != null)
                                    isFirstTime = false
                                loadOrders()
                            }
                        } catch (exception: Exception) {
                            exception.printStackTrace()
                        }
                    }
                    status = Result.failure(
                        Data.Builder()
                            .putString("reason", "Some error occurred while generating report")
                            .build()
                    )
                }
            }
        }
        val time = Calendar.getInstance().apply {
            time = generateReportDate
            add(Calendar.DATE, 1)
        }.time
        val nextDate = LMUtilise.localData.parse(LMUtilise.localData.format(time))
        return if ((nextDate != null && (nextDate < todayDate || nextDate.compareTo(todayDate) == 0)) && status == Result.success()) {
            reportResponse(
                LMApiManager.getAuthorisedClient().getSalesReport(LMUtilise.localData.format(time))
                    .execute(), time
            )
        } else {
            status as Result
        }
    }
}