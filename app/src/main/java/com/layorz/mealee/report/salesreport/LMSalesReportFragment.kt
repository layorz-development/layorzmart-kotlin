package com.layorz.mealee.report.salesreport

import android.app.DatePickerDialog
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.report.adapter.LMSalesReportItemAdapter
import kotlinx.android.synthetic.main.lm_fragment_sales_report.*
import java.util.*

class LMSalesReportFragment : LMPermissionFragment() {
    private val salesReportViewModel by lazy { ViewModelProvider(this).get(LMSalesReportViewModel::class.java) }

    companion object {
        fun newInstance() = LMSalesReportFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.lm_fragment_sales_report, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {}

    private fun initView() {
        setToolbar(getString(R.string.sales_report),resources.configuration.orientation== Configuration.ORIENTATION_PORTRAIT)
        tv_sales_report_date.text = salesReportViewModel.currentDate
    }

    private fun setListener() {
        vw_date_selector.setOnClickListener {
            with(salesReportViewModel) {
                val datePickerDialog = DatePickerDialog(
                    currentContext,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        currentDate = LMUtilise.getDayFormat(dayOfMonth, month, year)
                        tv_sales_report_date.text = currentDate
                        getSalesReport(true)
                        currentDay = dayOfMonth
                        currentMonth = month
                        currentYear = year
                    },
                    currentYear,
                    currentMonth,
                    currentDay
                )
                datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
                datePickerDialog.show()
            }
        }
        srl_sales_report.setOnRefreshListener { salesReportViewModel.getSalesReport(false) }
        fab_generate_report.setOnClickListener {
            salesReportViewModel.salesReportDetail.value?.let {
                createDailySalesBill(
                    "create and open",
                    it,
                    tv_sales_report_date.text.toString()
                )
            }
        }
    }

    private fun observerViewModel() {
        with(salesReportViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                srl_sales_report.isRefreshing = false
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            salesReportDetail.observe(viewLifecycleOwner, Observer {
                it?.let {
                    mcv_sales_count.visibility = it.orderDetails?.let {
                        tv_total_sales.text = it["total_amount"]?.let {
                            "\u20B9 " + LMUtilise.priceFormat.format(
                                it.toFloat().toDouble()
                            )
                        } ?: "-"
                        tv_no_of_order.text = it["no_of_orders"] ?: "-"
                        tv_no_of_item_sold.text = it["no_of_items_sold"] ?: "-"
                        View.VISIBLE
                    } ?: View.GONE
                }
            })
            salesReportItem.observe(viewLifecycleOwner, Observer {
                it?.let {
                    rv_sales_report_item.apply {
                        layoutManager = LinearLayoutManager(currentContext)
                        adapter = LMSalesReportItemAdapter(currentContext, it)
                    }
                    fab_generate_report.visibility =
                        if (it.isNotEmpty()) View.VISIBLE else View.GONE
                }
            })
        }
    }
}