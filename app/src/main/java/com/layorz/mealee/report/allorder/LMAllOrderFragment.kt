package com.layorz.mealee.report.allorder

import android.app.DatePickerDialog
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.listener.LMPrintTableOrderItem
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMPdfUtil
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.report.adapter.LMAllOrderListAdapter
import kotlinx.android.synthetic.main.lm_fragment_order_list.*
import java.util.*

class LMAllOrderFragment : LMPermissionFragment(), LMPrintTableOrderItem {
    private val allOrderViewModel by lazy { ViewModelProvider(this).get(LMAllOrderViewModel::class.java) }
    private val allOrderList by lazy { mutableListOf<LMOrderResponse>() }
    private val allOrderAdapter by lazy { LMAllOrderListAdapter(allOrderList, this) }

    companion object {
        fun newInstance() = LMAllOrderFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_fragment_order_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun printTableOrder(tableOrder: LMOrderResponse) {
        createBill(LMOrderDetail().apply {
            billNo = tableOrder.billNo
            orderItemList = Gson().fromJson<MutableList<LMOrderItemData>>(
                tableOrder.order,
                object : TypeToken<MutableList<LMOrderItemData>>() {}.type
            )
            orderName = tableOrder.orderName
            tableName = tableOrder.tableName
            billType = LMPdfUtil.CUSTOMER_BILL
            discountApplied = tableOrder.discount?.let { it > 0 } ?: false
            discount = LMUtilise.priceFormat.format(tableOrder.discount)
            subTotal = LMUtilise.priceFormat.format(tableOrder.total)
            tableOrder.total?.let { totalDetail ->
                total = LMUtilise.priceFormat.format(tableOrder.discount?.let {
                    if (it > totalDetail) 0F else totalDetail - it
                } ?: totalDetail)
            }
        })
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {}

    private fun initView() {
        setToolbar(getString(R.string.all_order),resources.configuration.orientation== Configuration.ORIENTATION_PORTRAIT)
        tv_order_date.text = allOrderViewModel.currentDate
        rv_order.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = allOrderAdapter
        }
    }

    private fun setListener() {
        vw_date_selector.setOnClickListener {
            with(allOrderViewModel) {
                val datePickerDialog = DatePickerDialog(
                    currentContext,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        currentDate = LMUtilise.getDayFormat(dayOfMonth, month, year)
                        tv_order_date.text = currentDate
                        getAllOrderList(true)
                        currentDay = dayOfMonth
                        currentMonth = month
                        currentYear = year
                    },
                    currentYear,
                    currentMonth,
                    currentDay
                )
                datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
                datePickerDialog.show()
            }
        }
        srl_order_list.setOnRefreshListener {
            allOrderViewModel.getAllOrderList(false)
        }
    }

    private fun observerViewModel() {
        with(allOrderViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                srl_order_list.isRefreshing = it
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            allOrderList.observe(viewLifecycleOwner, Observer {
                it?.let { setAdapter(it) }
                iv_order_not_available.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
        }
    }

    private fun setAdapter(orderList: MutableList<LMOrderResponse>) {
        allOrderList.clear()
        allOrderList.addAll(orderList)
        allOrderAdapter.notifyDataSetChanged()
    }
}