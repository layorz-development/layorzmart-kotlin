package com.layorz.mealee.report.allorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.report.pendingorder.instance
import com.layorz.mealee.repository.LMOrderRepository
import java.util.*

class LMAllOrderViewModel(application: Application) : LMViewModel(application) {
    val allOrderList by lazy { MutableLiveData<MutableList<LMOrderResponse>>() }
    var currentDate = LMUtilise.localData.format(Date())
    var currentMonth = instance.get(Calendar.MONTH)
    var currentDay = instance.get(Calendar.DAY_OF_MONTH)
    var currentYear = instance.get(Calendar.YEAR)

    init {
        getAllOrderList(true)
    }

    fun getAllOrderList(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMOrderRepository.getOrderAvailable(currentDate, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                (data as? MutableList<LMOrderResponse>)?.let {
                    allOrderList.value = it
                }
                isProgressShowing.value = false
                isPullToRefresh.value = false
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("errors")) {
                        apiResponseMessage.value = errors.getAsJsonArray("errors").get(0).toString()
                    }
                    isProgressShowing.value = false
                    isPullToRefresh.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}