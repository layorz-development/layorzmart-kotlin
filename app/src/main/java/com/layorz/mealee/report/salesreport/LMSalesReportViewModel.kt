package com.layorz.mealee.report.salesreport

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMSalesReportData
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMSalesReportResponse
import com.layorz.mealee.report.pendingorder.instance
import com.layorz.mealee.repository.LMReportRepository
import java.util.*

class LMSalesReportViewModel(application: Application) : LMViewModel(application) {
    val salesReportDetail by lazy { MutableLiveData<LMSalesReportResponse>() }
    val salesReportItem by lazy { MutableLiveData<MutableList<LMSalesReportData>>() }
    var currentDate = LMUtilise.localData.format(Date())
    var currentMonth = instance.get(Calendar.MONTH)
    var currentDay = instance.get(Calendar.DAY_OF_MONTH)
    var currentYear = instance.get(Calendar.YEAR)

    init {
        getSalesReport(true)
    }

    fun getSalesReport(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMReportRepository.getSalesReport(currentDate, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                isPullToRefresh.value = false
                (data as? LMSalesReportResponse)?.let {
                    salesReportDetail.value = it
                    generatorReportList(it)
                }
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("errors")) {
                        apiResponseMessage.value = errors.getAsJsonArray("errors").get(0).toString()
                    }
                    isProgressShowing.value = false
                    isPullToRefresh.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    private fun generatorReportList(salesReport: LMSalesReportResponse) {
        val salesReportListData = mutableListOf<LMSalesReportData>()
        addSalesReportItem(
            salesReportListData,
            salesReport.billDetails,
            R.string.billwise_count,
            R.string.bill_no,
            R.string.amount
        )
        addSalesReportItem(
            salesReportListData,
            salesReport.itemsCount,
            R.string.itemwise_count,
            R.string.item,
            R.string.sales_count
        )
        addSalesReportItem(
            salesReportListData,
            salesReport.typeCount,
            R.string.typewise_count,
            R.string.item,
            R.string.sales_count
        )
        addSalesReportItem(
            salesReportListData,
            salesReport.categoryCount,
            R.string.category_wise_count, R.string.item, R.string.sales_count
        )
        salesReportItem.value = salesReportListData
    }

    private fun addSalesReportItem(
        salesReportListData: MutableList<LMSalesReportData>,
        billDetails: Map<String, String>?,
        title: Int,
        tableName: Int,
        tableData: Int
    ) {
        billDetails.apply {
            billDetails?.let {
                if (it.isNotEmpty()) {
                    salesReportListData.add(LMSalesReportData(title, tableName, tableData, it))
                }
            }
        }
    }
}