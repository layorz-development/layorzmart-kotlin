package com.layorz.mealee.report.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMPrintTableOrderItem
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.network.response.LMOrderResponse
import kotlinx.android.synthetic.main.lm_item_order_summary.view.*

class LMPendingOrderAdapter(
    private val tableOrder: MutableList<LMOrderResponse>,
    private val printTableOrder: LMPrintTableOrderItem
) : RecyclerView.Adapter<LMPendingOrderAdapter.LMOrderDetailViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LMOrderDetailViewModel(
            LayoutInflater.from(parent.context).inflate(
                R.layout.lm_item_order_summary, parent, false
            ), printTableOrder
        )

    override fun getItemCount() = tableOrder.size

    override fun onBindViewHolder(holder: LMOrderDetailViewModel, position: Int) {
        holder.apply {
            tableOrder[adapterPosition].apply {
                btnPrintBill.setOnClickListener {
                    printTableOrder.printTableOrder(this)
                }
                tvTableName.text = tableName
                tvOrderName.text = orderName
                ivExpandMenu.setImageResource(if (isExpandedOrChecked) R.drawable.ic_collapse_menu else R.drawable.ic_expand_menu)
                rvTableDetail.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = Gson().fromJson<MutableList<LMOrderItemData>>(
                        order,
                        object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                    )?.let {
                        LMFoodListAdapter(
                            it
                        )
                    }
                }
                vwClickArea.setOnClickListener {
                    isExpandedOrChecked = !isExpandedOrChecked
                    clPendingOrder.visibility = if (isExpandedOrChecked) View.VISIBLE else View.GONE
                    ivExpandMenu.setImageResource(if (isExpandedOrChecked) R.drawable.ic_collapse_menu else R.drawable.ic_expand_menu)
                }
            }
        }
    }

    class LMOrderDetailViewModel(
        itemView: View,
        val printTableOrder: LMPrintTableOrderItem
    ) : RecyclerView.ViewHolder(itemView) {
        val tvOrderName: TextView = itemView.tv_order_name
        val tvTableName: TextView = itemView.tv_table_name
        val btnPrintBill: Button = itemView.btn_print_bill
        val rvTableDetail: RecyclerView = itemView.rv_order_detail
        val vwClickArea: View = itemView.vw_click_area
        val clPendingOrder: ConstraintLayout = itemView.cl_pending_order
        val ivExpandMenu: ImageView = itemView.iv_expand_menu
    }
}