package com.layorz.mealee.report.pendingorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.repository.LMPendingOrderRepository
import java.util.*

val instance: Calendar by lazy { Calendar.getInstance() }

class LMPendingOrderViewModel(application: Application) : LMViewModel(application) {
    val pendingOrder by lazy { MutableLiveData<MutableList<LMOrderResponse>>() }
    var currentDate = LMUtilise.localData.format(Date())
    var currentMonth = instance.get(Calendar.MONTH)
    var currentDay = instance.get(Calendar.DAY_OF_MONTH)
    var currentYear = instance.get(Calendar.YEAR)
    val orderName by lazy { mutableListOf<String>() }

    init {
        getPendingOrderList(true)
    }

    fun getPendingOrderList(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMPendingOrderRepository.getPendingOrderOnDate(currentDate, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                isPullToRefresh.value = false
                orderName.clear()
                pendingOrder.value = (data as? MutableList<LMOrderResponse>) ?: mutableListOf()
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("errors")) {
                        apiResponseMessage.value = errors.getAsJsonArray("errors").get(0).toString()
                    }
                    isProgressShowing.value = false
                    isPullToRefresh.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}