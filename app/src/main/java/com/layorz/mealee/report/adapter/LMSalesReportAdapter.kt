package com.layorz.mealee.report.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import kotlinx.android.synthetic.main.lm_item_sales_report.view.*

class LMSalesReportAdapter(
    private val reportList: Map<String, String>
) :
    RecyclerView.Adapter<LMSalesReportAdapter.LMSalesReportViewModel>() {
    private val keys by lazy { reportList.keys.toMutableList() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LMSalesReportViewModel(
        LayoutInflater.from(parent.context).inflate(
            R.layout.lm_item_sales_report, parent, false
        )
    )

    override fun getItemCount() = reportList.keys.size

    override fun onBindViewHolder(holder: LMSalesReportViewModel, position: Int) {
        holder.apply {
            val key = keys[adapterPosition]
            reportList[key]?.let { data ->
                tvItemName.text = key
                tvItemDetail.text = data
            }
            vwBottomLine.visibility = View.GONE
        }
    }

    class LMSalesReportViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvItemName: TextView = itemView.tv_item_name
        val tvItemDetail: TextView = itemView.tv_item_detail
        val vwBottomLine: View = itemView.vw_bottom_line
    }
}