package com.layorz.mealee.report.pendingorder

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.listener.LMPrintTableOrderItem
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMPdfUtil
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.report.adapter.LMPendingOrderAdapter
import kotlinx.android.synthetic.main.lm_fragment_order_list.*
import java.util.*

class LMPendingOrderFragment : LMPermissionFragment(), LMPrintTableOrderItem {
    private val pendingOrderViewModel by lazy { ViewModelProvider(this).get(LMPendingOrderViewModel::class.java) }
    private val pendingOrderList by lazy { mutableListOf<LMOrderResponse>() }
    private val pendingOrderAdapter by lazy {
        LMPendingOrderAdapter(
            pendingOrderList,
            this
        )
    }

    companion object {
        fun newInstance() =
            LMPendingOrderFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {}

    private fun initView() {
        setToolbar(getString(R.string.kot))
        tv_order_date.text = pendingOrderViewModel.currentDate
        rv_order.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = pendingOrderAdapter
        }
    }

    private fun setListener() {
        vw_date_selector.setOnClickListener {
            with(pendingOrderViewModel) {
                val datePickerDialog = DatePickerDialog(
                    currentContext,
                    OnDateSetListener { _, year, month, dayOfMonth ->
                        currentDate = LMUtilise.getDayFormat(dayOfMonth, month, year)
                        tv_order_date.text = currentDate
                        pendingOrderViewModel.getPendingOrderList(true)
                        currentDay = dayOfMonth
                        currentMonth = month
                        currentYear = year
                    },
                    currentYear,
                    currentMonth,
                    currentDay
                )
                datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
                datePickerDialog.show()
            }
        }
        srl_order_list.setOnRefreshListener {
            pendingOrderViewModel.getPendingOrderList(false)
        }
    }

    private fun observerViewModel() {
        with(pendingOrderViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                srl_order_list.isRefreshing = it
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
            pendingOrder.observe(viewLifecycleOwner, Observer {
                it?.let { setAdapter(it) }
                iv_order_not_available.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
        }
    }

    private fun setAdapter(orderList: MutableList<LMOrderResponse>) {
        pendingOrderList.clear()
        pendingOrderList.addAll(orderList)
        pendingOrderAdapter.notifyDataSetChanged()
    }

    override fun printTableOrder(tableOrder: LMOrderResponse) {
        createBill(LMOrderDetail().apply {
            orderName = tableOrder.orderName
            tableName = tableOrder.tableName
            orderItemList = Gson().fromJson<MutableList<LMOrderItemData>>(
                tableOrder.order,
                object : TypeToken<MutableList<LMOrderItemData>>() {}.type
            )
            billType = LMPdfUtil.KITCHEN_BILL
        })
    }
}