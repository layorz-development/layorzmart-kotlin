package com.layorz.mealee.report.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.model.LMSalesReportData
import kotlinx.android.synthetic.main.lm_item_sales_report_item.view.*

class LMSalesReportItemAdapter(
    private val context: Context,
    private val salesReportList: MutableList<LMSalesReportData>
) :
    RecyclerView.Adapter<LMSalesReportItemAdapter.LMSalesReportItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LMSalesReportItemViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.lm_item_sales_report_item, parent, false
        )
    )

    override fun getItemCount() = salesReportList.size

    override fun onBindViewHolder(holder: LMSalesReportItemViewHolder, position: Int) {
        holder.apply {
            salesReportList[adapterPosition].apply {
                tvSalesReport.text = context.getString(salesReportName)
                rvSalesReport.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = LMSalesReportAdapter(reportData)
                }
                tvBillNumberTitle.text = context.getString(tableItemName)
                tvAmountTitle.text = context.getString(tableItemData)
            }
        }
    }

    class LMSalesReportItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSalesReport: TextView = itemView.tv_sales_report
        val rvSalesReport: RecyclerView = itemView.rv_sales_report
        val tvBillNumberTitle: TextView = itemView.tv_bill_number_title
        val tvAmountTitle: TextView = itemView.tv_amount_title
    }
}