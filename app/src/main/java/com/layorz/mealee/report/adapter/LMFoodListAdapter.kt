package com.layorz.mealee.report.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMUtilise
import kotlinx.android.synthetic.main.lm_item_food_list.view.*

class LMFoodListAdapter(private val foodList: MutableList<LMOrderItemData>) :
    RecyclerView.Adapter<LMFoodListAdapter.LMFoodListViewModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LMFoodListViewModel(
            LayoutInflater.from(parent.context).inflate(R.layout.lm_item_food_list, parent, false)
        )

    override fun getItemCount() = foodList.size

    override fun onBindViewHolder(holder: LMFoodListViewModel, position: Int) {
        holder.apply {
            foodList[adapterPosition].apply {
                tvFoodName.text = itemName
                tvQuantity.text = quantity.toString()
                tvPrice.text = "\u20B9 ${LMUtilise.priceFormat.format(price)}"
                tvAmount.text = "\u20B9 ${LMUtilise.priceFormat.format(price * quantity)}"
            }
        }
    }

    class LMFoodListViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvFoodName: TextView = itemView.tv_food_name
        val tvQuantity: TextView = itemView.tv_quantity
        val tvPrice: TextView = itemView.tv_price
        val tvAmount: TextView = itemView.tv_amount
    }
}