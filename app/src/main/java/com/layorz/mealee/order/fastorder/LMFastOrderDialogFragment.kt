package com.layorz.mealee.order.fastorder

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMCompleteFastOrderEvent
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.request.LMCreateFastOrderRequest
import com.layorz.mealee.network.request.LMCreateOrderRequest
import kotlinx.android.synthetic.main.lm_dialog_complete_order.*
import org.greenrobot.eventbus.EventBus

class LMFastOrderDialogFragment : LMDialogFragment() {
    private val fastOrderViewModel by lazy {
        ViewModelProvider(this).get(
            LMFastOrderViewModel::class.java
        )
    }

    companion object {
        private const val ORDER_DETAIL_JSON = "order_detail_json"
        private const val TOTAL = "total"
        fun newInstance(orderDetailJson: String, total: Int) =
            LMFastOrderDialogFragment().apply {
                arguments = bundleOf(
                    ORDER_DETAIL_JSON to orderDetailJson,
                    TOTAL to total

                )
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            fastOrderViewModel.setDetail(
                Gson().fromJson<ArrayList<LMCreateFastOrderRequest>>(
                    getString(ORDER_DETAIL_JSON),
                    object :
                        TypeToken<ArrayList<LMCreateFastOrderRequest>>() {}.type
                )
                , getString(ORDER_DETAIL_JSON)!!, getInt(TOTAL)

            )
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(currentContext).apply {
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.lm_dialog_fast_order, container, false)
        var Tv_price = view.findViewById<TextView>(R.id.tv_price)
        Tv_price.setText(fastOrderViewModel.total_cpy.toString())

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        LMUtilise.hideKeyboard(currentContext as Activity)
    }

    private fun setListener() {
        et_order_discount.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                if (it.toString().length < 10)
                    fastOrderViewModel.setDiscount(it.toString())
            } else
                fastOrderViewModel.total_cpy.value = fastOrderViewModel.total

        }

        et_order_discount.setOnClickListener {
            et_order_discount.isFocusableInTouchMode = true;
            et_order_discount.isFocusable = true
            val inputMethodManager =
                currentContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(et_order_discount, InputMethodManager.SHOW_IMPLICIT)
            et_order_discount.requestFocus()
        }
        iv_close.setOnClickListener {
            dismiss()
        }
        btn_complete_order.setOnClickListener {
            var sub_total = fastOrderViewModel.total
            var total = fastOrderViewModel.total_cpy.value
            var _order = fastOrderViewModel.orderDetailsJson
            var orderDetails = LMCreateOrderRequest(
                _order,
                sub_total.toString(),
                total.toString(),
                fastOrderViewModel.discount.toString()
            )
            dismiss()
            EventBus.getDefault().post(
                LMCompleteFastOrderEvent(
                    orderDetails,
                    false,
                    fastOrderViewModel.orderDetails!!
                )
            )

        }
        btn_print_complete_order.setOnClickListener {
            dismiss()
            var sub_total = fastOrderViewModel.total
            var total = fastOrderViewModel.total_cpy.value
            var _order = fastOrderViewModel.orderDetailsJson
            var orderDetails = LMCreateOrderRequest(
                _order,
                sub_total.toString(),
                total.toString(),
                fastOrderViewModel.discount.toString()
            )
            EventBus.getDefault().post(
                LMCompleteFastOrderEvent(
                    orderDetails,
                    true,
                    fastOrderViewModel.orderDetails!!
                )
            )

        }
    }

    private fun observerViewModel() {
        with(fastOrderViewModel) {
            total_cpy.observe(viewLifecycleOwner, Observer {
                tv_price.setText(" ₹ ${it.toFloat()}")
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    android.widget.Toast.makeText(
                        currentContext,
                        getString(it),
                        android.widget.Toast.LENGTH_LONG
                    ).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
        }
    }

}