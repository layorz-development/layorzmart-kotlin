package com.layorz.mealee.order.completeorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMOrderItemData

class LMEditTableMenuViewModel(application: Application) : LMViewModel(application) {
    val updateQuantity by lazy { MutableLiveData<Int>() }
    var tableItem: LMOrderItemData? = null

    fun setTableMenu(tableItem: LMOrderItemData) {
        this.tableItem = tableItem
        updateQuantity.value = tableItem.quantity
    }

    fun minusItem() {
        tableItem?.apply {
            if (quantity > 0) {
                quantity -= 1
                updateQuantity.value = quantity
            }
        }
    }

    fun addItem() {
        tableItem?.apply {
            quantity += 1
            updateQuantity.value = quantity
        }
    }
}