package com.layorz.mealee.order.completeorder

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMCompleteOrderEvent
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.common.utils.LMUtilise.priceFormat
import com.layorz.mealee.network.response.LMOrderResponse
import kotlinx.android.synthetic.main.lm_dialog_complete_order.*
import org.greenrobot.eventbus.EventBus

class LMCompleteOrderDialogFragment : LMDialogFragment() {
    private val completeOrderViewModel by lazy {
        ViewModelProvider(this).get(
            LMCompleteOrderViewModel::class.java
        )
    }

    companion object {
        private const val ORDER_DETAIL = "order_detil"
        private const val IS_COMPLETING_ONE_TABLE = "is_completing_one_table"
        fun newInstance(orderDetail: LMOrderResponse, isCompletingOneTable: Boolean) =
            LMCompleteOrderDialogFragment().apply {
                arguments = bundleOf(
                    ORDER_DETAIL to Gson().toJson(orderDetail),
                    IS_COMPLETING_ONE_TABLE to isCompletingOneTable
                )
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            completeOrderViewModel.setDetail(
                Gson().fromJson(
                    getString(ORDER_DETAIL),
                    LMOrderResponse::class.java
                ), getBoolean(IS_COMPLETING_ONE_TABLE)
            )
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(currentContext).apply {
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_dialog_complete_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        with(completeOrderViewModel) {
            tv_order_name.text = orderDetail?.orderName
            tv_table_name.text = orderDetail?.tableName
        }
        LMUtilise.hideKeyboard(currentContext as Activity)
    }

    private fun setListener() {
        et_order_discount.addTextChangedListener {
            completeOrderViewModel.setDiscount(it.toString())
        }
        et_order_discount.setOnClickListener {
            et_order_discount.isFocusableInTouchMode = true;
            et_order_discount.isFocusable = true
            (currentContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(
                et_order_discount,
                InputMethodManager.SHOW_IMPLICIT
            )
            et_order_discount.requestFocus()
        }
        iv_close.setOnClickListener {
            dismiss()
        }
        btn_complete_order.setOnClickListener {
            dismiss()
            completeOrderViewModel.orderDetail?.let {
                EventBus.getDefault().post(
                    LMCompleteOrderEvent(
                        completeOrderViewModel.isCompletingOneTable,
                        false,
                        it
                    )
                )
            }
        }
        btn_print_complete_order.setOnClickListener {
            completeOrderViewModel.orderDetail?.let {
                dismiss()
                EventBus.getDefault().post(
                    LMCompleteOrderEvent(
                        completeOrderViewModel.isCompletingOneTable,
                        true,
                        it
                    )
                )
            }
        }
    }

    private fun observerViewModel() {
        with(completeOrderViewModel) {
            priceEvent.observe(viewLifecycleOwner, Observer {
                tv_price.text = "\u20B9 ${priceFormat.format(it) ?: 0}"
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}