package com.layorz.mealee.order.completeorder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMEditTableOrderEvent
import com.layorz.mealee.common.model.LMOrderItemData
import kotlinx.android.synthetic.main.lm_dialog_edit_table_menu.*
import org.greenrobot.eventbus.EventBus

class LMEditTableMenuDialogFragment : LMDialogFragment() {
    private val editTableMenuViewModel by lazy {
        ViewModelProvider(this).get(LMEditTableMenuViewModel::class.java)
    }

    companion object {
        private const val TABLE_MENU = "table_menu"
        fun newInstance(orderName: LMOrderItemData) = LMEditTableMenuDialogFragment().apply {
            arguments = bundleOf(TABLE_MENU to Gson().toJson(orderName))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            editTableMenuViewModel.setTableMenu(
                Gson().fromJson(
                    getString(TABLE_MENU),
                    LMOrderItemData::class.java
                )
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_dialog_edit_table_menu, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    private fun initView() {
        with(editTableMenuViewModel) {
            tableItem?.itemName?.let {
                tv_menu_name.text = it
            }
        }
    }

    private fun setListener() {
        iv_close.setOnClickListener {
            dismiss()
        }
        btn_save_item.setOnClickListener {
            val quantity = editTableMenuViewModel.tableItem?.quantity
            quantity?.let { itemQuantity ->
                EventBus.getDefault().post(
                    LMEditTableOrderEvent(itemQuantity)
                )
            }
            dismiss()
        }
        btn_remove_item.setOnClickListener {
            EventBus.getDefault().post(LMEditTableOrderEvent(0))
            dismiss()
        }
        iv_minus.setOnClickListener {
            editTableMenuViewModel.minusItem()
        }
        iv_add.setOnClickListener {
            editTableMenuViewModel.addItem()
        }
    }

    private fun observerViewModel() {
        with(editTableMenuViewModel) {
            updateQuantity.observe(viewLifecycleOwner, Observer {
                tv_quantity.text = it?.toString() ?: "0"
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}