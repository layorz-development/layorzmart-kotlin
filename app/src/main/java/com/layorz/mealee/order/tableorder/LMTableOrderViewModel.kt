package com.layorz.mealee.order.tableorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.repository.LMPendingOrderRepository

class LMTableOrderViewModel(application: Application) : LMViewModel(application) {
    val orderDetail by lazy { MutableLiveData<MutableList<LMOrderResponse>>() }
    var orderName: String? = null
    val tableName by lazy { mutableListOf<String>() }
    var selectedOrderName: String? = null
    var selectedTableName: String? = null

    fun setOrder(orderName: String?, tableName: String?) {
        this.orderName = orderName
        selectedTableName = tableName
    }

    fun getOrderDetail(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        orderName?.let {
            LMPendingOrderRepository.getPendingForOrder(it, object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    tableName.clear()
                    val orderList = mutableListOf<LMOrderResponse>()
                    (data as? MutableList<LMOrderResponse>)?.let {
                        for (orderItem in it) {
                            if (!tableName.contains(orderItem.tableName)) {
                                orderItem.tableName?.let {
                                    tableName.add(it)
                                }
                                val orderOnTable by lazy { mutableListOf<LMOrderItemData>() }
                                for (tableItemData in it) {
                                    val tableItem = Gson().fromJson<MutableList<LMOrderItemData>>(
                                        tableItemData.order,
                                        object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                                    )
                                    if (tableItemData.tableName == orderItem.tableName) {
                                        for (individualItem in tableItem) {
                                            val tableOrderItem by lazy { mutableListOf<LMOrderItemData>() }
                                            tableOrderItem.addAll(orderOnTable)
                                            if (tableOrderItem.size > 0) {
                                                var isAlreadyAvailable = false
                                                for (itemData in tableOrderItem) {
                                                    if (itemData.itemId == individualItem.itemId) {
                                                        for (existItem in orderOnTable) {
                                                            if (existItem.itemId == individualItem.itemId) {
                                                                existItem.quantity += individualItem.quantity
                                                            }
                                                        }
                                                        isAlreadyAvailable = true
                                                    }
                                                }
                                                if (!isAlreadyAvailable) {
                                                    orderOnTable.add(individualItem)
                                                }
                                            } else {
                                                orderOnTable.add(individualItem)
                                            }
                                        }
                                    }
                                }
                                var totalCost = 0F
                                for (item in orderOnTable) {
                                    totalCost += item.quantity * item.price
                                }
                                orderItem.total = totalCost
                                orderItem.order = Gson().toJson(orderOnTable)
                                orderList.add(orderItem)
                            }
                        }
                        orderDetail.value = orderList
                    }
                    isProgressShowing.value = false
                    isPullToRefresh.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    val errors = errorMessage as? JsonObject
                    errors?.let {
                        if (errors.has("order_name")) {
                            orderDetail.value = mutableListOf()
                        }
                        isProgressShowing.value = false
                    }
                    if (errors == null) {
                        handleApiResponse(errorMessage as? String)
                    }
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
        }
    }

    fun deleteTableOrder() {
        selectedTableName?.let { table ->
            selectedOrderName?.let { order ->
                isProgressShowing.value = true
                LMPendingOrderRepository.deletePendingOrder(
                    table,
                    order,
                    object : LMApiResponseCallback {
                        override fun <T> onSuccess(data: T) {
                            (data as? LMCommonResponse)?.let {
                                apiResponseMessage.value = it.message
                            }
                            getOrderDetail(false)
                        }

                        override fun <T> onError(errorMessage: T) {
                            handleApiResponse(errorMessage as? String)
                        }

                        override fun onFailure(failureMessage: String?) {
                            handleApiResponse(failureMessage)
                        }

                        override fun onNoNetwork() {
                            showNoNetworkToast()
                        }
                    })
            }
        }
    }
}