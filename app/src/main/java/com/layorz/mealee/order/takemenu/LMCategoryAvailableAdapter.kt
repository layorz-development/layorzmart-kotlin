package com.layorz.mealee.order.takemenu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMCategoryMenuListener
import com.layorz.mealee.common.model.LMMenuFilterData
import kotlinx.android.synthetic.main.lm_item_category_menu.view.*

class LMCategoryAvailableAdapter(
    private val context: Context,
    private val menuFilter: MutableList<LMMenuFilterData>,
    private val categoryMenu: LMCategoryMenuListener
) :
    RecyclerView.Adapter<LMCategoryAvailableAdapter.LMCategoryAvailableViewModel>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = LMCategoryAvailableViewModel(
        LayoutInflater.from(parent.context).inflate(R.layout.lm_item_category_menu, parent, false)
    )

    override fun getItemCount() = menuFilter.size

    override fun onBindViewHolder(holder: LMCategoryAvailableViewModel, position: Int) {
        holder.apply {
            menuFilter[adapterPosition].apply {
                clCategory.setOnClickListener {
                    categoryMenu.categoryMenu(this, adapterPosition)
                    notifyItemChanged(adapterPosition)
                }
                tvCategoryName.text = nameId?.let { context.getString(it) } ?: name
                tvCategoryName.setTextColor(
                    ContextCompat.getColor(
                        context,
                        if (isSelected) R.color.colorPrimary else R.color.colorProgressBar
                    )
                )
            }
        }
    }

    class LMCategoryAvailableViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clCategory: ConstraintLayout = itemView.cl_category
        val tvCategoryName: TextView = itemView.tv_category_name
    }
}