package com.layorz.mealee.order.tableorder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat.setNestedScrollingEnabled
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.listener.LMDeleteTableOrderListener
import com.layorz.mealee.common.listener.LMEditTableItemListener
import com.layorz.mealee.common.listener.LMSelectTableOrderListener
import com.layorz.mealee.common.listener.LMTakeOrderForTableListener
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMUtilise.priceFormat
import com.layorz.mealee.network.response.LMOrderResponse
import kotlinx.android.synthetic.main.lm_item_order_detail.view.*

class LMOrderDetailAdapter(
    private val context: Context,
    private val orderDetail: MutableList<LMOrderResponse>,
    private val takeOrderForTableListener: LMTakeOrderForTableListener?,
    private val editTableItemListener: LMEditTableItemListener?,
    private val selectTableOrderListener: LMSelectTableOrderListener?,
    private val deleteTableOrderListener: LMDeleteTableOrderListener?,
    private val isCompleteOrder: Boolean = false,
    private val isEditOption: Boolean = true
) :
    RecyclerView.Adapter<LMOrderDetailAdapter.LMOrderDetailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LMOrderDetailViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.lm_item_order_detail, parent, false
            ),
            takeOrderForTableListener,
            editTableItemListener,
            selectTableOrderListener,
            deleteTableOrderListener
        )

    override fun getItemCount() = orderDetail.size

    override fun onBindViewHolder(holder: LMOrderDetailViewHolder, position: Int) {
        val userData = LMApplication.getInstance().getPreference().userData
        holder.apply {
            ivDeleteTable.visibility =
                if (userData?.permissions?.contains(
                        LMConstant.DELETE_PENDING_ORDER
                    ) == true
                ) View.VISIBLE else View.GONE
            setNestedScrollingEnabled(rvTableOrderItem, false)
            orderDetail[adapterPosition].apply {
                clTableSelect.visibility = if (isCompleteOrder) View.VISIBLE else View.GONE
                ivTableChecked.setImageResource(if (isExpandedOrChecked) R.drawable.ic_radio_button_checked else R.drawable.ic_radio_button_unchecked)
                tvTableCheck.text =
                    context.resources.getString(if (isExpandedOrChecked) R.string.selected else R.string.un_selected)
                btnAddComplete.text =
                    context.resources.getString(if (isCompleteOrder) R.string.complete_table_order else R.string.add_menu)
                tvTableName.text = tableName
                Gson().fromJson<MutableList<LMOrderItemData>>(
                    order,
                    object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                )?.let {
                    rvTableOrderItem.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter =
                            LMOrderTableDetailAdapter(
                                it,
                                editTableItemListener,
                                isEditOption,
                                adapterPosition
                            )
                    }
                }
                tvTotal.text = "\u20B9 ${priceFormat.format(total)}"
                tvActionTitle.visibility = if (isEditOption) View.VISIBLE else View.GONE
                btnAddComplete.setOnClickListener {
                    if (isCompleteOrder) {
                        takeOrderForTableListener?.completeTableOrder(this)
                    } else {
                        tableName?.let {
                            takeOrderForTableListener?.takeOrderForTable(it)
                        }
                    }
                }
                clTableSelect.setOnClickListener {
                    notifyItemChanged(adapterPosition)
                    isExpandedOrChecked = !isExpandedOrChecked
                    selectTableOrderListener?.selectTableOrder(this, adapterPosition)
                }
                ivDeleteTable.setOnClickListener {
                    tableName?.let { table ->
                        orderName?.let {
                            deleteTableOrderListener?.deleteTableOrder(table, it)
                        }
                    }
                }
            }
        }
    }

    class LMOrderDetailViewHolder(
        itemView: View,
        val takeOrderForTableListener: LMTakeOrderForTableListener?,
        val editTableItemListener: LMEditTableItemListener?,
        val selectTableOrderListener: LMSelectTableOrderListener?,
        val deleteTableOrderListener: LMDeleteTableOrderListener?
    ) : RecyclerView.ViewHolder(itemView) {
        val btnAddComplete: Button = itemView.btn_add_complete
        val clTableSelect: ConstraintLayout = itemView.cl_check_table
        val tvTableName: TextView = itemView.tv_table_name
        val rvTableOrderItem: RecyclerView = itemView.rv_table_order_item
        val ivDeleteTable: ImageView = itemView.iv_delete_table
        val ivTableChecked: ImageView = itemView.iv_table_checked
        val tvTableCheck: TextView = itemView.tv_table_check
        val tvActionTitle: TextView = itemView.tv_action_title
        val tvTotal: TextView = itemView.tv_total
    }
}