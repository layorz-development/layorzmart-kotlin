package com.layorz.mealee.order.completeorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.events.LMCompleteOrderEvent
import com.layorz.mealee.common.events.LMEditTableOrderEvent
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMPdfUtil
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMCreateOrderRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMCompleteOrderResponse
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.repository.LMOrderRepository
import com.layorz.mealee.repository.LMPendingOrderRepository

class LMCompleteViewModel(application: Application) : LMViewModel(application) {
    val orderAvailable by lazy {
        MutableLiveData<MutableList<String>>()
    }
    val orderList by lazy {
        mutableListOf<LMOrderResponse>()
    }
    val completeTable by lazy {
        MutableLiveData<LMOrderResponse>()
    }
    val tableName by lazy { mutableListOf<String>() }
    val orderDetail by lazy { MutableLiveData<MutableList<LMOrderResponse>>() }
    var printBill: LMOrderDetail? = null
    var orderSelected = 0
    var orderName: String? = null
    var selectAllTable = true
    var selectedOrderName: String? = null
    var selectedTableName: String? = null
    var selectedTablePosition = 0
    var tableOrderItem: LMOrderItemData? = null


    init {
        getOrderAvailable(null)
    }

    private fun getOrderAvailable(billOrder: LMOrderDetail?) {
        isProgressShowing.value = true
        orderSelected = 0
        LMPendingOrderRepository.getAllPendingOrderName(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                printBill = billOrder
                orderAvailable.value = data as MutableList<String>
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getOrderDetail(isProgressNeed: Boolean, billOrder: LMOrderDetail?) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        orderName?.let {
            LMPendingOrderRepository.getPendingForOrder(it, object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    tableName.clear()
                    val orderList = mutableListOf<LMOrderResponse>()
                    (data as? MutableList<LMOrderResponse>)?.let {
                        if (it.size > 0) {
                            for (check in it) {
                                check.isExpandedOrChecked = selectAllTable
                            }
                            for (orderItem in it) {
                                if (!tableName.contains(orderItem.tableName)) {
                                    orderItem.tableName?.let {
                                        tableName.add(it)
                                    }
                                    val orderOnTable by lazy { mutableListOf<LMOrderItemData>() }
                                    for (tableItemData in it) {
                                        val tableItem =
                                            Gson().fromJson<MutableList<LMOrderItemData>>(
                                                tableItemData.order,
                                                object :
                                                    TypeToken<MutableList<LMOrderItemData>>() {}.type
                                            )
                                        if (tableItemData.tableName == orderItem.tableName) {
                                            for (individualItem in tableItem) {
                                                val tableOrderItem by lazy { mutableListOf<LMOrderItemData>() }
                                                tableOrderItem.addAll(orderOnTable)
                                                if (tableOrderItem.size > 0) {
                                                    var isAlreadyAvailable = false
                                                    for (itemData in tableOrderItem) {
                                                        if (itemData.itemId == individualItem.itemId) {
                                                            for (existItem in orderOnTable) {
                                                                if (existItem.itemId == individualItem.itemId) {
                                                                    existItem.quantity += individualItem.quantity
                                                                }
                                                            }
                                                            isAlreadyAvailable = true
                                                        }
                                                    }
                                                    if (!isAlreadyAvailable) {
                                                        orderOnTable.add(individualItem)
                                                    }
                                                } else {
                                                    orderOnTable.add(individualItem)
                                                }
                                            }
                                        }
                                    }
                                    var totalCost = 0F
                                    for (item in orderOnTable) {
                                        totalCost += item.quantity * item.price
                                    }
                                    orderItem.total = totalCost
                                    orderItem.order = Gson().toJson(orderOnTable)
                                    orderList.add(orderItem)
                                }
                            }
                            printBill = billOrder
                            orderDetail.value = orderList
                        } else {
                            orderDetail.value = mutableListOf()
                            getOrderAvailable(billOrder)
                        }
                    }
                    isProgressShowing.value = false
                    isPullToRefresh.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    val errors = errorMessage as? JsonObject
                    errors?.let {
                        if (errors.has("order_name")) {
                            orderDetail.value = mutableListOf()
                        }
                        isProgressShowing.value = false
                        isPullToRefresh.value = false
                    }
                    if (errors == null) {
                        handleApiResponse(errorMessage as? String)
                    }
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
        }
    }

    fun deleteTableOrder() {
        selectedTableName?.let { table ->
            selectedOrderName?.let { order ->
                isProgressShowing.value = true
                LMPendingOrderRepository.deletePendingOrder(
                    table,
                    order,
                    object : LMApiResponseCallback {
                        override fun <T> onSuccess(data: T) {
                            (data as? LMCommonResponse)?.let {
                                apiResponseMessage.value = it.message
                            }
                            getOrderDetail(false, null)
                        }

                        override fun <T> onError(errorMessage: T) {
                            handleApiResponse(errorMessage as? String)
                        }

                        override fun onFailure(failureMessage: String?) {
                            handleApiResponse(failureMessage)
                        }

                        override fun onNoNetwork() {
                            showNoNetworkToast()
                        }
                    })
            }
        }
    }

    fun editTableMenu(editTableOrder: LMEditTableOrderEvent) {
        orderDetail.value?.let {
            it[selectedTablePosition].let {
                tableOrderItem?.apply {
                    val tableItem = Gson().fromJson<MutableList<LMOrderItemData>>(
                        it.order,
                        object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                    )
                    for (item in tableItem) {
                        if (item.itemId == itemId) {
                            if (editTableOrder.quantity > 0) {
                                item.quantity = editTableOrder.quantity
                                break
                            } else {
                                tableItem.remove(item)
                                break
                            }
                        }
                    }
                    var totalCost = 0F
                    for (item in tableItem) {
                        totalCost += item.quantity * item.price
                    }
                    it.total = totalCost
                    it.order = Gson().toJson(tableItem)
                }
                orderDetail.value = orderDetail.value
            }
        }
    }

    fun getAllSelected() {
        orderDetail.value?.let { orderDetail ->
            orderName?.let {
                completeTable.value = LMOrderResponse().apply {
                    orderName = it
                    val orderOnTable by lazy { mutableListOf<LMOrderItemData>() }
                    for (tableItemData in orderDetail) {
                        if (tableItemData.isExpandedOrChecked) {
                            tableName = tableName?.let {
                                it + "," + tableItemData.tableName
                            } ?: tableItemData.tableName
                            val tableItem = Gson().fromJson<MutableList<LMOrderItemData>>(
                                tableItemData.order,
                                object : TypeToken<MutableList<LMOrderItemData>>() {}.type
                            )
                            for (individualItem in tableItem) {
                                val tableOrderItem by lazy { mutableListOf<LMOrderItemData>() }
                                tableOrderItem.addAll(orderOnTable)
                                if (tableOrderItem.size > 0) {
                                    var isAlreadyAvailable = false
                                    for (itemData in tableOrderItem) {
                                        if (itemData.itemId == individualItem.itemId) {
                                            for (existItem in orderOnTable) {
                                                if (existItem.itemId == individualItem.itemId) {
                                                    existItem.quantity += individualItem.quantity
                                                }
                                            }
                                            isAlreadyAvailable = true
                                        }
                                    }
                                    if (!isAlreadyAvailable) {
                                        orderOnTable.add(individualItem)
                                    }
                                } else {
                                    orderOnTable.add(individualItem)
                                }
                            }
                        }
                    }
                    var totalCost = 0F
                    for (item in orderOnTable) {
                        totalCost += item.quantity * item.price
                    }
                    total = totalCost
                    order = Gson().toJson(orderOnTable)
                }
            }
        }
    }

    fun completeOrder(orderDetailData: LMCompleteOrderEvent) {
        isProgressShowing.value = true
        orderDetailData.orderDetail.let {
            val billOrder = getBillOrder(it)
            LMOrderRepository.createOrderDetail(
                LMCreateOrderRequest(
                    it.order,
                    billOrder.subTotal,
                    billOrder.total,
                    billOrder.discount
                ), object : LMApiResponseCallback {
                    override fun <T> onSuccess(data: T) {
                        (data as? LMCompleteOrderResponse)?.let { bill ->
                            billOrder.billNo = bill.billNo
                            if (orderDetailData.isCompletingOneTable) {
                                LMPendingOrderRepository.deletePendingOrder(
                                    billOrder.tableName,
                                    billOrder.orderName,
                                    object : LMApiResponseCallback {
                                        override fun <T> onSuccess(data: T) {
                                            getOrderDetail(
                                                false,
                                                if (orderDetailData.canPrintOrder) billOrder else null
                                            )
                                            apiResponseMessage.value = bill.message
                                        }

                                        override fun <T> onError(errorMessage: T) {
                                            handleApiResponse(errorMessage as? String)
                                        }

                                        override fun onFailure(failureMessage: String?) {
                                            handleApiResponse(failureMessage)
                                        }

                                        override fun onNoNetwork() {
                                            showNoNetworkToast()
                                        }
                                    }
                                )
                            } else {
                                orderDetail.value?.let {
                                    var orderSize = 0
                                    var deleteditem = 0
                                    for (data in it) {
                                        if (data.isExpandedOrChecked) {
                                            orderSize += 1
                                        }
                                    }
                                    for (data in it) {
                                        if (data.isExpandedOrChecked) {
                                            LMPendingOrderRepository.deletePendingOrder(
                                                data.tableName,
                                                billOrder.orderName,
                                                object : LMApiResponseCallback {
                                                    override fun <T> onSuccess(data: T) {
                                                        deleteditem += 1
                                                        if (deleteditem == orderSize) {
                                                            getOrderDetail(
                                                                false,
                                                                if (orderDetailData.canPrintOrder) billOrder else null
                                                            )
                                                            apiResponseMessage.value = bill.message
                                                        }
                                                    }

                                                    override fun <T> onError(errorMessage: T) {
                                                        handleApiResponse(errorMessage as? String)
                                                    }

                                                    override fun onFailure(failureMessage: String?) {
                                                        handleApiResponse(failureMessage)
                                                    }

                                                    override fun onNoNetwork() {
                                                        showNoNetworkToast()
                                                    }
                                                }
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }

                    override fun <T> onError(errorMessage: T) {
                        handleApiResponse(errorMessage as? String)
                    }

                    override fun onFailure(failureMessage: String?) {
                        handleApiResponse(failureMessage)
                    }

                    override fun onNoNetwork() {
                        showNoNetworkToast()
                    }
                }
            )
        }
    }

    private fun getBillOrder(it: LMOrderResponse) =
        LMOrderDetail().apply {
            orderName = it.orderName
            tableName = it.tableName
            orderItemList = Gson().fromJson<MutableList<LMOrderItemData>>(
                it.order,
                object : TypeToken<MutableList<LMOrderItemData>>() {}.type
            )
            subTotal = LMUtilise.priceFormat.format(it.total)
            billType = LMPdfUtil.CUSTOMER_BILL
            total = LMUtilise.priceFormat.format(it.amount)
            if (it.discount == null || it.discount == 0f) {
                discount = LMUtilise.priceFormat.format(0.00)
                discountApplied = false
            } else {
                discount = LMUtilise.priceFormat.format(it.discount)
                discountApplied = true
            }
        }
}