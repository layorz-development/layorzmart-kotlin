package com.layorz.mealee.order.fastorder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMAddRemoveMenuListener
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_item_table_menu.view.*

class LMFastOrderMenusAdapter(
    private val context: Context,
    private val menuAvailableData: MutableList<LMMenuResponse>,
    private val selectedMenuData: MutableList<LMMenuResponse>,
    private val addRemoveMenuListener: LMAddRemoveMenuListener,
    private val showOnlySelectedItem: Boolean = false
) :
    RecyclerView.Adapter<LMFastOrderMenusAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.lm_item_table_menu, parent, false
        ), addRemoveMenuListener
    )

    override fun getItemCount() = menuAvailableData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            menuAvailableData[adapterPosition].apply {
                checkMenuItem(this).apply {
                    if (!showOnlySelectedItem || itemSelected > 0) {
                        Glide.with(context)
                            .load(itemPreviewUrl) // File of the picture
                            .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(16)))
                            .error(Glide.with(ivPreviewMenu).load(R.mipmap.ic_launcher))
                            .into(ivPreviewMenu)
                        tvMenuName.text = foodName
                        tvPrice.text =  "\u20B9 ${LMUtilise.priceFormat.format(price ?: 0F)}"
                        tvSelectedItemCount.text = itemSelected.toString()
                        ivMinusItem.setOnClickListener {
                            if (itemSelected > 0) {
                                itemSelected -= 1
                                tvSelectedItemCount.text = itemSelected.toString()
                                addRemoveMenuListener.addRemoveMenu(this,adapterPosition)
                                notifyItemChanged(adapterPosition)
                            }
                        }
                        ivAddItem.setOnClickListener {
                            itemSelected += 1
                            tvSelectedItemCount.text = itemSelected.toString()
                            addRemoveMenuListener.addRemoveMenu(this,adapterPosition)
                        }
                        tvType.text = type.name
                    }
                }
            }
        }
    }

    private fun checkMenuItem(checkMenu: LMMenuResponse) = checkMenu.apply {
        for (checkItem in selectedMenuData) {
            if (checkMenu.id != null && checkItem.id != null && checkItem.id == checkMenu.id) {
                checkMenu.itemSelected = checkItem.itemSelected
            }
        }
    }

    class ViewHolder(
        itemView: View,
        val addRemoveMenuListener: LMAddRemoveMenuListener
    ) : RecyclerView.ViewHolder(itemView) {
        val ivPreviewMenu: ImageView = itemView.iv_preview_menu
        val tvMenuName: TextView = itemView.tv_menu_name
        val tvPrice: TextView = itemView.tv_price
        val ivMinusItem: ImageView = itemView.iv_minus_item
        val tvSelectedItemCount: TextView = itemView.tv_selected_item_count
        val ivAddItem: ImageView = itemView.iv_add_item
        val tvType = itemView.tv_type
    }
}