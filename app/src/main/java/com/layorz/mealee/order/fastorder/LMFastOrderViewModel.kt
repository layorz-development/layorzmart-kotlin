package com.layorz.mealee.order.fastorder

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.events.LMCompleteFastOrderEvent
import com.layorz.mealee.common.model.LMMenuFilterData
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMPdfUtil
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.request.LMCreateFastOrderRequest
import com.layorz.mealee.network.request.LMFilterMenuRequest
import com.layorz.mealee.network.response.LMCompleteOrderResponse
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import com.layorz.mealee.repository.LMMenuRepository
import retrofit2.Call
import retrofit2.Response
import java.util.*

class LMFastOrderViewModel(application: Application) : LMViewModel(application) {
    val menuCategoryList by lazy {
        MutableLiveData<MutableList<LMMenuFilterData>>()
    }
    val menuAvailableList by lazy {
        MutableLiveData<MutableList<LMMenuResponse>>()
    }
    val selectedMenuItem by lazy {
        mutableListOf<LMMenuResponse>()
    }
    var printBill=MutableLiveData<Boolean>()
    val menuAvailable by lazy {
        mutableListOf<LMMenuResponse>()
    }
    val createMenu by lazy {
        MutableLiveData<String>()
    }
    val total_cpy by lazy { MutableLiveData<Int>() }
    var total=0
    var discount=0
    var _orderDetail: LiveData<LMOrderDetail>
        get() = orderDetail
        set(value) {}
    var orderDetail = MutableLiveData<LMOrderDetail>()
    val price=MutableLiveData<Int>()
    val menuTypeList by lazy {
        MutableLiveData<LMMenuFilterResponse>()
    }

    var orderDetails: ArrayList<LMCreateFastOrderRequest>? = null
    var orderDetailsJson:String?=null

    fun setDetail(orderDetail: ArrayList<LMCreateFastOrderRequest>,orderDetailJson:String,Total:Int) {
        this.orderDetails = orderDetail
        this.orderDetailsJson=orderDetailJson
        this.total_cpy.value=Total
        this.total=Total
    }
    var choosedMenu: LMOrderDetail? = null
    var allMenuAvailable: MutableList<LMMenuResponse>? = null
    var menuType: Int? = null
    var menuCategory: Int? = null
    var searchQuery: String = ""


    fun updatePrice() {
        var defaultPrice = 0
        for (selectedItem in selectedMenuItem) {
            selectedItem.price.times(selectedItem.itemSelected).let {
                defaultPrice += it
            }
        }
        price.value = defaultPrice
    }
    fun setDiscount(discountData:String){
        if(!discountData.isNullOrEmpty()) {
            var tot=0
            this.total_cpy.value=total
            this.discount=discountData.toInt()
            tot=this.total_cpy.value?.minus(discountData.toInt())!!
            if(tot>0) {
                this.total_cpy.value = this.total.minus(discountData.toInt())
            }else{
                this.total_cpy.value = 0
            }

        }else {
            this.total_cpy.value = this.total
        }
    }

    init {
        getMenuFilterType()
    }

    fun getMenuFilterType() {
        isProgressShowing.value = true
        LMMenuRepository.getMenuFilter(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuTypeList.value = data as LMMenuFilterResponse
                getFilterMenuAvailable(false)
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getFilterMenuAvailable(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMMenuRepository.getMenuList(
            LMFilterMenuRequest().apply {
                withMenuTimings = true
                type = if (menuType != -1) {
                    menuType
                } else {
                    null
                }
                category = if (menuCategory != -1) {
                    menuCategory
                } else {
                    null
                }
            },
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    allMenuAvailable = mutableListOf()
                    (data as? MutableList<LMMenuResponse>)?.let {
                        for (item in it) {
                            if (item.status) {
                                allMenuAvailable?.add(item)
                            }
                        }
                    }
                    getFoodForMenu(searchQuery)
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun getMenuDropDown(menuFilter: List<LMMenuFilterData>?) =
        mutableListOf<String>().apply {
            var loop = 0
            menuFilter?.let {
                while (loop < it.size) {
                    it[loop].name?.let { it1 -> add(it1) }
                    loop += 1
                }
            }
        }

    fun getMenuFilterId(
        filterType: String,
        filterData: List<LMMenuFilterData>?
    ): Int? {
        filterData?.let {
            var loop = 0
            while (loop < it.size) {
                it[loop].name?.let { name ->
                    if (name == filterType) {
                        return it[loop].id
                    }
                }
                loop += 1
            }
        }
        return null
    }

    fun getFoodForMenu(searchQuery: String) {
        this.searchQuery = searchQuery
        val filterData by lazy {
            mutableListOf<LMMenuResponse>()
        }
        allMenuAvailable?.let { it ->
            for (data in it) {
                data.foodName?.let { foodName ->
                    if (foodName.toLowerCase(Locale.getDefault()).contains(searchQuery)) {
                        filterData.add(data)
                    }
                }
            }
        }
        menuAvailableList.value = filterData
    }

    fun getPositionMenu(menuId: Int?, filterData: List<LMMenuFilterData>?): Int {
        menuId?.let { id ->
            var loop = 1
            filterData?.let {
                for (data in it) {
                    data.id?.let {
                        if (it == id) {
                            return loop
                        }
                    }
                    loop += 1
                }
            }
        }
        return 0
    }

    fun completeFastOrder(
        event:LMCompleteFastOrderEvent
    ) {
        var order=event.orderDetails
        isProgressShowing.value=true
        LMApplication.getInstance().apply {
            getApiManager().createOrderDetail(event.orderDetails, object :
                LMRetrofitCallback<LMCompleteOrderResponse> {
                override fun onNoNetwork() {

                }

                override fun onFailure(call: Call<LMCompleteOrderResponse>, t: Throwable) {
                    isProgressShowing.value=false
                    apiResponseMessage.value = t.message

                }

                override fun onResponse(
                    call: Call<LMCompleteOrderResponse>,
                    response: Response<LMCompleteOrderResponse>
                ) {
                    if (response.isSuccessful) {
                        getMenuFilterType()

                        selectedMenuItem.clear()
                        isProgressShowing.value=false
                        price.value = 0
                        apiResponseMessage.value = response.body()?.message
                        if (event.canPrintOrder) {
                            var orderItemData = mutableListOf<LMOrderItemData>()
                            var orderDetails = LMOrderDetail().apply {
                                billNo = response.body()?.billNo
                                billType = LMPdfUtil.CUSTOMER_BILL
                                subTotal = order.subTotal
                                total = order.total
                                discount = order.discount
                                if(order.discount!=""){
                                    discountApplied=true
                                }else discountApplied= false
                                var Order=event.Order
                                for (i in 0 until Order.size){
                                    orderItemData.add(LMOrderItemData().apply {
                                        itemId=Order[i].item_id
                                        itemName=Order[i].item_name
                                        amount=(Order[i].price.toInt()*quantity).toFloat()
                                        categoryId=Order[i].category_id
                                        price=Order[i].price.toInt()
                                        typeId=Order[i].type_id
                                        quantity=Order[i].quantity
                                    })
                                }
                                orderItemList=orderItemData
                            }
                            printBill.value=true
                            orderDetail.value=orderDetails

                        }
                    }
                    if(response.code()==422){
                        isProgressShowing.value=false
                        apiResponseMessage.value="Something went wrong"
                    }
                }
            })

        }
    }

}