package com.layorz.mealee.order.completeorder

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.events.LMCompleteOrderEvent
import com.layorz.mealee.common.events.LMEditTableOrderEvent
import com.layorz.mealee.common.listener.LMDeleteTableOrderListener
import com.layorz.mealee.common.listener.LMEditTableItemListener
import com.layorz.mealee.common.listener.LMSelectTableOrderListener
import com.layorz.mealee.common.listener.LMTakeOrderForTableListener
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.order.tableorder.LMOrderDetailAdapter
import kotlinx.android.synthetic.main.lm_fragment_complete_order.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LMCompleteOrderFragment : LMPermissionFragment(), LMTakeOrderForTableListener,
    LMEditTableItemListener,
    LMSelectTableOrderListener, LMDeleteTableOrderListener {
    private val completeViewModel by lazy {
        ViewModelProvider(this).get(LMCompleteViewModel::class.java)
    }

    companion object {
        private const val DELETE_TABLE_ORDER = "delete_table_order"
        private const val EDIT_TABLE_MENU = "edit_table_menu"
        private const val COMPLETE_ORDER = "complete_order"
        fun newInstance() = LMCompleteOrderFragment()
    }

    private val orderDetailAdapter by lazy {
        LMOrderDetailAdapter(
            currentContext,
            completeViewModel.orderList,
            takeOrderForTableListener = this,
            editTableItemListener = this,
            selectTableOrderListener = this,
            deleteTableOrderListener = this,
            isCompleteOrder = true
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_fragment_complete_order, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun takeOrderForTable(tableName: String) {}

    override fun completeTableOrder(tableOrder: LMOrderResponse) {
        LMCompleteOrderDialogFragment.newInstance(tableOrder, true)
            .show(parentFragmentManager, COMPLETE_ORDER)
    }

    override fun editTableItem(
        parentPosition: Int,
        orderItemData: LMOrderItemData
    ) {
        with(completeViewModel) {
            selectedTablePosition = parentPosition
            tableOrderItem = orderItemData
            LMEditTableMenuDialogFragment.newInstance(orderItemData)
                .show(
                    parentFragmentManager,
                    EDIT_TABLE_MENU
                )
        }
    }

    override fun selectTableOrder(
        tableDetail: LMOrderResponse,
        adapterPosition: Int
    ) {
        var selectedOrder = 0
        for (table in completeViewModel.orderList) {
            if (table.isExpandedOrChecked) {
                selectedOrder += 1
            }
        }
        completeViewModel.selectAllTable = completeViewModel.orderList.size == selectedOrder
        changeCheckboxStatus()
    }

    override fun deleteTableOrder(tableName: String, orderName: String) {
        LMAlertUtil.showAlertDialog(
            fragmentActivity = currentContext as FragmentActivity,
            message = currentContext.getString(R.string.delete_order_table, tableName, orderName),
            title = getString(R.string.delete),
            isCancelNeeded = true,
            alertKey = DELETE_TABLE_ORDER
        )
        with(completeViewModel) {
            selectedOrderName = orderName
            selectedTableName = tableName
        }
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == DELETE_TABLE_ORDER) {
            completeViewModel.deleteTableOrder()
        }
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {}

    private fun initView() {
        setToolbar(getString(R.string.complete_order),resources.configuration.orientation== Configuration.ORIENTATION_PORTRAIT)
        rv_order_list.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = orderDetailAdapter
        }
        changeCheckboxStatus()
    }

    private fun setListener() {
        btn_complete_order.setOnClickListener {
            completeViewModel.getAllSelected()
        }
        srl_order_table_list.setOnRefreshListener {
            completeViewModel.getOrderDetail(false, null)
        }
        cl_all_table_check.setOnClickListener {
            completeViewModel.selectAllTable = !completeViewModel.selectAllTable
            for (order in completeViewModel.orderList) {
                order.isExpandedOrChecked = completeViewModel.selectAllTable
            }
            rv_order_list.post { orderDetailAdapter.notifyDataSetChanged() }
            changeCheckboxStatus()
        }
        acp_order_name.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(completeViewModel) {
                            orderName = it
                            if (orderSelected != position) {
                                completeViewModel.selectAllTable = true
                                orderSelected = position
                                getOrderDetail(true, null)
                            }
                            changeCheckboxStatus()
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
    }

    private fun observerViewModel() {
        with(completeViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                srl_order_table_list.isRefreshing = false
            })
            orderAvailable.observe(viewLifecycleOwner, Observer {
                ArrayAdapter(
                    currentContext,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(0, getString(R.string.select_order))
                        addAll(it)
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_order_name.adapter = this
                    acp_order_name.setSelection(orderSelected)
                }
                if (printBill != null) {
                    createBill(printBill)
                    printBill = null
                }
            })
            orderDetail.observe(viewLifecycleOwner, Observer {
                setAdapter(it)
                iv_no_order_detail.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
                if (printBill != null) {
                    createBill(printBill)
                    printBill = null
                }
            })
            completeTable.observe(viewLifecycleOwner, Observer {
                it?.let {
                    completeTable.value = null
                    if (it.tableName != null) {
                        LMCompleteOrderDialogFragment.newInstance(it, false)
                            .show(parentFragmentManager, COMPLETE_ORDER)
                    } else {
                        Toast.makeText(
                            currentContext,
                            getString(R.string.please_any_table),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })
        }
    }

    private fun setAdapter(orderList: MutableList<LMOrderResponse>) {
        completeViewModel.orderList.apply {
            clear()
            addAll(orderList)
        }
        orderDetailAdapter.notifyDataSetChanged()
    }

    private fun changeCheckboxStatus() {
        iv_all_table_checked.setImageResource(if (completeViewModel.selectAllTable) R.drawable.ic_radio_button_checked else R.drawable.ic_radio_button_unchecked)
        tv_all_table_checked.text =
            getString(if (completeViewModel.selectAllTable) R.string.un_selecte_all else R.string.select_all)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun editTableMenu(editTableOrder: LMEditTableOrderEvent) {
        completeViewModel.editTableMenu(editTableOrder)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun completeOrderDetail(orderDetail: LMCompleteOrderEvent) {
        completeViewModel.completeOrder(orderDetail)
    }
}