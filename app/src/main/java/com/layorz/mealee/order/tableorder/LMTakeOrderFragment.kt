package com.layorz.mealee.order.tableorder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.events.LMChooseTableEvent
import com.layorz.mealee.common.listener.LMDeleteTableOrderListener
import com.layorz.mealee.common.listener.LMEditTableItemListener
import com.layorz.mealee.common.listener.LMSelectTableOrderListener
import com.layorz.mealee.common.listener.LMTakeOrderForTableListener
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.network.response.LMOrderResponse
import com.layorz.mealee.order.choosetable.LMChooseTableDialogFragment
import com.layorz.mealee.order.takemenu.LMTakeOrderTableFragment
import kotlinx.android.synthetic.main.lm_fragment_take_order.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LMTakeOrderFragment : LMFragment(), LMTakeOrderForTableListener, LMEditTableItemListener,
    LMSelectTableOrderListener, LMDeleteTableOrderListener {
    private val tableOrderViewModel by lazy { ViewModelProvider(this).get(LMTableOrderViewModel::class.java) }
    private val orderDetail by lazy { mutableListOf<LMOrderResponse>() }
    private val orderDetailAdapter by lazy {
        LMOrderDetailAdapter(
            currentContext,
            orderDetail,
            takeOrderForTableListener = this,
            editTableItemListener = this,
            selectTableOrderListener = this,
            deleteTableOrderListener = this,
            isEditOption = false
        )
    }

    companion object {
        private const val ORDER_NAME = "order_name"
        private const val TABLE_NAME = "table_name"
        private const val CHOOSE_TABLE = "choose_table"
        private const val DELETE_TABLE_ORDER = "delete_table_order"
        fun newInstance(orderName: String?, tableName: String?) = LMTakeOrderFragment()
            .apply {
                arguments = bundleOf(ORDER_NAME to orderName, TABLE_NAME to tableName)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            arguments?.apply {
                tableOrderViewModel.setOrder(
                    getString(ORDER_NAME, null),
                    getString(TABLE_NAME, null)
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_take_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onResume() {
        super.onResume()
        tableOrderViewModel.getOrderDetail(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun takeOrderForTable(tableName: String) {
        getOrderForTable(tableName)
    }

    override fun completeTableOrder(tableOrder: LMOrderResponse) {

    }

    override fun editTableItem(
        parentPosition: Int,
        orderItemData: LMOrderItemData
    ) {

    }

    override fun selectTableOrder(
        tableDetail: LMOrderResponse,
        adapterPosition: Int
    ) {

    }

    override fun deleteTableOrder(tableName: String, orderName: String) {
        LMAlertUtil.showAlertDialog(
            fragmentActivity = currentContext as FragmentActivity,
            message = currentContext.getString(R.string.delete_order_table, tableName, orderName),
            title = getString(R.string.delete),
            isCancelNeeded = true,
            alertKey = DELETE_TABLE_ORDER
        )
        with(tableOrderViewModel) {
            selectedOrderName = orderName
            selectedTableName = tableName
        }
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == DELETE_TABLE_ORDER) {
            tableOrderViewModel.deleteTableOrder()
        }
    }

    private fun initView() {
        setToolbar(getString(R.string.view_order))
        tv_order_name.text = tableOrderViewModel.orderName
        rv_menu_taken.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = orderDetailAdapter
        }
    }

    private fun setListener() {
        btn_choose_table.setOnClickListener {
            tableOrderViewModel.orderName?.let {
                LMChooseTableDialogFragment.newInstance(it)
                    .show(
                        parentFragmentManager,
                        CHOOSE_TABLE
                    )
            }
        }
        srl_order_detail.setOnRefreshListener {
            tableOrderViewModel.getOrderDetail(false)
        }
    }

    private fun observerViewModel() {
        with(tableOrderViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            isPullToRefresh.observe(viewLifecycleOwner, Observer {
                srl_order_detail.isRefreshing = false
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            orderDetail.observe(viewLifecycleOwner, Observer {
                setAdapter(it ?: mutableListOf())
                iv_order_detail_not_availble.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
        }
    }

    private fun setAdapter(orderDetailList: MutableList<LMOrderResponse>) {
        orderDetail.clear()
        orderDetail.addAll(orderDetailList)
        orderDetailAdapter.notifyDataSetChanged()
    }

    private fun getOrderForTable(tableName: String?) {
        tableName?.let { table ->
            tableOrderViewModel.orderName?.let {
                replaceFragment(
                    LMTakeOrderTableFragment.newInstance(it, table),
                    R.id.fl_navigation,
                    true
                )
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun getTableName(tableDetail: LMChooseTableEvent) {
        getOrderForTable(tableDetail.tableName)
    }
}