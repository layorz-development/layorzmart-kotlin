package com.layorz.mealee.order.completeorder

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.response.LMOrderResponse

class LMCompleteOrderViewModel(application: Application) : LMViewModel(application) {
    val priceEvent by lazy {
        MutableLiveData<Float>()
    }
    var isCompletingOneTable: Boolean = false
    var orderDetail: LMOrderResponse? = null

    fun setDetail(orderDetail: LMOrderResponse, isCompletingOneTable: Boolean) {
        this.orderDetail = orderDetail
        this.isCompletingOneTable = isCompletingOneTable
        orderDetail.amount = orderDetail.total
        priceEvent.value = orderDetail.amount
    }

    fun setDiscount(discountData: String) {
        (discountData.toFloatOrNull()).let { discountDetail ->
            orderDetail?.apply {
                if (discountDetail != null) {
                    amount = total?.let { if (discountDetail > it) 0F else it - discountDetail }
                } else {
                    amount = total
                }
                discount = discountDetail ?: 0F
                priceEvent.value = orderDetail?.amount
            }
        }
    }
}