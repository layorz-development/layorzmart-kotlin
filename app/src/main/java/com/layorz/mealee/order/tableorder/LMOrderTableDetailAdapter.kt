package com.layorz.mealee.order.tableorder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMEditTableItemListener
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMUtilise
import kotlinx.android.synthetic.main.lm_item_table_order_detail.view.*

class LMOrderTableDetailAdapter(
    private val order: MutableList<LMOrderItemData>,
    private val editTableItemListener: LMEditTableItemListener?,
    private val editOption: Boolean = false,
    private val adapterPostion: Int
) :
    RecyclerView.Adapter<LMOrderTableDetailAdapter.LMOrderTableDetailViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        LMOrderTableDetailViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.lm_item_table_order_detail, parent, false), editTableItemListener
        )

    override fun getItemCount() = order.size

    override fun onBindViewHolder(holder: LMOrderTableDetailViewHolder, position: Int) {
        holder.apply {
            val priceFormat = LMUtilise.priceFormat
            order[adapterPosition].apply {
                tvItem.text = itemName
                tvQuantity.text = quantity.toString()
                tvPrice.text = "\u20B9 ${priceFormat.format(price)}"
                tvAmount.text = "\u20B9 ${priceFormat.format(price * quantity)}"
                ivAction.visibility = if (editOption) View.VISIBLE else View.GONE
                ivAction.setOnClickListener {
                    editTableItemListener?.editTableItem(adapterPostion,this)
                }
            }
        }
    }

    class LMOrderTableDetailViewHolder(
        itemView: View,
        val editTableItemListener: LMEditTableItemListener?
    ) : RecyclerView.ViewHolder(itemView) {
        val tvItem: TextView = itemView.tv_item
        val tvQuantity: TextView = itemView.tv_quantity
        val tvPrice: TextView = itemView.tv_price
        val tvAmount: TextView = itemView.tv_amount
        val ivAction: ImageView = itemView.iv_action
    }
}