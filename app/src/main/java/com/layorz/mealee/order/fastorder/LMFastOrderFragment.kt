package com.layorz.mealee.order.fastorder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.events.LMCompleteFastOrderEvent
import com.layorz.mealee.common.listener.LMAddRemoveMenuListener
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.request.LMCreateFastOrderRequest
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_fragment_take_order_table.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class LMFastOrderFragment : LMPermissionFragment(), LMAddRemoveMenuListener {


    companion object {
        private const val PRINT_ORDER = "print_order"
        fun newInstance() = LMFastOrderFragment()
    }

    private val fastOrderViewModel by lazy {
        ViewModelProvider(this).get(LMFastOrderViewModel::class.java)
    }
    private val takeOrderAdapter by lazy {
        LMFastOrderMenusAdapter(
            currentContext,
            fastOrderViewModel.menuAvailable,
            fastOrderViewModel.selectedMenuItem,
            this
        )
    }
    private val selectedMenuAdapter by lazy {
        LMFastOrderMenusAdapter(
            currentContext, fastOrderViewModel.selectedMenuItem,
            mutableListOf(), this, true
        )
    }
    private var menuTypeSetFirst = false
    private var menuCategoryFirst = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_fragment_fast_order, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbar(" Fast Order", true)
        fastOrderViewModel.printBill.value=false
        initView()
        setListener()
        LMLog.d("FastOrder",LMApplication.getInstance().getPreference().authToken!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun addRemoveMenu(menuItem: LMMenuResponse,adapterPosition:Int) {
        with(fastOrderViewModel) {
            if (menuItem.itemSelected > 0) {
                var alreadyExist = true
                for (item in selectedMenuItem) {
                    if (item.id == menuItem.id) {
                        alreadyExist = false
                        item.itemSelected = menuItem.itemSelected
                        break
                    }
                }
                if (alreadyExist) {
                    selectedMenuItem.add(menuItem)
                    selectedMenuAdapter.notifyDataSetChanged()
                } else {
                    selectedMenuAdapter.notifyItemChanged(adapterPosition)
                }
            } else {
                selectedMenuItem.remove(menuItem)
                selectedMenuAdapter.notifyDataSetChanged()
            }
            updatePrice()

        }
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {
        popFragment()
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == PRINT_ORDER) {
            createBill(fastOrderViewModel.choosedMenu)
            popFragment()
        }
    }

    override fun onNegativeButtonClick(alertKey: String) {
        super.onNegativeButtonClick(alertKey)
        if (alertKey == PRINT_ORDER) {
            popFragment()
        }
    }

    private fun initView() {
        rv_selected_item.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = selectedMenuAdapter
        }
        rv_menu_available.apply {
            layoutManager = LinearLayoutManager(currentContext)
            adapter = takeOrderAdapter
        }
    }

    private fun setListener() {
        cb_show_only_selected.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                cl_take_menu.visibility = View.GONE
                rv_selected_item.visibility = View.VISIBLE
                setSelectedItemAdapter()
            } else {
                iv_no_selected_item.visibility = View.GONE
                fastOrderViewModel.menuAvailableList.value =
                    fastOrderViewModel.menuAvailableList.value
                cl_take_menu.visibility = View.VISIBLE
                rv_selected_item.visibility = View.GONE
            }
        }
        btn_place_order.setOnClickListener {
            if (fastOrderViewModel.selectedMenuItem.size > 0) {
                var order = ArrayList<LMCreateFastOrderRequest>()
                for (item in fastOrderViewModel.selectedMenuItem) {

                    order.add(
                        LMCreateFastOrderRequest(
                            item.foodName!!,
                            item.id!!,
                            item.category.id!!,
                            item.type.id!!,
                            item.price.toString(),
                            item.itemSelected
                        )
                    )
                }
                var _order = Gson().toJson(order)
                LMFastOrderDialogFragment.newInstance(_order,fastOrderViewModel.price.value!!).show(parentFragmentManager,"COMPLETE")

            }else {
                Toast.makeText(
                    requireActivity().applicationContext,
                    "Please select any one menu atleast",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        et_menu_search.setOnClickListener {
            et_menu_search.isFocusableInTouchMode = true
            et_menu_search.isFocusable = true
        }
        et_menu_search.addTextChangedListener { searchQuery ->
            fastOrderViewModel.getFoodForMenu(searchQuery.toString())
        }
        iv_reload_menu_filter.setOnClickListener {
            fastOrderViewModel.getMenuFilterType()
        }
        iv_reload_menu_available.setOnClickListener {
            fastOrderViewModel.getFilterMenuAvailable(true)
        }
        acp_menu_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuTypeSetFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(fastOrderViewModel) {
                            menuCategory = getMenuFilterId(it, menuTypeList.value?.category)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuTypeSetFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        acp_menu_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuCategoryFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(fastOrderViewModel) {
                            menuType = getMenuFilterId(it, menuTypeList.value?.type)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuCategoryFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun setSelectedItemAdapter() {
        val selectedMenuItem = fastOrderViewModel.selectedMenuItem
        iv_no_selected_item.visibility = if (selectedMenuItem.size > 0) View.GONE else View.VISIBLE
        selectedMenuAdapter.notifyDataSetChanged()
    }

    private fun observerViewModel() {
        with(fastOrderViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            menuAvailableList.observe(viewLifecycleOwner, Observer {
                menuAvailable.clear()
                menuAvailable.addAll(it)
                takeOrderAdapter.notifyDataSetChanged()
                iv_no_data.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
            price.observe(viewLifecycleOwner, Observer {
                if (it != 0) {
                    btn_place_order.text =
                        "${getString(R.string.place_order)} \u20B9 ${LMUtilise.priceFormat.format(
                            it ?: 0
                        )}"
                } else {
                    btn_place_order.text =
                        "${getString(R.string.place_order)}"
                }


            })

            _orderDetail.observe(viewLifecycleOwner, Observer {
                if(printBill.value!!) {
                    createBill(it)
                }
            })
            createMenu.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                    LMAlertUtil.showAlertDialog(
                        fragmentActivity = currentContext as FragmentActivity,
                        message = getString(R.string.print_order),
                        title = getString(R.string.info),
                        isCancelNeeded = true,
                        alertKey = PRINT_ORDER
                    )
                    createMenu.value = null
                }
            })
            menuTypeList.observe(viewLifecycleOwner, Observer {
                it?.let {
                    setDropDownData(it)
                }
            })
        }
    }

    private fun setDropDownData(menuFilter: LMMenuFilterResponse) {
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner_dropdown,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(fastOrderViewModel.getMenuDropDown(menuFilter.type))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_type.adapter = this
            menuTypeSetFirst = false
            acp_menu_type.setSelection(
                fastOrderViewModel.getPositionMenu(
                    fastOrderViewModel.menuType,
                    menuFilter.type
                )
            )
        }
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(fastOrderViewModel.getMenuDropDown(menuFilter.category))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_category.adapter = this
            menuCategoryFirst = false
            acp_menu_category.setSelection(
                fastOrderViewModel.getPositionMenu(
                    fastOrderViewModel.menuCategory,
                    menuFilter.category
                )
            )
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun completeFastOrderDetail(orderDetail: LMCompleteFastOrderEvent) {
        fastOrderViewModel.completeFastOrder(orderDetail)
    }

}