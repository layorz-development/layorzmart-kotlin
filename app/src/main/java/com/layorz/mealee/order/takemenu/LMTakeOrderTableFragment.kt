package com.layorz.mealee.order.takemenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMPermissionFragment
import com.layorz.mealee.common.listener.LMAddRemoveMenuListener
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.navigation.LMNavigationActivity
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import kotlinx.android.synthetic.main.lm_fragment_take_order_table.*


class LMTakeOrderTableFragment : LMPermissionFragment(), LMAddRemoveMenuListener {
    private val takeOrderTableViewModel by lazy {
        ViewModelProvider(this).get(LMTakeOrderTableViewModel::class.java)
    }
    private val takeOrderAdapter by lazy {
        LMTakeMenuItemAdapter(
            currentContext,
            takeOrderTableViewModel.menuAvailable,
            takeOrderTableViewModel.selectedMenuItem,
            this
        )
    }
    private val selectedMenuAdapter by lazy {
        LMTakeMenuItemAdapter(
            currentContext, takeOrderTableViewModel.selectedMenuItem,
            mutableListOf(), this, true
        )
    }
    private var menuTypeSetFirst = false
    private var menuCategoryFirst = false

    companion object {
        private const val PRINT_ORDER = "print_order"
        fun newInstance(orderName: String, tableName: String) = LMTakeOrderTableFragment()
            .apply {
                arguments = bundleOf(
                    LMNavigationActivity.ORDER_DETAIL to orderName,
                    LMNavigationActivity.TABLE_DETAIL to tableName
                )
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            arguments?.apply {
                takeOrderTableViewModel.setOrderDetail(
                    getString(LMNavigationActivity.ORDER_DETAIL, null),
                    getString(LMNavigationActivity.TABLE_DETAIL, null)
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_fragment_take_order_table, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun addRemoveMenu(
        menuItem: LMMenuResponse,
        adapterPosition: Int
    ) {
        with(takeOrderTableViewModel) {
            if (menuItem.itemSelected > 0) {
                var alreadyExist = true
                for (item in selectedMenuItem) {
                    if (item.id == menuItem.id) {
                        alreadyExist = false
                        item.itemSelected = menuItem.itemSelected
                        break
                    }
                }
                if (alreadyExist) {
                    selectedMenuItem.add(menuItem)
                    selectedMenuAdapter.notifyDataSetChanged()
                } else {
                    selectedMenuAdapter.notifyItemChanged(adapterPosition)
                }
            } else {
                selectedMenuItem.remove(menuItem)
                selectedMenuAdapter.notifyDataSetChanged()
            }
            updatePrice()
        }
    }

    override fun imageUrl(imagePath: String) {}

    override fun completeFragement() {
        popFragment()
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == PRINT_ORDER) {
            createBill(takeOrderTableViewModel.choosedMenu)
            popFragment()
        }
    }

    override fun onNegativeButtonClick(alertKey: String) {
        super.onNegativeButtonClick(alertKey)
        if (alertKey == PRINT_ORDER) {
            popFragment()
        }
    }

    private fun initView() {
        takeOrderTableViewModel.orderName?.let {
            setToolbar(it)
        }
        tv_table_name.text = takeOrderTableViewModel.tableName
        rv_selected_item.apply {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    layoutManager =
                        GridLayoutManager(currentContext, LMUtilise.numberOfMenuInList(currentContext,width))
                }
            })
            adapter = selectedMenuAdapter
        }
        rv_menu_available.apply {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    layoutManager =
                        GridLayoutManager(currentContext, LMUtilise.numberOfMenuInList(currentContext,width))
                }
            })
            adapter = takeOrderAdapter
        }
    }

    private fun setListener() {
        cb_show_only_selected.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                cl_take_menu.visibility = View.GONE
                rv_selected_item.visibility = View.VISIBLE
                setSelectedItemAdapter()
            } else {
                iv_no_selected_item.visibility = View.GONE
                takeOrderTableViewModel.menuAvailableList.value =
                    takeOrderTableViewModel.menuAvailableList.value
                cl_take_menu.visibility = View.VISIBLE
                rv_selected_item.visibility = View.GONE
            }
        }
        btn_place_order.setOnClickListener {
            takeOrderTableViewModel.makeOrderForTable()
        }
        et_menu_search.setOnClickListener {
            et_menu_search.isFocusableInTouchMode = true
            et_menu_search.isFocusable = true
        }
        et_menu_search.addTextChangedListener { searchQuery ->
            takeOrderTableViewModel.getFoodForMenu(searchQuery.toString())
        }
        iv_reload_menu_filter.setOnClickListener {
            takeOrderTableViewModel.getMenuFilterType()
        }
        iv_reload_menu_available.setOnClickListener {
            takeOrderTableViewModel.getFilterMenuAvailable(true)
        }
        acp_menu_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuTypeSetFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(takeOrderTableViewModel) {
                            menuCategory = getMenuFilterId(it, menuTypeList.value?.category)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuTypeSetFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        acp_menu_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (menuCategoryFirst) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        with(takeOrderTableViewModel) {
                            menuType = getMenuFilterId(it, menuTypeList.value?.type)
                            getFilterMenuAvailable(true)
                        }
                    }
                }
                menuCategoryFirst = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun setSelectedItemAdapter() {
        val selectedMenuItem = takeOrderTableViewModel.selectedMenuItem
        iv_no_selected_item.visibility = if (selectedMenuItem.size > 0) View.GONE else View.VISIBLE
        selectedMenuAdapter.notifyDataSetChanged()
    }

    private fun observerViewModel() {
        with(takeOrderTableViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                it?.let {
                    updateProgress(it)
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            menuAvailableList.observe(viewLifecycleOwner, Observer {
                menuAvailable.clear()
                menuAvailable.addAll(it)
                takeOrderAdapter.notifyDataSetChanged()
                iv_no_data.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
            price.observe(viewLifecycleOwner, Observer {
                btn_place_order.text =
                    "${getString(R.string.place_order)} \u20B9 ${LMUtilise.priceFormat.format(it ?: 0)}"
            })
            createMenu.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_SHORT).show()
                    LMAlertUtil.showAlertDialog(
                        fragmentActivity = currentContext as FragmentActivity,
                        message = getString(R.string.print_order),
                        title = getString(R.string.info),
                        isCancelNeeded = true,
                        alertKey = PRINT_ORDER
                    )
                    createMenu.value = null
                }
            })
            menuTypeList.observe(viewLifecycleOwner, Observer {
                it?.let {
                    setDropDownData(it)
                }
            })
        }
    }

    private fun setDropDownData(menuFilter: LMMenuFilterResponse) {
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner_dropdown,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(takeOrderTableViewModel.getMenuDropDown(menuFilter.type))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_type.adapter = this
            menuTypeSetFirst = false
            acp_menu_type.setSelection(
                takeOrderTableViewModel.getPositionMenu(
                    takeOrderTableViewModel.menuType,
                    menuFilter.type
                )
            )
        }
        ArrayAdapter(
            currentContext,
            R.layout.lm_item_spinner,
            mutableListOf<String>().apply {
                add(getString(R.string.all))
                addAll(takeOrderTableViewModel.getMenuDropDown(menuFilter.category))
            }
        ).apply {
            setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
            acp_menu_category.adapter = this
            menuCategoryFirst = false
            acp_menu_category.setSelection(
                takeOrderTableViewModel.getPositionMenu(
                    takeOrderTableViewModel.menuCategory,
                    menuFilter.category
                )
            )
        }
    }
}