package com.layorz.mealee.order.choosetable

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMDialogFragment
import com.layorz.mealee.common.events.LMChooseTableEvent
import com.layorz.mealee.common.listener.LMSelectedTableItemListener
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.table.LMTableOrderItemAdapter
import kotlinx.android.synthetic.main.lm_dialog_choose_table.*
import org.greenrobot.eventbus.EventBus

class LMChooseTableDialogFragment : LMDialogFragment(), LMSelectedTableItemListener {
    private val chooseTableViewModel by lazy {
        ViewModelProvider(this).get(LMChooseTableViewModel::class.java)
    }

    private val tableAvailableList by lazy { mutableListOf<LMTableListData>() }

    private val tableItemAdapter by lazy {
        LMTableOrderItemAdapter(
            currentContext,
            tableItem = tableAvailableList, orderClickListener = null,
            selectedTableItemListener = this, tableOrOrder = true, isTableDeleteOptionNeeded = false
        )
    }

    companion object {
        private const val ORDER_NAME = "order_name"
        fun newInstance(orderName: String) = LMChooseTableDialogFragment().apply {
            arguments = bundleOf(ORDER_NAME to orderName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            chooseTableViewModel.setOrderName(getString(ORDER_NAME, null))
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(currentContext).apply {
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.lm_dialog_choose_table, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerViewModel()
    }

    override fun selectTable(
        tableDetail: LMTableListData,
        adapterPosition: Int
    ) {
        tableDetail.tableName?.let {
            EventBus.getDefault().post(LMChooseTableEvent().apply { tableName = it })
            dismiss()
        }
    }

    private fun initView() {
        tv_order_name.text = chooseTableViewModel.orderNameForTable
        rv_choose_table.apply {
            layoutManager = GridLayoutManager(currentContext, 4)
            adapter = tableItemAdapter
        }
    }

    private fun setListener() {
        iv_close_table.setOnClickListener {
            dismiss()
        }
    }

    private fun observerViewModel() {
        with(chooseTableViewModel) {
            isProgressShowing.observe(viewLifecycleOwner, Observer {
                updateProgress(it)
            })
            apiResponseInId.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, getString(it), Toast.LENGTH_LONG).show()
                }
            })
            apiResponseMessage.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Toast.makeText(currentContext, it, Toast.LENGTH_LONG).show()
                }
            })
            tableAvailable.observe(viewLifecycleOwner, Observer {
                setAdapter(it ?: mutableListOf())
                iv_no_table_found.visibility =
                    if (it != null && it.size > 0) View.GONE else View.VISIBLE
            })
        }
    }

    private fun setAdapter(tableAvailable: MutableList<LMTableListData>) {
        tableAvailableList.clear()
        tableAvailableList.addAll(tableAvailable)
        tableItemAdapter.notifyDataSetChanged()
    }
}