package com.layorz.mealee.order.choosetable

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.repository.LMTableRepository

class LMChooseTableViewModel(application: Application) : LMViewModel(application) {
    val tableAvailable by lazy {
        MutableLiveData<MutableList<LMTableListData>>()
    }
    var orderNameForTable: String? = null

    init {
        getTableName()
    }

    fun setOrderName(orderName: String?) {
        orderNameForTable = orderName
    }

    private fun getTableName() {
        isProgressShowing.value = true
        LMTableRepository.getAllTableList(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                tableAvailable.value = data as MutableList<LMTableListData>
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}