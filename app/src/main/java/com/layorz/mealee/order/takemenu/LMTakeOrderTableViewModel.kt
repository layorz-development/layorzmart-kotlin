package com.layorz.mealee.order.takemenu

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.layorz.mealee.R
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMMenuFilterData
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.model.LMOrderItemData
import com.layorz.mealee.common.utils.LMPdfUtil
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMFilterMenuRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import com.layorz.mealee.repository.LMMenuRepository
import com.layorz.mealee.repository.LMPendingOrderRepository
import java.util.*

class LMTakeOrderTableViewModel(application: Application) : LMViewModel(application) {
    val menuCategoryList by lazy {
        MutableLiveData<MutableList<LMMenuFilterData>>()
    }
    val menuAvailableList by lazy {
        MutableLiveData<MutableList<LMMenuResponse>>()
    }
    val selectedMenuItem by lazy {
        mutableListOf<LMMenuResponse>()
    }
    val menuAvailable by lazy {
        mutableListOf<LMMenuResponse>()
    }
    val createMenu by lazy {
        MutableLiveData<String>()
    }
    val price by lazy { MutableLiveData<Int>() }
    val menuTypeList by lazy {
        MutableLiveData<LMMenuFilterResponse>()
    }

    var orderName: String? = null
    var tableName: String? = null
    var choosedMenu: LMOrderDetail? = null
    var allMenuAvailable: MutableList<LMMenuResponse>? = null
    var menuType: Int? = null
    var menuCategory: Int? = null
    var searchQuery: String = ""

    fun setOrderDetail(orderName: String?, tableName: String) {
        this.orderName = orderName
        this.tableName = tableName
    }

    fun updatePrice() {
        var defaultPrice = 0
        for (selectedItem in selectedMenuItem) {
            selectedItem.price.times(selectedItem.itemSelected).let {
                defaultPrice += it
            }
        }
        price.value = defaultPrice
    }

    fun makeOrderForTable() {
        price.value?.let {
            if (it > 0) {
                isProgressShowing.value = true
                choosedMenu = LMOrderDetail().apply {
                    orderName = this@LMTakeOrderTableViewModel.orderName
                    tableName = this@LMTakeOrderTableViewModel.tableName
                    orderItemList = getItemChoose()
                    billType = LMPdfUtil.KITCHEN_BILL
                }
                LMPendingOrderRepository.createNewPendingOrder(
                    tableName,
                    orderName,
                    getItemChooseInGson(),
                    object : LMApiResponseCallback {
                        override fun <T> onSuccess(data: T) {
                            (data as? LMCommonResponse)?.let {
                                createMenu.value = it.message
                            }
                            isProgressShowing.value = false
                        }

                        override fun <T> onError(errorMessage: T) {
                            handleApiResponse(errorMessage as? String)
                        }

                        override fun onFailure(failureMessage: String?) {
                            handleApiResponse(failureMessage)
                        }

                        override fun onNoNetwork() {
                            showNoNetworkToast()
                        }
                    })
            } else {
                apiResponseInId.value = R.string.please_any_menu
            }
        }
        if (price.value == null) {
            apiResponseInId.value = R.string.please_any_menu
        }
    }

    private fun getItemChooseInGson() = Gson().toJson(getItemChoose())

    private fun getItemChoose() = mutableListOf<LMOrderItemData>().apply {
        for (menuChoose in selectedMenuItem) {
            menuChoose.let {
                add(LMOrderItemData().apply {
                    itemId = it.id ?: 0
                    itemName = it.foodName
                    quantity = it.itemSelected
                    amount = (it.itemSelected * it.price).toFloat()
                    price = it.price
                    it.category.id?.let {
                        categoryId = it
                    }
                    it.type.id?.let {
                        typeId = it
                    }
                })
            }
        }
    }

    init {
        getMenuFilterType()
    }

    fun getMenuFilterType() {
        isProgressShowing.value = true
        LMMenuRepository.getMenuFilter(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                menuTypeList.value = data as LMMenuFilterResponse
                getFilterMenuAvailable(false)
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getFilterMenuAvailable(isProgressNeed: Boolean) {
        if (isProgressNeed) {
            isProgressShowing.value = true
        }
        LMMenuRepository.getMenuList(LMFilterMenuRequest().apply {
            withMenuTimings = true
            type = if (menuType != -1) {
                menuType
            } else {
                null
            }
            category = if (menuCategory != -1) {
                menuCategory
            } else {
                null
            }
        },
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    allMenuAvailable = mutableListOf()
                    (data as? MutableList<LMMenuResponse>)?.let {
                        for (item in it) {
                            if (item.status) {
                                allMenuAvailable?.add(item)
                            }
                        }
                    }
                    getFoodForMenu(searchQuery)
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as? String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun getMenuDropDown(menuFilter: List<LMMenuFilterData>?) =
        mutableListOf<String>().apply {
            var loop = 0
            menuFilter?.let {
                while (loop < it.size) {
                    it[loop].name?.let { it1 -> add(it1) }
                    loop += 1
                }
            }
        }

    fun getMenuFilterId(
        filterType: String,
        filterData: List<LMMenuFilterData>?
    ): Int? {
        filterData?.let {
            var loop = 0
            while (loop < it.size) {
                it[loop].name?.let { name ->
                    if (name == filterType) {
                        return it[loop].id
                    }
                }
                loop += 1
            }
        }
        return null
    }

    fun getFoodForMenu(searchQuery: String) {
        this.searchQuery = searchQuery
        val filterData by lazy {
            mutableListOf<LMMenuResponse>()
        }
        allMenuAvailable?.let { it ->
            for (data in it) {
                data.foodName?.let { foodName ->
                    if (foodName.toLowerCase(Locale.getDefault()).contains(searchQuery)) {
                        filterData.add(data)
                    }
                }
            }
        }
        menuAvailableList.value = filterData
    }

    fun getPositionMenu(menuId: Int?, filterData: List<LMMenuFilterData>?): Int {
        menuId?.let { id ->
            var loop = 1
            filterData?.let {
                for (data in it) {
                    data.id?.let {
                        if (it == id) {
                            return loop
                        }
                    }
                    loop += 1
                }
            }
        }
        return 0
    }
}