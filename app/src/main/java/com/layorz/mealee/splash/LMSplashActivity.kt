package com.layorz.mealee.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.dashboard.activities.LMDashboardActivity
import com.layorz.mealee.login.LMLoginActivity
import com.layorz.mealee.other.LMSubscriptionActivity

class LMSplashActivity : LMActivity() {
    companion object {
        private const val SPLASH_TIME: Long = 3000
        private const val ACTIVE="active"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lm_activity_splash)
        if (savedInstanceState == null) {
            Handler().postDelayed({ switchToDestination() }, SPLASH_TIME)
        }
    }

    private fun switchToDestination() {
        if(LMApplication.getInstance().getPreference().subscriptionStatus==ACTIVE) {
            LMNavigation.switchActivity(
                this,
                LMApplication.getInstance()
                    .getPreference().userData?.let { LMDashboardActivity::class.java }
                    ?: LMLoginActivity::class.java,
                flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }else{
            LMNavigation.switchActivity(
                this,
                LMSubscriptionActivity::class.java,
                flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }
}
