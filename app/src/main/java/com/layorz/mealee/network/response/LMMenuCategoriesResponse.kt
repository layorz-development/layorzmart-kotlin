package com.layorz.mealee.network.response

data class LMMenuCategoriesResponse(var id:Int, var name:String)