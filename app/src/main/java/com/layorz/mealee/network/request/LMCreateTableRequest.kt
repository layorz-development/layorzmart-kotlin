package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMCreateTableRequest(
    @SerializedName("name")
    @Expose
    private var tableName: String? = null
) {
}