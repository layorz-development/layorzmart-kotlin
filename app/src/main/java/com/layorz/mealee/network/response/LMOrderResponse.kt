package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMOrderItemData

class LMOrderResponse {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("bill_no")
    @Expose
    var billNo: String? = null

    @SerializedName("table_name")
    @Expose
    var tableName: String? = null

    @SerializedName("order_name")
    @Expose
    var orderName: String? = null

    @SerializedName("order")
    @Expose
    var order: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("time")
    @Expose
    var time: String? = null

    @SerializedName("discount")
    @Expose
    var discount: Float? = null

    @SerializedName("sub_total")
    @Expose
    var total: Float? = null

    @SerializedName("total")
    @Expose
    var amount: Float? = null

    var isExpandedOrChecked = false
}