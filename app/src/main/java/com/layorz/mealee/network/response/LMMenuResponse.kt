package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMMenuFilterData
import com.layorz.mealee.common.model.LMMenuTimeOfDayData

class LMMenuResponse {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("food_name")
    @Expose
    var foodName: String? = null

    @SerializedName("category")
    @Expose
    var category: LMMenuFilterData = LMMenuFilterData()

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("type")
    @Expose
    var type: LMMenuFilterData = LMMenuFilterData()

    @SerializedName("status")
    @Expose
    var status: Boolean = true

    @SerializedName("time_of_day")
    @Expose
    var timeOfDay: LMMenuTimeOfDayData = LMMenuTimeOfDayData()

    @SerializedName("price")
    @Expose
    var price: Int = 0

    @SerializedName("image")
    @Expose
    var itemPreviewUrl: String? = null

    var itemSelected = 0
}