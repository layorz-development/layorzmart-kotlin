package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.layorz.mealee.BuildConfig

class LMAppVersionRequest {
    @SerializedName("app_version")
    @Expose
    val appVersion = BuildConfig.VERSION_NAME
}