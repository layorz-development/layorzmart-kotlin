package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMFeedbackRequest(
    @SerializedName("title")
    @Expose
    val title: String? = null,
    @SerializedName("type")
    @Expose
    val type: String? = null,
    @SerializedName("description")
    @Expose
    var description: String? = null
) {
}