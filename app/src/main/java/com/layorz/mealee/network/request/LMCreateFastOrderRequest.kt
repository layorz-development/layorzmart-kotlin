package com.layorz.mealee.network.request

data class LMCreateFastOrderRequest(var item_name:String, var item_id:Int, var category_id:Int, var type_id:Int, var price:String, var quantity:Int)
