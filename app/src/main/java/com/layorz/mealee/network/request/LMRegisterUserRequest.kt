package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMRegisterUserRequest {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("password_confirmation")
    @Expose
    var confirmPassword: String? = null

    @SerializedName("company_name")
    @Expose
    var companyName: String? = null

    @SerializedName("address_line_1")
    @Expose
    var addressLine1: String? = null

    @SerializedName("address_line_2")
    @Expose
    var addressLine2: String? = null

    @SerializedName("sub_city")
    @Expose
    var subCity: Int? = null

    @SerializedName("city")
    @Expose
    var city: Int? = null

    @SerializedName("state")
    @Expose
    var state: Int? = null

    @SerializedName("country")
    @Expose
    var country: Int? = null

    @SerializedName("phone_no")
    @Expose
    var phoneNo: Int? = null

    @SerializedName("bill_slug")
    @Expose
    var billSlug: String? = null

    @SerializedName("pincode")
    @Expose
    var pinCode:String?=null
}