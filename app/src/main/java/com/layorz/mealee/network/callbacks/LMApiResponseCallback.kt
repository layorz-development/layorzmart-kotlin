package com.layorz.mealee.network.callbacks

interface LMApiResponseCallback {
    fun <T> onSuccess(data: T)

    fun <T> onError(errorMessage: T)

    fun onFailure(failureMessage: String?)

    fun onNoNetwork()
}