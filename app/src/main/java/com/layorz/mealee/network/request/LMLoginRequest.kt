package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMLoginRequest(
    @SerializedName("email")
    @Expose
    private var email: String?=null,
    @SerializedName("password")
    @Expose
    private var password: String?=null
) {
}