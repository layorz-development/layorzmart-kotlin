package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMDeletePendingOrderRequest(
    @SerializedName("table_name")
    @Expose
    private val tableName: Int?=null,

    @SerializedName("order_name")
    @Expose
    private val orderName: String?=null
) {
}