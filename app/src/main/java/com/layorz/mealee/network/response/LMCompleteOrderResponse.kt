package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMCompleteOrderResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("bill_no")
    @Expose
    var billNo: String? = null
}