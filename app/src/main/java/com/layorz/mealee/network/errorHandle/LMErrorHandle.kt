package com.layorz.mealee.network.errorHandle

import android.content.res.Resources
import com.google.gson.JsonParser
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.network.manager.LMApiManager
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit

object LMErrorHandle {
    private val TAG = LMErrorHandle::class.java.simpleName
    private const val ERROR_CODE_422 = 422
    private val resources: Resources = LMApplication.getInstance().resources

    /**
     * Method which parse the json error response to java error response object and based on error code returns the required string
     *
     * @param response           error response
     * @param isAuthorisedClient boolean to represent whether authorised or unauthorised client
     * @return error response message
     */
    fun parseError(
        response: Response<*>,
        isAuthorisedClient: Boolean
    ): Any? {
        try {
            val retrofitClient: Retrofit = if (isAuthorisedClient) {
                LMApiManager.getAuthorisedRetrofitClient() //Here serializable null not needed so direct false set
            } else {
                LMApiManager.getUnAuthorisedRetrofitClient()
            }
            if (response.code() == ERROR_CODE_422) {
                response.errorBody()?.let {
                    val errorBody = JsonParser().parse(it.string()).asJsonObject
                    if (errorBody.has(LMConstant.KEY_ERRORS))
                        return errorBody.getAsJsonObject(LMConstant.KEY_ERRORS)
                }
            } else {
                val converter: Converter<ResponseBody?, LMErrorResponse> =
                    retrofitClient.responseBodyConverter(
                        LMErrorResponse::class.java,
                        arrayOfNulls(0)
                    )
                val errorBody = response.errorBody()
                if (errorBody != null) {
                    val errorResponse: LMErrorResponse? = converter.convert(errorBody)
                    errorResponse?.message?.let { return it }
                }
            }
        } catch (e: Exception) {
            LMLog.e(TAG, "getErrorResponse: Caught exception: " + e.message, e)
        }
        return resources.getString(R.string.something_went_wrong)
    }
}

class LMErrorResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null
}
