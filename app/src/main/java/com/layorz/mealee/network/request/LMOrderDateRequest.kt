package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMOrderDateRequest(
    @SerializedName("date")
    @Expose
    private var date: String? = null
) {
}