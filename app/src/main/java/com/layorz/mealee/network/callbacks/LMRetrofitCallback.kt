package com.layorz.mealee.network.callbacks

import retrofit2.Callback

interface LMRetrofitCallback<T> : Callback<T> {
    fun onNoNetwork()
}