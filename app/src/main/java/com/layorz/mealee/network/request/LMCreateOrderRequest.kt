package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMOrderItemData

class LMCreateOrderRequest(
    @SerializedName("order")
    @Expose
    var order: String? = null,

    @SerializedName("sub_total")
    @Expose
    var subTotal: String? = null,

    @SerializedName("total")
    @Expose
    var total: String? = null,

    @SerializedName("discount")
    @Expose
    var discount: String? = null
) {
}