package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMSettingTimeData


class LMSettingDetail {
    @SerializedName("apply")
    @Expose
    var settingNeed: Boolean = false

    @SerializedName("morning")
    @Expose
    var morning: LMSettingTimeData? = null

    @SerializedName("afternoon")
    @Expose
    var afternoon: LMSettingTimeData? = null

    @SerializedName("evening")
    @Expose
    var evening: LMSettingTimeData? = null

    @SerializedName("night")
    @Expose
    var night: LMSettingTimeData? = null
}