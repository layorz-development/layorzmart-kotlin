package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMSalesReportResponse {
    @SerializedName("order_details")
    @Expose
    var orderDetails: Map<String, String>? = null

    @SerializedName("bill_details")
    @Expose
    var billDetails: Map<String, String>? = null

    @SerializedName("items_count")
    @Expose
    var itemsCount: Map<String, String>? = null

    @SerializedName("category_count")
    @Expose
    var categoryCount: Map<String, String>? = null

    @SerializedName("type_count")
    @Expose
    var typeCount: Map<String, String>? = null
}