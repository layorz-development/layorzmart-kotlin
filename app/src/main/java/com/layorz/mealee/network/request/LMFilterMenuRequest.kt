package com.layorz.mealee.network.request

class LMFilterMenuRequest {
    var withMenuTimings: Boolean = true
    var type: Int? = null
    var category: Int? = null
}