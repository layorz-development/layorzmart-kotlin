package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMCommonResponse{
    @SerializedName("message")
    @Expose
    var message: String? = null
}