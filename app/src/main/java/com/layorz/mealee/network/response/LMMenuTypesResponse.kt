package com.layorz.mealee.network.response

data class LMMenuTypesResponse(var id:Int, var name:String)