package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMOrderItemData

class LMNewPendingOrderRequest(
    @SerializedName("table_name")
    @Expose
    private val tableName: String? = null,
    @SerializedName("order_name")
    @Expose
    private val orderName: String? = null,
    @SerializedName("order")
    @Expose
    private val order: List<LMOrderItemData>? = null
) {
}