package com.layorz.mealee.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMMenuVisibilityChangeRequest(
    @SerializedName("status")
    @Expose
    val status: Boolean = false
)