package com.layorz.mealee.network.response

data class MenuTypesAndCategoriesResponse (var type:ArrayList<LMMenuTypesResponse>, var category:ArrayList<LMMenuCategoriesResponse>)

