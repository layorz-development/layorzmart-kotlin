package com.layorz.mealee.network.manager

import com.google.gson.GsonBuilder
import com.layorz.mealee.BuildConfig
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.events.LMLogoutUserEvent
import com.layorz.mealee.common.events.LMSubscriptionEndEvent
import com.layorz.mealee.common.model.LMPlaceList
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.common.utils.LMLocaleUtil
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.request.*
import com.layorz.mealee.network.response.*
import com.layorz.mealee.repository.LMUserRepository
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.IOException
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit

class LMApiManager {
    companion object {
        const val BEARER = "Bearer "
        private const val REFRESH_TOKEN_KEY = "refresh_token"
        private const val KEY_ACCEPT_TYPE = "Accept"
        private const val KEY_CONTENT_TYPE = "content-type"
        private const val KEY_REQUESTED_WITH = "X-Requested-with"
        private const val KEY_XML_HTTP_REQUEST = "XMLHttpRequest"
        private const val APPLICATION_JSON = "application/json"
        private const val AUTHORISATION_KEY = "Authorization"
        private const val KEY_CONTENT_LANGUAGE = "content-language"
        private const val UNAUTHORISED_CODE = 401
        private const val TIMEOUT: Long = 30
        private lateinit var unAuthorisedClient: Retrofit
        private lateinit var authorisedClient: Retrofit
        private lateinit var refreshTokenClient: Retrofit
        private const val SUBSCRIPTION_ACTIVE="subscription_active"
        private const val ACTIVE="active"

        private val authorizedOkHttpClient by lazy {
            generateOkHttpClient(
                isAuthorizedClient = true,
                addRefresh = false
            )
        }

        private val unAuthorisedOkHttpClient by lazy {
            generateOkHttpClient(
                isAuthorizedClient = false,
                addRefresh = false
            )
        }

        private val refreshOkHttpClient by lazy {
            generateOkHttpClient(
                isAuthorizedClient = false,
                addRefresh = true
            )
        }

        private var isRefreshTokenGoingOn = false
        private val pendingRequests =
            CopyOnWriteArrayList<Request>()

        private fun generateOkHttpClient(
            isAuthorizedClient: Boolean,
            addRefresh: Boolean
        ): OkHttpClient {
            val builder = OkHttpClient().newBuilder()
            if (isAuthorizedClient) {
                builder.addInterceptor(LMAuthorizationInterceptor())
            }
            return builder.readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor { chain ->
                    chain.proceed(
                        getRequest(
                            chain.request(),
                            isAuthorizedClient,
                            addRefresh
                        )
                    )
                }
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
        }

        @Throws(IOException::class)
        fun getRequest(
            request: Request,
            isAuthorizedClient: Boolean,
            addRefresh: Boolean
        ) = request.newBuilder().apply {
            removeHeader(AUTHORISATION_KEY)
            if (request.headers[KEY_ACCEPT_TYPE] == null) {
                addHeader(KEY_ACCEPT_TYPE, APPLICATION_JSON)
            }
            if (request.headers[APPLICATION_JSON] == null) {
                addHeader(KEY_CONTENT_TYPE, APPLICATION_JSON)
            }
            if (request.headers[KEY_CONTENT_LANGUAGE] == null) {
                addHeader(
                    KEY_CONTENT_LANGUAGE,
                    LMLocaleUtil.getLanguage(LMApplication.getInstance())
                )
            }
            if (request.headers[KEY_REQUESTED_WITH] == null) {
                addHeader(KEY_REQUESTED_WITH, KEY_XML_HTTP_REQUEST)
            }
            val preference = LMApplication.getInstance().getPreference()
            addHeader(
                AUTHORISATION_KEY, BEARER + when {
                    isAuthorizedClient -> preference.authToken
                    addRefresh -> preference.refreshToken
                    else -> null
                }
            )
        }.build()

        /**
         * To get unauthorised client
         *
         * @return reference of unauthorised client
         */
        fun getUnAuthorisedClient(): LMApiInterface {
            if (!::unAuthorisedClient.isInitialized) {
                unAuthorisedClient = buildRetrofit(unAuthorisedOkHttpClient)
            }
            return unAuthorisedClient.create(LMApiInterface::class.java)
        }


        fun getRefreshToken(): LMApiInterface {
            if (!::refreshTokenClient.isInitialized) {
                refreshTokenClient = buildRetrofit(refreshOkHttpClient)
            }
            return refreshTokenClient.create(LMApiInterface::class.java)
        }

        /**
         * To get authorised client
         *
         * @return reference of authorised client
         */
        fun getAuthorisedClient(): LMApiInterface {
            if (!::authorisedClient.isInitialized) {
                authorisedClient = buildRetrofit(authorizedOkHttpClient)
            }
            return authorisedClient.create(LMApiInterface::class.java)
        }

        /**
         * Method which builds the retrofit with baseUrl and Client sent
         *
         * @param okHttpClient Client with request and header details
         * @return Retrofit
         */
        private fun buildRetrofit(okHttpClient: OkHttpClient): Retrofit {
            return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(okHttpClient)
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().setLenient().create()
                    )
                )
                .build()
        }

        fun getUnAuthorisedRetrofitClient(): Retrofit {
            return buildRetrofit(unAuthorisedOkHttpClient)
        }

        fun getAuthorisedRetrofitClient(): Retrofit {
            return buildRetrofit(authorizedOkHttpClient)
        }
    }

    private fun <T> request(call: Call<T>, callback: LMRetrofitCallback<T>) {
        if (LMApplication.isNetworkConnected()) {
            call.enqueue(callback)
        } else {
            callback.onNoNetwork()
        }
    }

    fun login(loginData: LMLoginRequest, callback: LMRetrofitCallback<LMUserResponse>) {
        request(getUnAuthorisedClient().loginUser(loginData), callback)
    }

    fun logoutUser(callback: LMRetrofitCallback<LMCommonResponse>) {
        request(getAuthorisedClient().logoutUser(), callback)
    }

    fun createTable(
        tableDetail: LMCreateTableRequest,
        callback: LMRetrofitCallback<MutableList<LMTableListData>>
    ) {
        request(getAuthorisedClient().createTable(tableDetail), callback)
    }

    fun createMenu(
        menuDetail: LMMenuResponse,
        callback: LMRetrofitCallback<MutableList<LMMenuResponse>>
    ) {
        request(getAuthorisedClient().createMenu(menuDetail), callback)
    }

    fun changeMenuStatus(
        menuId: Int,
        status: Boolean,
        callback: LMRetrofitCallback<LMCommonResponse>
    ) {
        request(
            getAuthorisedClient().changeMenuStatus(
                menuId,
                LMMenuVisibilityChangeRequest(status)
            ), callback
        )
    }

    fun createNewPendingOrder(
        tableName: String?,
        orderName: String?,
        order: String?,
        callback: LMRetrofitCallback<LMCommonResponse>
    ) {
        request(getAuthorisedClient().createNewPendingOrder(tableName, orderName, order), callback)
    }

    fun createOrderDetail(
        orderDetail: LMCreateOrderRequest,
        callback: LMRetrofitCallback<LMCompleteOrderResponse>
    ) {
        orderDetail.apply {
            request(
                getAuthorisedClient().createOrderDetail(
                    order,
                    subTotal,
                    total,
                    discount
                ), callback
            )
        }
    }


    fun createFeedBack(
        feedbackData: LMFeedbackRequest,
        callback: LMRetrofitCallback<LMCommonResponse>
    ) {
        request(getAuthorisedClient().createFeedBack(feedbackData), callback)
    }

    fun createSettingTime(
        settingTimeDetail: LMSettingDetail?,
        callback: LMRetrofitCallback<LMCommonResponse>
    ) {
        request(getAuthorisedClient().createSettingTime(settingTimeDetail), callback)
    }

    fun getAllTableList(callback: LMRetrofitCallback<MutableList<LMTableListData>>) {
        request(getAuthorisedClient().getTableList(), callback)
    }

    fun getIndividualTable(tableId: Int, callback: LMRetrofitCallback<LMTableListData>) {
        request(getAuthorisedClient().getIndividualTable(tableId), callback)
    }

    fun getMenuFilter(callback: LMRetrofitCallback<LMMenuFilterResponse>) {
        request(getAuthorisedClient().getMenuFilter(), callback)
    }

    fun getMenuList(
        filterMenu: LMFilterMenuRequest,
        callback: LMRetrofitCallback<MutableList<LMMenuResponse>>
    ) {
        with(filterMenu) {
            request(getAuthorisedClient().getMenuList(withMenuTimings, type, category), callback)
        }
    }

    fun getIndividualMenu(menuId: Int, callback: LMRetrofitCallback<LMMenuResponse>) {
        request(getAuthorisedClient().getIndividualMenu(menuId), callback)
    }

    fun getPendingOrderOnDate(
        pendingOrderData: String,
        callback: LMRetrofitCallback<MutableList<LMOrderResponse>>
    ) {
        request(getAuthorisedClient().getPendingOrders(pendingOrderData), callback)
    }

    fun getPendingForOrder(
        orderName: String,
        callback: LMRetrofitCallback<MutableList<LMOrderResponse>>
    ) {
        request(getAuthorisedClient().getPendingOrder(orderName), callback)
    }

    fun getAllPendingOrderName(callback: LMRetrofitCallback<MutableList<String>>) {
        request(getAuthorisedClient().getAllPendingOrderName(), callback)
    }

    fun getAllPendingOrderName(
        tableName: String,
        callback: LMRetrofitCallback<MutableList<String>>
    ) {
        request(getAuthorisedClient().getAllPendingOrderName(tableName), callback)
    }

    fun getOrderAvailable(
        orderDate: String,
        callback: LMRetrofitCallback<MutableList<LMOrderResponse>>
    ) {
        request(getAuthorisedClient().getOrderAvailable(orderDate), callback)
    }

    fun getSalesReport(
        orderDate: String,
        callback: LMRetrofitCallback<LMSalesReportResponse>
    ) {
        request(getAuthorisedClient().getSalesReport(orderDate), callback)
    }

    fun getSettingTime(
        callback: LMRetrofitCallback<LMSettingDetail>
    ) {
        request(getAuthorisedClient().getSettingTime(), callback)
    }

    fun updateMenuDetail(
        menuId: Int,
        menuDetail: LMMenuResponse,
        callback: LMRetrofitCallback<MutableList<LMMenuResponse>>
    ) {
        request(getAuthorisedClient().updateMenuDetail(menuId, menuDetail), callback)
    }

    fun deleteTable(
        tableId: Int,
        callback: LMRetrofitCallback<MutableList<LMTableListData>>
    ) {
        request(getAuthorisedClient().deleteTable(tableId), callback)
    }

    fun deleteMenu(
        menuId: Int,
        callback: LMRetrofitCallback<MutableList<LMMenuResponse>>
    ) {
        request(getAuthorisedClient().deleteMenu(menuId), callback)
    }

    fun deletePendingOrder(
        tableName: String?, orderName: String?,
        callback: LMRetrofitCallback<LMCommonResponse>
    ) {
        request(getAuthorisedClient().deletePendingOrder(tableName, orderName), callback)
    }

    fun getAppVersion(callback: LMRetrofitCallback<LMAppVersionResponse>) {
        request(getUnAuthorisedClient().getAppVersion(BuildConfig.VERSION_NAME), callback)
    }

    fun registerUser(
        registrationUserDetail: LMRegisterUserRequest,
        callback: LMRetrofitCallback<LMUserResponse>
    ) {
        request(getUnAuthorisedClient().registerUser(registrationUserDetail), callback)
    }
    fun getMenuTypes(callback:LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>){
        request(getAuthorisedClient().getMenuTypes(),callback)
    }
    fun editType(id:Int,name: LMMenuTypesRequest,callback:LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>){
        request(getAuthorisedClient().editType(id, name),callback)
    }
    fun deleteType(id:Int,callback: LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>){
        request(getAuthorisedClient().deleteType(id),callback)
    }
    fun createType(name:LMMenuTypesRequest,callback:LMRetrofitCallback<ArrayList<LMMenuTypesResponse>>){
        request(getAuthorisedClient().createType(name),callback)
    }
    fun getMenuCategories(callback:LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>){
        request(getAuthorisedClient().getMenuCategories(),callback)
    }
    fun editCategory(id:Int,name: LMMenuTypesRequest,callback:LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>){
        request(getAuthorisedClient().editCategory(id, name),callback)
    }
    fun deleteCategory(id:Int,callback: LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>){
        request(getAuthorisedClient().deleteCategory(id),callback)
    }
    fun createCategory(name:LMMenuTypesRequest,callback:LMRetrofitCallback<ArrayList<LMMenuCategoriesResponse>>){
        request(getAuthorisedClient().createCategory(name),callback)
    }

    fun getCountryList(callback: LMRetrofitCallback<MutableList<LMPlaceList>>) {
        request(getUnAuthorisedClient().getCountryList(), callback)
    }

    fun getStateList(countryId: Int?, callback: LMRetrofitCallback<MutableList<LMPlaceList>>) {
        request(getUnAuthorisedClient().getStateList(countryId), callback)
    }

    fun getCitiesList(
        countryId: Int?,
        stateId: Int?,
        callback: LMRetrofitCallback<MutableList<LMPlaceList>>
    ) {
        request(getUnAuthorisedClient().getCitiesList(countryId, stateId), callback)
    }

    fun currentUser(callback:LMRetrofitCallback<LMUserResponse>){
        request(getAuthorisedClient().checkSubscription(),callback)
    }

    fun getSubCitiesList(
        countryId: Int?,
        stateId: Int?,
        cityId: Int?,
        callback: LMRetrofitCallback<MutableList<LMPlaceList>>
    ) {
        request(getUnAuthorisedClient().getSubCitiesList(countryId, stateId, cityId), callback)
    }

    /**
     * To cancel all the existing requests at once
     */
    fun cancelAllRequests() {
        authorizedOkHttpClient.dispatcher.cancelAll()
    }

    interface LMApiInterface {
        @POST("login")
        fun loginUser(@Body loginUserRequest: LMLoginRequest): Call<LMUserResponse>

        @POST("logout")
        fun logoutUser(): Call<LMCommonResponse>

        @POST("refresh")
        fun refreshToken(): Call<ResponseBody>

        @POST("tables")
        fun createTable(@Body tableDetail: LMCreateTableRequest): Call<MutableList<LMTableListData>>

        @POST("menus")
        fun createMenu(@Body menuDetail: LMMenuResponse): Call<MutableList<LMMenuResponse>>

        @POST("menus/{id}/change-status")
        fun changeMenuStatus(
            @Path("id") menuId: Int,
            @Body statusChange: LMMenuVisibilityChangeRequest
        ): Call<LMCommonResponse>

        @POST("pending-orders")
        fun createNewPendingOrder(
            @Query("table_name") tableName: String?,
            @Query("order_name") orderName: String?,
            @Query("order") order: String?
        ): Call<LMCommonResponse>

        @POST("orders")
        fun createOrderDetail(
            @Query("order") order: String?,
            @Query("sub_total") subTotal: String?,
            @Query("total") total: String?,
            @Query("discount") discount: String?
        ): Call<LMCompleteOrderResponse>

        @POST("admin-feedback")
        fun createFeedBack(@Body feedbackRequest: LMFeedbackRequest): Call<LMCommonResponse>

        @POST("settings/menu")
        fun createSettingTime(@Body settingTimeDetail: LMSettingDetail?): Call<LMCommonResponse>

        @GET("app-version")
        fun getAppVersion(@Query("app_version") versionNumber: String): Call<LMAppVersionResponse>

        @POST("register")
        fun registerUser(@Body userDetail: LMRegisterUserRequest): Call<LMUserResponse>

        @GET("tables")
        fun getTableList(): Call<MutableList<LMTableListData>>

        @GET("tables/{id}")
        fun getIndividualTable(@Path("id") tableId: Int): Call<LMTableListData>

        @GET("menus/filters")
        fun getMenuFilter(): Call<LMMenuFilterResponse>

        @GET("menus")
        fun getMenuList(
            @Query("with_menu_timings") menuTime: Boolean,
            @Query("type") menuType: Int?,
            @Query("category") menuCategory: Int?
        ): Call<MutableList<LMMenuResponse>>

        @GET("menus/{id}")
        fun getIndividualMenu(@Path("id") menuId: Int): Call<LMMenuResponse>

        @GET("pending-orders")
        fun getPendingOrders(@Query("date") date: String): Call<MutableList<LMOrderResponse>>

        @GET("pending-orders")
        fun getPendingOrder(@Query("order_name") orderName: String): Call<MutableList<LMOrderResponse>>

        @GET("pending-orders/get-all-names")
        fun getAllPendingOrderName(): Call<MutableList<String>>

        @GET("pending-orders/get-all-names")
        fun getAllPendingOrderName(@Query("table_name") table: String): Call<MutableList<String>>

        @GET("orders")
        fun getOrderAvailable(@Query("date") orderDate: String): Call<MutableList<LMOrderResponse>>

        @GET("reports/daily/sales-report")
        fun getSalesReport(@Query("date") date: String): Call<LMSalesReportResponse>

        @GET("settings/menu")
        fun getSettingTime(): Call<LMSettingDetail>

        @GET("countries")
        fun getCountryList(): Call<MutableList<LMPlaceList>>

        @GET("countries/{countryId}/states")
        fun getStateList(@Query("countryId") countryId: Int?): Call<MutableList<LMPlaceList>>

        @GET("countries/{countryId}/states/{stateId}/cities")
        fun getCitiesList(
            @Query("countryId") countryId: Int?,
            @Query("stateId") stateId: Int?
        ): Call<MutableList<LMPlaceList>>

        @GET("countries/{countryId}/states/{stateId}/cities/{cityId}/sub-cities")
        fun getSubCitiesList(
            @Query("countryId") countryId: Int?,
            @Query("stateId") stateId: Int?,
            @Query("cityId") cityId: Int?
        ): Call<MutableList<LMPlaceList>>

        @PUT("menus/{id}")
        fun updateMenuDetail(
            @Path("id") menuId: Int,
            @Body menuDetail: LMMenuResponse
        ): Call<MutableList<LMMenuResponse>>

        @DELETE("tables/{id}")
        fun deleteTable(@Path("id") tableId: Int): Call<MutableList<LMTableListData>>

        @DELETE("menus/{id}")
        fun deleteMenu(@Path("id") menuId: Int): Call<MutableList<LMMenuResponse>>

        @DELETE("pending-orders")
        fun deletePendingOrder(
            @Query("table_name") tableName: String?,
            @Query("order_name") orderName: String?
        ): Call<LMCommonResponse>

        @GET("menus/filters")
        fun getMenuTypesAndCategories():Call<MenuTypesAndCategoriesResponse>

        @GET("me")
        fun checkSubscription():Call<LMUserResponse>

        @GET("types")
        fun getMenuTypes():Call<ArrayList<LMMenuTypesResponse>>

        @PUT("types/{id}")
        fun editType(
            @Path("id")id:Int,
            @Body name:LMMenuTypesRequest
        ):Call<ArrayList<LMMenuTypesResponse>>

        @DELETE("types/{id}")
        fun deleteType(@Path("id")id:Int):Call<ArrayList<LMMenuTypesResponse>>

        @POST("types")
        fun createType(@Body name:LMMenuTypesRequest):Call<ArrayList<LMMenuTypesResponse>>

        @GET("categories")
        fun getMenuCategories():Call<ArrayList<LMMenuCategoriesResponse>>

        @PUT("categories/{id}")
        fun editCategory(
            @Path("id")id:Int,
            @Body name:LMMenuTypesRequest
        ):Call<ArrayList<LMMenuCategoriesResponse>>

        @DELETE("categories/{id}")
        fun deleteCategory(@Path("id")id:Int):Call<ArrayList<LMMenuCategoriesResponse>>

        @POST("categories")
        fun createCategory(@Body name:LMMenuTypesRequest):Call<ArrayList<LMMenuCategoriesResponse>>

    }

    class LMAuthorizationInterceptor : Interceptor {
        private val TAG: String = LMAuthorizationInterceptor::class.java.simpleName

        override fun intercept(chain: Interceptor.Chain): Response {
            LMLog.d(
                TAG,
                "LMAuthorizationInterceptor: called"
            )
            val request = chain.request()
            var response = chain.proceed(request)
            try {
                if (response.code == UNAUTHORISED_CODE) {
                    synchronized(this) {
                        if (isRefreshTokenGoingOn) {
                            pendingRequests.add(request)
                        } else {
                            response = retry(request, chain, response)
                        }
                    }
                }
            } catch (e: IOException) {
                LMLog.e(
                    TAG, "intercept: IOException: ", e
                )
            }
            return response
        }

        private fun retry(
            request: Request,
            chain: Interceptor.Chain,
            response: Response
        ): Response {
            isRefreshTokenGoingOn = true
            LMApplication.getInstance().getPreference().refreshToken?.let {
                val refreshTokenCall: Call<ResponseBody> = getRefreshToken().refreshToken()
                val refreshTokenResponse = refreshTokenCall.execute()
                when {
                    refreshTokenResponse.isSuccessful ->{
                        LMLog.d(TAG, "LMAuthorizationInterceptor: intercept: success")
                        LMUserRepository.setTokens(refreshTokenResponse.headers())
                        var headers=refreshTokenResponse.headers()
                        LMLog.d("ApiManager",headers["subscription_status"].toString())
                        if( headers["subscription_status"]== ACTIVE){
                        if (LMApplication.getInstance().getPreference().refreshToken != null) {
                            if (pendingRequests.size > 0) {
                                for (currentRequest in pendingRequests) {
                                    getRequest(
                                        currentRequest,
                                        isAuthorizedClient = true,
                                        addRefresh = false
                                    )
                                }
                                pendingRequests.clear()
                            }
                            isRefreshTokenGoingOn = false
                            val authorizationRequest: Request = getRequest(
                                request,
                                isAuthorizedClient = true,
                                addRefresh = false
                            )
                            return chain.proceed(authorizationRequest)
                        } else {
                            logoutUser()
                        }
                        }else{
                            isRefreshTokenGoingOn=false
                            LMLog.d("Logout","Subscription Ended")
                            SubscriptionEnded()
                            LMApplication.getInstance().getPreference().subscriptionStatus="inactive"
                        }
                    }
                    refreshTokenResponse.code() == UNAUTHORISED_CODE -> {
                        isRefreshTokenGoingOn = false
                        LMLog.d(
                            TAG,
                            "LMAuthorizationInterceptor: intercept: 401, invalidating session"
                        )
                        logoutUser()
                    }
                    else -> {
                        isRefreshTokenGoingOn = false
                        LMLog.d(
                            TAG,
                            "LMAuthorizationInterceptor: intercept: else called. Login Retry problem"
                        )
                    }
                }
            } ?: logoutUser()
            return response
        }

        private fun logoutUser() {
            EventBus.getDefault().post(LMLogoutUserEvent())
        }
        private fun SubscriptionEnded() {
            EventBus.getDefault().post(LMSubscriptionEndEvent())
        }

    }
}