package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMAppVersionResponse {
    @SerializedName("status")
    @Expose
    var appStatus: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
}