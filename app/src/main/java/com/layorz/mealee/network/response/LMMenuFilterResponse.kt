package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMMenuFilterData

class LMMenuFilterResponse {
    @SerializedName("type")
    @Expose
    var type: MutableList<LMMenuFilterData>? = null

    @SerializedName("category")
    @Expose
    var category: MutableList<LMMenuFilterData>? = null
}