package com.layorz.mealee.network.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.layorz.mealee.common.model.LMBranchData
import com.layorz.mealee.common.model.LMCompanyData
import com.layorz.mealee.common.model.LMS3Credentials

class LMUserResponse {
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null

    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null

    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null

    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null

    @SerializedName("roles")
    @Expose
    var roles: List<String>? = null

    @SerializedName("permissions")
    @Expose
    var permissions: List<String>? = null

    @SerializedName("subscription_status")
    @Expose
    var subscriptionStatus:String?=null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("bill_slug")
    @Expose
    var billSlug: String? = null

    @SerializedName("company")
    @Expose
    var company: LMCompanyData? = null

    @SerializedName("branch")
    @Expose
    var branch: LMBranchData? = null

    @SerializedName("s3_credentials")
    @Expose
    var s3Credentials: LMS3Credentials? = null
}