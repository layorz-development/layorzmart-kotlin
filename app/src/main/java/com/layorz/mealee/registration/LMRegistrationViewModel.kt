package com.layorz.mealee.registration

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.layorz.mealee.R
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.common.model.LMPlaceList
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.request.LMRegisterUserRequest
import com.layorz.mealee.network.response.LMUserResponse
import com.layorz.mealee.repository.LMSettingRepository
import com.layorz.mealee.repository.LMUserRepository

class LMRegistrationViewModel(application: Application) : LMViewModel(application) {
    val countryList by lazy { MutableLiveData<MutableList<LMPlaceList>>() }
    val stateList by lazy { MutableLiveData<MutableList<LMPlaceList>>() }
    val cityList by lazy { MutableLiveData<MutableList<LMPlaceList>>() }
    val subCityList by lazy { MutableLiveData<MutableList<LMPlaceList>>() }
    val registerSuccess by lazy { MutableLiveData<String>() }
    val apiUserNameEvent by lazy { MutableLiveData<String>() }
    val apiEmailEvent by lazy { MutableLiveData<String>() }
    val apiNewPasswordEvent by lazy { MutableLiveData<String>() }
    val apiConfirmPasswordEvent by lazy { MutableLiveData<String>() }
    val apiCompanyNameEvent by lazy { MutableLiveData<String>() }
    val apiCompanyPhoneEvent by lazy { MutableLiveData<String>() }
    val apiCompanyAddressOneEvent by lazy { MutableLiveData<String>() }
    val apiCompanyAddressTwoEvent by lazy { MutableLiveData<String>() }
    val apiCompanyPinCodeEvent by lazy { MutableLiveData<String>() }
    val apiCompanyBillSlug by lazy { MutableLiveData<String>() }
    val userNameEvent by lazy { MutableLiveData<Int>() }
    val emailEvent by lazy { MutableLiveData<Int>() }
    val newPasswordEvent by lazy { MutableLiveData<Int>() }
    val confirmPasswordEvent by lazy { MutableLiveData<Int>() }
    val companyNameEvent by lazy { MutableLiveData<Int>() }
    val companyPhoneEvent by lazy { MutableLiveData<Int>() }
    val companyAddressOneEvent by lazy { MutableLiveData<Int>() }
    val companyAddressTwoEvent by lazy { MutableLiveData<Int>() }
    val companyBillSlug by lazy { MutableLiveData<Int>() }
    val companyPinCodeEvent by lazy { MutableLiveData<Int>() }
    var userName: String? = null
    var emailId: String? = null
    var newPassword: String? = null
    var confirmPassword: String? = null
    var companyName: String? = null
    var companyPhoneNumber: String? = null
    var companyAddressOne: String? = null
    var companyAddressTwo: String? = null
    var companyPinCode: String? = null
    var companySlug: String? = null
    var companyCity: Int? = null
    var companyState: Int? = null
    var companyCountry: Int? = null
    var companySubCity: Int? = null
    var companyCityPosition = 0
    var companyStatePosition = 0
    var companyCountyPosition = 0
    var companySubCityPosition = 0

    init {
        getCountryList()
    }

    private fun getCountryList() {
        isProgressShowing.value = true
        LMSettingRepository.getCountryList(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                (data as? MutableList<LMPlaceList>)?.let {
                    countryList.value = it
                }
                isProgressShowing.value = false
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }

    fun getStatusList(countryName: String) {
        isProgressShowing.value = true
        LMSettingRepository.getStateList(getPlaceId(countryName, countryList.value),
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    (data as? MutableList<LMPlaceList>)?.let {
                        stateList.value = it
                    }
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as? String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun getCityList(stateName: String) {
        isProgressShowing.value = true
        LMSettingRepository.getCitiesList(
            companyCountry,
            getPlaceId(stateName, stateList.value),
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    (data as? MutableList<LMPlaceList>)?.let {
                        cityList.value = it
                    }
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as? String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun getSubCityList(cityName: String) {
        isProgressShowing.value = true
        LMSettingRepository.getSubCitiesList(
            companyCountry,
            companyState,
            getPlaceId(cityName, cityList.value),
            object : LMApiResponseCallback {
                override fun <T> onSuccess(data: T) {
                    (data as? MutableList<LMPlaceList>)?.let {
                        subCityList.value = it
                    }
                    isProgressShowing.value = false
                }

                override fun <T> onError(errorMessage: T) {
                    handleApiResponse(errorMessage as? String)
                }

                override fun onFailure(failureMessage: String?) {
                    handleApiResponse(failureMessage)
                }

                override fun onNoNetwork() {
                    showNoNetworkToast()
                }
            })
    }

    fun getPlaceList(placeList: MutableList<LMPlaceList>) = mutableListOf<String>().apply {
        for (item in placeList) {
            item.name?.let {
                add(it)
            }
        }
    }

    fun getPlaceId(placeName: String?, placeList: MutableList<LMPlaceList>?): Int? {
        placeList?.let {
            for (item in it) {
                if (item.name == placeName) {
                    return item.id
                }
            }
        }
        return null
    }

    fun checkFieldRegistration() {
        isProgressShowing.value = true
        var isValidUserRegistration = true
        userNameEvent.value = if (TextUtils.isEmpty(userName?.trim())) {
            isValidUserRegistration = false
            R.string.select_state
        } else {
            null
        }
        emailEvent.value = if (TextUtils.isEmpty(emailId?.trim())) {
            isValidUserRegistration = false
            R.string.email_field_empty
        } else {
            null
        }
        newPasswordEvent.value = if (TextUtils.isEmpty(newPassword?.trim())) {
            isValidUserRegistration = false
            R.string.new_password_error
        } else {
            null
        }
        val confirmPasswordDetail = confirmPassword
        confirmPasswordEvent.value = if (confirmPasswordDetail != null) {
            if (TextUtils.isEmpty(confirmPassword?.trim())) {
                isValidUserRegistration = false
                R.string.new_password_error
            } else if (confirmPasswordDetail.length < 8) {
                isValidUserRegistration = false
                R.string.confirm_password_error
            } else if (newPassword != confirmPassword) {
                isValidUserRegistration = false
                R.string.password_not_match
            } else {
                null
            }
        } else {
            isValidUserRegistration = false
            null
        }
        companyNameEvent.value = if (TextUtils.isEmpty(companyName?.trim())) {
            isValidUserRegistration = false
            R.string.company_name_empty
        } else {
            null
        }
        companyPhoneEvent.value = if (TextUtils.isEmpty(companyPhoneNumber?.toString()?.trim())) {
            isValidUserRegistration = false
            R.string.company_phone_empty
        } else {
            null
        }
        companyAddressOneEvent.value = if (TextUtils.isEmpty(companyAddressOne?.trim())) {
            isValidUserRegistration = false
            R.string.company_address_1_empty
        } else {
            null
        }
        companyAddressTwoEvent.value = if (TextUtils.isEmpty(companyAddressTwo?.trim())) {
            isValidUserRegistration = false
            R.string.company_address_2_empty
        } else {
            null
        }
        companyPinCodeEvent.value = if (TextUtils.isEmpty(companyPinCode?.trim())) {
            isValidUserRegistration = false
            R.string.company_pincode_empty
        } else {
            null
        }
        companyBillSlug.value = if (TextUtils.isEmpty(companySlug?.trim())) {
            isValidUserRegistration = false
            R.string.company_address_2_empty
        } else {
            null
        }
        apiResponseInId.value = if (companyCountry != null) {
            isValidUserRegistration = false
            R.string.country_name_error
        } else if (companyState != null) {
            isValidUserRegistration = false
            R.string.state_name_error
        } else if (companyCity != null) {
            isValidUserRegistration = false
            R.string.city_name_error
        } else if (companySubCity != null) {
            isValidUserRegistration = false
            R.string.city_name_error
        } else {
            null
        }
        if (isValidUserRegistration) {
            registerUser(LMRegisterUserRequest().apply {
                name = userName
                email = emailId
                password = newPassword
                confirmPassword = this@LMRegistrationViewModel.confirmPassword
                companyName = this@LMRegistrationViewModel.companyName
                addressLine1 = companyAddressOne
                addressLine2 = companyAddressTwo
                subCity = companySubCity
                city = companyCity
                state = companyState
                country = companyCountry
                phoneNo = companyPhoneNumber?.toInt()
                billSlug = companySlug
                pinCode = companyPinCode
            })
        } else {
            isProgressShowing.value = false
        }
    }

    private fun registerUser(registerUser: LMRegisterUserRequest) {
        LMUserRepository.registerUser(registerUser, object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                (data as? LMUserResponse)?.let {
                    LMApplication.getInstance().getPreference().apply {
                        this@LMRegistrationViewModel.emailId?.let {
                            emailId = it
                        }
                    }
                    registerSuccess.value = ""
                }
            }

            override fun <T> onError(errorMessage: T) {
                val errors = errorMessage as? JsonObject
                errors?.let {
                    if (errors.has("name")) {
                        apiUserNameEvent.value = errors.getAsJsonArray("name").get(0).toString()
                    }
                    if (errors.has("email")) {
                        apiEmailEvent.value = errors.getAsJsonArray("email").get(0).toString()
                    }
                    if (errors.has("password")) {
                        apiNewPasswordEvent.value =
                            errors.getAsJsonArray("password").get(0).toString()
                    }
                    if (errors.has("confirmPassword")) {
                        apiConfirmPasswordEvent.value =
                            errors.getAsJsonArray("confirmPassword").get(0).toString()
                    }
                    if (errors.has("companyName")) {
                        apiCompanyNameEvent.value =
                            errors.getAsJsonArray("companyName").get(0).toString()
                    }
                    if (errors.has("addressLine1")) {
                        apiCompanyAddressOneEvent.value =
                            errors.getAsJsonArray("addressLine1").get(0).toString()
                    }
                    if (errors.has("addressLine2")) {
                        apiCompanyAddressTwoEvent.value =
                            errors.getAsJsonArray("addressLine2").get(0).toString()
                    }
                    if (errors.has("pincode")) {
                        apiCompanyPinCodeEvent.value =
                            errors.getAsJsonArray("pincode").get(0).toString()
                    }
                    var countryStateCityFieldError: String? = null
                    if (errors.has("country")) {
                        countryStateCityFieldError =
                            errors.getAsJsonArray("country").get(0).toString()
                    }
                    if (errors.has("state")) {
                        val stateError = errors.getAsJsonArray("state").get(0).toString()
                        countryStateCityFieldError =
                            countryStateCityFieldError?.let { "$it, $stateError" } ?: stateError
                    }
                    if (errors.has("city")) {
                        val cityError = errors.getAsJsonArray("city").get(0).toString()
                        countryStateCityFieldError =
                            countryStateCityFieldError?.let { "$it, $cityError" } ?: cityError
                    }
                    if (errors.has("sub_city")) {
                        val cityError = errors.getAsJsonArray("sub_city").get(0).toString()
                        countryStateCityFieldError =
                            countryStateCityFieldError?.let { "$it, $cityError" } ?: cityError
                    }
                    countryStateCityFieldError?.let {
                        apiResponseMessage.value = it
                    }
                    if (errors.has("phoneNo")) {
                        apiCompanyPhoneEvent.value =
                            errors.getAsJsonArray("phoneNo").get(0).toString()
                    }
                    if (errors.has("billSlug")) {
                        apiCompanyBillSlug.value =
                            errors.getAsJsonArray("billSlug").get(0).toString()
                    }
                    isProgressShowing.value = false
                }
                if (errors == null) {
                    handleApiResponse(errorMessage as? String)
                }
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}