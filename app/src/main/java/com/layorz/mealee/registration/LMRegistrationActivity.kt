package com.layorz.mealee.registration

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputLayout
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.common.utils.LMNavigation
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.dashboard.activities.LMDashboardActivity
import kotlinx.android.synthetic.main.lm_activity_registration.*

class LMRegistrationActivity : LMActivity() {
    private val registrationViewModel by lazy {
        ViewModelProvider(this).get(LMRegistrationViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lm_activity_registration)
        initView()
        setListener()
        observerViewModel()
    }

    private fun initView() {
        with(registrationViewModel) {
            mcv_company_state.visibility = companyCountry?.let { View.VISIBLE } ?: View.GONE
            mcv_company_city.visibility = companyState?.let { View.VISIBLE } ?: View.GONE
            mcv_company_sub_city.visibility = companyCity?.let { View.VISIBLE } ?: View.GONE
        }
    }

    private fun setListener() {
        with(registrationViewModel) {
            tie_username_registration.addTextChangedListener {
                userName = it.toString().trim()
            }
            tie_email_registration.addTextChangedListener {
                emailId = it.toString().trim()
            }
            tie_new_password_registration.addTextChangedListener {
                newPassword = it.toString().trim()
            }
            tie_password_registration.addTextChangedListener {
                confirmPassword = it.toString().trim()
            }
            tie_company_name.addTextChangedListener {
                companyName = it.toString().trim()
            }
            tie_company_phone_number.addTextChangedListener {
                companyPhoneNumber = it.toString().trim()
            }
            tie_company_address_1.addTextChangedListener {
                companyAddressOne = it.toString().trim()
            }
            tie_company_address_2.addTextChangedListener {
                companyAddressTwo = it.toString().trim()
            }
            tie_company_pincode.addTextChangedListener {
                companyPinCode = it.toString().trim()
            }
            tie_company_bill_slug.addTextChangedListener {
                companySlug = it.toString().trim()
            }
            btn_register.setOnClickListener {
                registrationViewModel.checkFieldRegistration()
            }
            iv_back_up.setOnClickListener {
                super.onBackPressed()
            }
            acp_company_country.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        (parent?.getItemAtPosition(position) as? String)?.let {
                            if (companyCountyPosition != position) {
                                companyCountyPosition = position
                                companyState = null
                                companyCity = null
                                companySubCity = null
                                companyStatePosition = 0
                                companyCityPosition = 0
                                companySubCityPosition = 0
                                if (companyCountyPosition == 0) {
                                    companyCountry = null
                                    initView()
                                } else {
                                    companyCountry = getPlaceId(it, countryList.value)
                                    getStatusList(it)
                                    mcv_company_state.visibility = View.VISIBLE
                                }
                            }
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            acp_company_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        if (companyStatePosition != position) {
                            companyStatePosition = position
                            companyCity = null
                            companySubCity = null
                            companyCityPosition = 0
                            companySubCityPosition = 0
                            if (companyStatePosition == 0) {
                                companyState = null
                                mcv_company_city.visibility = View.GONE
                            } else {
                                companyState = getPlaceId(it, stateList.value)
                                getCityList(it)
                                mcv_company_city.visibility = View.VISIBLE
                            }
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            acp_company_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    (parent?.getItemAtPosition(position) as? String)?.let {
                        if (companyCityPosition != position) {
                            companyCityPosition = position
                            companySubCity = null
                            companySubCityPosition = 0
                            if (companyCityPosition == 0) {
                                companyState = null
                                mcv_company_city.visibility = View.GONE
                            } else {
                                companyCity = getPlaceId(it, cityList.value)
                                getSubCityList(it)
                                mcv_company_city.visibility = View.VISIBLE
                            }
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            acp_company_sub_city.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        (parent?.getItemAtPosition(position) as? String)?.let {
                            if (companySubCityPosition != position) {
                                companySubCityPosition = position
                                companySubCity =
                                    if (position == 0) null else getPlaceId(it, subCityList.value)
                            }
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }
        }
    }

    private fun observerViewModel() {
        with(registrationViewModel) {
            isProgressShowing.observe(this@LMRegistrationActivity, Observer {
                updateProgressBar(it)
            })
            apiResponseMessage.observe(this@LMRegistrationActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMRegistrationActivity, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(this@LMRegistrationActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMRegistrationActivity, getString(it), Toast.LENGTH_SHORT)
                        .show()
                }
            })
            countryList.observe(this@LMRegistrationActivity, Observer {
                ArrayAdapter(
                    this@LMRegistrationActivity,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(0, getString(R.string.select_country))
                        addAll(getPlaceList(it))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_company_country.adapter = this
                    acp_company_country.setSelection(companyCountyPosition)
                }
            })
            stateList.observe(this@LMRegistrationActivity, Observer {
                ArrayAdapter(
                    this@LMRegistrationActivity,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(0, getString(R.string.select_state))
                        addAll(getPlaceList(it))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_company_state.adapter = this
                    acp_company_state.setSelection(companyStatePosition)
                }
            })
            cityList.observe(this@LMRegistrationActivity, Observer {
                ArrayAdapter(
                    this@LMRegistrationActivity,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(0, getString(R.string.select_city))
                        addAll(getPlaceList(it))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_company_city.adapter = this
                    acp_company_city.setSelection(companyCityPosition)
                }
            })
            subCityList.observe(this@LMRegistrationActivity, Observer {
                ArrayAdapter(
                    this@LMRegistrationActivity,
                    R.layout.lm_item_spinner_dropdown,
                    mutableListOf<String>().apply {
                        add(0, getString(R.string.select_sub_city))
                        addAll(getPlaceList(it))
                    }
                ).apply {
                    setDropDownViewResource(R.layout.lm_item_spinner_dropdown)
                    acp_company_sub_city.adapter = this
                    acp_company_sub_city.setSelection(companyCityPosition)
                }
            })
            userNameEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_username_registration, it?.let { getString(it) })
            })
            emailEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_email_registration, it?.let { getString(it) })
            })
            newPasswordEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_new_password_registration, it?.let { getString(it) })
            })
            confirmPasswordEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_password_registration, it?.let { getString(it) })
            })
            companyNameEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_name, it?.let { getString(it) })
            })
            companyPhoneEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_phone_number, it?.let { getString(it) })
            })
            companyAddressOneEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_address_1, it?.let { getString(it) })
            })
            companyAddressTwoEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_address_2, it?.let { getString(it) })
            })
            companyPinCodeEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_pincode, it?.let { getString(it) })
            })
            companyBillSlug.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_bill_slug, it?.let { getString(it) })
            })
            apiUserNameEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_username_registration, it)
            })
            apiEmailEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_email_registration, it)
            })
            apiNewPasswordEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_new_password_registration, it)
            })
            apiConfirmPasswordEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_password_registration, it)
            })
            apiCompanyNameEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_name, it)
            })
            apiCompanyPhoneEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_phone_number, it)
            })
            apiCompanyAddressOneEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_address_1, it)
            })
            apiCompanyAddressTwoEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_address_2, it)
            })
            apiCompanyPinCodeEvent.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_pincode, it)
            })
            apiCompanyBillSlug.observe(this@LMRegistrationActivity, Observer {
                setMessage(til_company_bill_slug, it)
            })
            registerSuccess.observe(this@LMRegistrationActivity, Observer {
                it?.let {
                    LMUtilise.hideKeyboard(this@LMRegistrationActivity)
                    LMNavigation.switchActivity(
                        this@LMRegistrationActivity,
                        LMDashboardActivity::class.java,
                        flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    )
                }
            })
        }
    }

    private fun setMessage(field: TextInputLayout, message: String?) {
        field.apply {
            this.isErrorEnabled = message?.let {
                error = it
                true
            } ?: false
        }
    }
}