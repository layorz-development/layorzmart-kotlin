package com.layorz.mealee.widget

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Build
import android.os.Message
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.fragment.app.Fragment
import java.lang.ref.WeakReference


class LMWebView : WebView {

    interface LMWebViewListener {
        fun onPageStarted(url: String?, favicon: Bitmap?)
        fun onPageFinished(url: String?)
        fun onPageError(
            errorCode: Int,
            description: String?,
            failingUrl: String?
        )
    }

    private var mActivity: WeakReference<Activity>? = null
    private var mFragment: WeakReference<Fragment>? = null
    private var mListener: LMWebViewListener? = null
    private var mCustomWebViewClient: WebViewClient? = null
    private var mCustomWebChromeClient: WebChromeClient? = null
    private var mGeolocationEnabled = false
    private val mHttpHeaders: MutableMap<String, String> = HashMap()
    private var mLastError: Long = 0

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    override fun onResume() {
        resumeTimers()
        super.onResume()
    }

    override fun onPause() {
        pauseTimers()
        super.onPause()
    }

    fun onDestroy() {
        try {
            (parent as ViewGroup).removeView(this) // try to remove this view from its parent first
        } catch (ignored: Exception) {
        }
        try {
            removeAllViews() // then try to remove all child views from this view
        } catch (ignored: Exception) {
        }
        destroy() // and finally destroy this view
    }

    override fun setWebChromeClient(client: WebChromeClient?) {
        mCustomWebChromeClient = client
    }

    override fun setWebViewClient(client: WebViewClient) {
        mCustomWebViewClient = client
    }

    override fun loadUrl(
        url: String,
        additionalHttpHeaders: MutableMap<String, String>
    ) {
        var httpHeaders = additionalHttpHeaders
        if (httpHeaders == null) {
            httpHeaders = mHttpHeaders
        } else if (mHttpHeaders.isNotEmpty()) {
            httpHeaders.putAll(mHttpHeaders)
        }
        super.loadUrl(url, httpHeaders)
    }

    override fun loadUrl(url: String) {
        if (mHttpHeaders.isNotEmpty()) {
            super.loadUrl(url, mHttpHeaders)
        } else {
            super.loadUrl(url)
        }
    }

    fun setListener(activity: Activity?, listener: LMWebViewListener?) {
        mActivity = activity?.let { WeakReference<Activity>(it) }
        mListener = listener
    }

    fun setListener(fragment: Fragment?, listener: LMWebViewListener?) {
        mFragment =fragment?.let { WeakReference<Fragment>(it) }
        mListener = listener
    }

    /**
     * Loads and displays the provided HTML source text
     *
     * @param html the HTML source text to load
     */
    fun loadHtml(html: String) {
        loadHtml(html, null)
    }

    /**
     * Add header to web view
     *
     * @param name  the name of the HTTP header to add
     * @param value the value of the HTTP header to send
     */
    fun addHttpHeader(name: String, value: String) {
        mHttpHeaders[name] = value
    }

    /**
     * Removes one of the HTTP headers that have previously been added via `addHttpHeader()`
     *
     * @param name the name of the HTTP header to remove
     */
    fun removeHttpHeader(name: String) {
        mHttpHeaders.remove(name)
    }


    fun setCookiesEnabled(enabled: Boolean) {
        CookieManager.getInstance().setAcceptCookie(enabled)
    }

    fun setMixedContentAllowed(allowed: Boolean) {
        setMixedContentAllowed(settings, allowed)
    }

    fun setDesktopMode(enabled: Boolean) {
        val webSettings = settings
        val newUserAgent: String
        newUserAgent = if (enabled) {
            webSettings.userAgentString.replace("Mobile", "eliboM")
                .replace("Android", "diordnA")
        } else {
            webSettings.userAgentString.replace("eliboM", "Mobile")
                .replace("diordnA", "Android")
        }
        webSettings.userAgentString = newUserAgent
        webSettings.useWideViewPort = enabled
        webSettings.loadWithOverviewMode = enabled
        webSettings.setSupportZoom(enabled)
        webSettings.builtInZoomControls = enabled
    }

    fun onBackPressed(): Boolean {
        return if (canGoBack()) {
            goBack()
            false
        } else {
            true
        }
    }

    fun setThirdPartyCookiesEnabled(enabled: Boolean) {
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, enabled)
        }
    }

    /**
     * Loads and displays the provided HTML source text
     *
     * @param html    the HTML source text to load
     * @param baseUrl the URL to use as the page's base URL
     */
    fun loadHtml(html: String, baseUrl: String?) {
        loadHtml(html, baseUrl, null)
    }

    /**
     * Loads and displays the provided HTML source text
     *
     * @param html       the HTML source text to load
     * @param baseUrl    the URL to use as the page's base URL
     * @param historyUrl the URL to use for the page's history entry
     */
    fun loadHtml(
        html: String,
        baseUrl: String?,
        historyUrl: String?
    ) {
        loadHtml(html, baseUrl, historyUrl, "utf-8")
    }

    /**
     * Loads and displays the provided HTML source text
     *
     * @param html       the HTML source text to load
     * @param baseUrl    the URL to use as the page's base URL
     * @param historyUrl the URL to use for the page's history entry
     * @param encoding   the encoding or charset of the HTML source text
     */
    fun loadHtml(
        html: String,
        baseUrl: String?,
        historyUrl: String?,
        encoding: String?
    ) {
        loadDataWithBaseURL(baseUrl, html, "text/html", encoding, historyUrl)
    }

    protected fun setMixedContentAllowed(
        webSettings: WebSettings,
        allowed: Boolean
    ) {
        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.mixedContentMode =
                if (allowed) WebSettings.MIXED_CONTENT_ALWAYS_ALLOW else WebSettings.MIXED_CONTENT_NEVER_ALLOW
        }
    }

    protected fun setAllowAccessFromFileUrls(
        webSettings: WebSettings,
        allowed: Boolean
    ) {
        webSettings.allowFileAccessFromFileURLs = allowed
        webSettings.allowUniversalAccessFromFileURLs = allowed
    }

    protected fun init(context: Context) {
        if (isInEditMode) {
            return
        }
        if (context is Activity) {
            mActivity = WeakReference(context)
        }
        isFocusable = true
        isFocusableInTouchMode = true
        isSaveEnabled = true
        val webSettings = settings
        webSettings.allowFileAccess = false
        setAllowAccessFromFileUrls(webSettings, false)
        webSettings.builtInZoomControls = false
        webSettings.javaScriptEnabled = true
        setMixedContentAllowed(webSettings, true)
        setThirdPartyCookiesEnabled(true)
        super.setWebViewClient(object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                if (!hasError()) {
                    mListener?.onPageStarted(url, favicon)
                }
                mCustomWebViewClient?.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!hasError()) {
                    mListener?.onPageFinished(url)
                }
                mCustomWebViewClient?.onPageFinished(view, url)
            }

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                setLastError()
                mListener?.onPageError(errorCode, description, failingUrl)
                mCustomWebViewClient?.onReceivedError(view, errorCode, description, failingUrl)
            }

            override fun onLoadResource(view: WebView, url: String) {
                mCustomWebViewClient?.onLoadResource(view, url) ?: super.onLoadResource(view, url)
            }


            override fun shouldInterceptRequest(
                view: WebView?,
                request: WebResourceRequest?
            ) = if (Build.VERSION.SDK_INT >= 21) {
                mCustomWebViewClient?.shouldInterceptRequest(view, request)
                    ?: super.shouldInterceptRequest(view, request)
            } else {
                null
            }

            override fun onFormResubmission(
                view: WebView?,
                dontResend: Message?,
                resend: Message?
            ) {
                mCustomWebViewClient?.onFormResubmission(view, dontResend, resend)
                    ?: super.onFormResubmission(view, dontResend, resend)
            }

            override fun doUpdateVisitedHistory(view: WebView?, url: String?, isReload: Boolean) {
                mCustomWebViewClient?.doUpdateVisitedHistory(view, url, isReload)
                    ?: super.doUpdateVisitedHistory(view, url, isReload)
            }

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                mCustomWebViewClient?.onReceivedSslError(view, handler, error)
                    ?: super.onReceivedSslError(view, handler, error)
            }

            override fun onReceivedClientCertRequest(view: WebView?, request: ClientCertRequest?) {
                if (Build.VERSION.SDK_INT >= 21) {
                    mCustomWebViewClient?.onReceivedClientCertRequest(view, request)
                        ?: super.onReceivedClientCertRequest(view, request)
                }
            }

            override fun onReceivedHttpAuthRequest(
                view: WebView?,
                handler: HttpAuthHandler?,
                host: String?,
                realm: String?
            ) {
                mCustomWebViewClient?.onReceivedHttpAuthRequest(view, handler, host, realm)
                    ?: super.onReceivedHttpAuthRequest(view, handler, host, realm)
            }

            override fun shouldOverrideKeyEvent(view: WebView?, event: KeyEvent?) =
                mCustomWebViewClient?.shouldOverrideKeyEvent(view, event)
                    ?: super.shouldOverrideKeyEvent(view, event)


            override fun onUnhandledKeyEvent(view: WebView?, event: KeyEvent?) {
                mCustomWebViewClient?.onUnhandledKeyEvent(view, event) ?: super.onUnhandledKeyEvent(
                    view,
                    event
                )
            }

            override fun onScaleChanged(view: WebView?, oldScale: Float, newScale: Float) {
                mCustomWebViewClient?.onScaleChanged(view, oldScale, newScale)
                    ?: super.onScaleChanged(view, oldScale, newScale)
            }

            override fun onReceivedLoginRequest(
                view: WebView?,
                realm: String?,
                account: String?,
                args: String?
            ) {
                mCustomWebViewClient?.onReceivedLoginRequest(view, realm, account, args)
                    ?: super.onReceivedLoginRequest(view, realm, account, args)
            }
        })
        super.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                mCustomWebChromeClient?.onProgressChanged(view, newProgress)
                    ?: super.onProgressChanged(view, newProgress)
            }

            override fun onReceivedTitle(view: WebView?, title: String?) {
                mCustomWebChromeClient?.onReceivedTitle(view, title) ?: super.onReceivedTitle(
                    view,
                    title
                )
            }

            override fun onReceivedIcon(view: WebView?, icon: Bitmap?) {
                mCustomWebChromeClient?.onReceivedIcon(view, icon) ?: super.onReceivedIcon(
                    view,
                    icon
                )
            }

            override fun onReceivedTouchIconUrl(
                view: WebView?,
                url: String?,
                precomposed: Boolean
            ) {
                mCustomWebChromeClient?.onReceivedTouchIconUrl(view, url, precomposed)
                    ?: super.onReceivedTouchIconUrl(view, url, precomposed)
            }

            override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                mCustomWebChromeClient?.onShowCustomView(view, callback) ?: super.onShowCustomView(
                    view,
                    callback
                )
            }

            override fun onHideCustomView() {
                mCustomWebChromeClient?.onHideCustomView() ?: super.onHideCustomView()
            }

            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ) = mCustomWebChromeClient?.onCreateWindow(
                view,
                isDialog,
                isUserGesture,
                resultMsg
            ) ?: super.onCreateWindow(view, isDialog, isUserGesture, resultMsg)

            override fun onRequestFocus(view: WebView?) {
                mCustomWebChromeClient?.onRequestFocus(view) ?: super.onRequestFocus(view)
            }

            override fun onCloseWindow(window: WebView?) {
                mCustomWebChromeClient?.onCloseWindow(window) ?: super.onCloseWindow(window)
            }

            override fun onJsAlert(
                view: WebView?,
                url: String?,
                message: String?,
                result: JsResult?
            ) = mCustomWebChromeClient?.onJsAlert(view, url, message, result) ?: super.onJsAlert(
                view,
                url,
                message,
                result
            )

            override fun onJsConfirm(
                view: WebView?,
                url: String?,
                message: String?,
                result: JsResult?
            ) = mCustomWebChromeClient?.onJsConfirm(view, url, message, result)
                ?: super.onJsConfirm(view, url, message, result)

            override fun onJsPrompt(
                view: WebView?,
                url: String?,
                message: String?,
                defaultValue: String?,
                result: JsPromptResult?
            ) = mCustomWebChromeClient?.onJsPrompt(
                view,
                url,
                message,
                defaultValue,
                result
            ) ?: super.onJsPrompt(view, url, message, defaultValue, result)

            override fun onJsBeforeUnload(
                view: WebView?,
                url: String?,
                message: String?,
                result: JsResult?
            ) = mCustomWebChromeClient?.onJsBeforeUnload(view, url, message, result)
                ?: super.onJsBeforeUnload(view, url, message, result)

            override fun onGeolocationPermissionsShowPrompt(
                origin: String?,
                callback: GeolocationPermissions.Callback?
            ) {
                if (mGeolocationEnabled) {
                    callback?.invoke(origin, true, false)
                } else {
                    mCustomWebChromeClient?.onGeolocationPermissionsShowPrompt(
                        origin,
                        callback
                    ) ?: super.onGeolocationPermissionsShowPrompt(origin, callback)
                }
            }

            override fun onGeolocationPermissionsHidePrompt() {
                mCustomWebChromeClient?.onGeolocationPermissionsHidePrompt()
                    ?: super.onGeolocationPermissionsHidePrompt()
            }

            override fun onConsoleMessage(consoleMessage: ConsoleMessage?) =
                mCustomWebChromeClient?.onConsoleMessage(consoleMessage) ?: super.onConsoleMessage(
                    consoleMessage
                )

            override fun getDefaultVideoPoster() =
                mCustomWebChromeClient?.defaultVideoPoster ?: super.getDefaultVideoPoster()

            override fun getVideoLoadingProgressView() =
                mCustomWebChromeClient?.videoLoadingProgressView
                    ?: super.getVideoLoadingProgressView()

            override fun getVisitedHistory(callback: ValueCallback<Array<String>>?) {
                mCustomWebChromeClient?.getVisitedHistory(callback) ?: super.getVisitedHistory(
                    callback
                )
            }
        })
    }

    protected fun setLastError() {
        mLastError = System.currentTimeMillis()
    }

    protected fun hasError(): Boolean {
        return mLastError + 500 >= System.currentTimeMillis()
    }
}