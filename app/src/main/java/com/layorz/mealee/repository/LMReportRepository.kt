package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMOrderDateRequest
import com.layorz.mealee.network.response.LMSalesReportResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMReportRepository {
    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun getSalesReport(
        orderDate: String,
        callback: LMApiResponseCallback
    ) {
        apiManager.getSalesReport(orderDate, object : LMRetrofitCallback<LMSalesReportResponse> {
            override fun onResponse(
                call: Call<LMSalesReportResponse>,
                response: Response<LMSalesReportResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMSalesReportResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }
}