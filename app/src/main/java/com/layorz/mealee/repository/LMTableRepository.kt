package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.model.LMTableListData
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMCreateTableRequest
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMTableRepository {
    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createTable(
        tableDetail: LMCreateTableRequest,
        callback: LMApiResponseCallback
    ) {
        apiManager.createTable(tableDetail,
            object : LMRetrofitCallback<MutableList<LMTableListData>> {
                override fun onResponse(
                    call: Call<MutableList<LMTableListData>>,
                    response: Response<MutableList<LMTableListData>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMTableListData>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getAllTableList(callback: LMApiResponseCallback) {
        apiManager.getAllTableList(object : LMRetrofitCallback<MutableList<LMTableListData>> {
            override fun onResponse(
                call: Call<MutableList<LMTableListData>>,
                response: Response<MutableList<LMTableListData>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMTableListData>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getIndividualTable(tableId: Int, callback: LMApiResponseCallback) {
        apiManager.getIndividualTable(tableId, object : LMRetrofitCallback<LMTableListData> {
            override fun onResponse(
                call: Call<LMTableListData>,
                response: Response<LMTableListData>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMTableListData>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun deleteTable(
        tableId: Int,
        callback: LMApiResponseCallback
    ) {
        apiManager.deleteTable(tableId, object : LMRetrofitCallback<MutableList<LMTableListData>> {
            override fun onResponse(
                call: Call<MutableList<LMTableListData>>,
                response: Response<MutableList<LMTableListData>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMTableListData>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }
}