package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMFilterMenuRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMMenuFilterResponse
import com.layorz.mealee.network.response.LMMenuResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMMenuRepository {

    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createMenu(
        menuDetail: LMMenuResponse,
        callback: LMApiResponseCallback
    ) {
        apiManager.createMenu(menuDetail, object : LMRetrofitCallback<MutableList<LMMenuResponse>> {
            override fun onResponse(
                call: Call<MutableList<LMMenuResponse>>,
                response: Response<MutableList<LMMenuResponse>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMMenuResponse>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun changeMenuStatus(menuId: Int, status: Boolean, callback: LMApiResponseCallback) {
        apiManager.changeMenuStatus(menuId, status, object : LMRetrofitCallback<LMCommonResponse> {
            override fun onResponse(
                call: Call<LMCommonResponse>,
                response: Response<LMCommonResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getMenuFilter(callback: LMApiResponseCallback) {
        apiManager.getMenuFilter(object : LMRetrofitCallback<LMMenuFilterResponse> {
            override fun onResponse(
                call: Call<LMMenuFilterResponse>,
                response: Response<LMMenuFilterResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMMenuFilterResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getMenuList(filterMenu: LMFilterMenuRequest, callback: LMApiResponseCallback) {
        apiManager.getMenuList(filterMenu,
            object : LMRetrofitCallback<MutableList<LMMenuResponse>> {
                override fun onResponse(
                    call: Call<MutableList<LMMenuResponse>>,
                    response: Response<MutableList<LMMenuResponse>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMMenuResponse>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getIndividualMenu(menuId: Int, callback: LMApiResponseCallback) {
        apiManager.getIndividualMenu(menuId, object : LMRetrofitCallback<LMMenuResponse> {
            override fun onResponse(
                call: Call<LMMenuResponse>,
                response: Response<LMMenuResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMMenuResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun updateMenuDetail(
        menuId: Int,
        menuDetail: LMMenuResponse,
        callback: LMApiResponseCallback
    ) {
        apiManager.updateMenuDetail(
            menuId,
            menuDetail,
            object : LMRetrofitCallback<MutableList<LMMenuResponse>> {
                override fun onResponse(
                    call: Call<MutableList<LMMenuResponse>>,
                    response: Response<MutableList<LMMenuResponse>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMMenuResponse>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun deleteMenu(
        menuId: Int,
        callback: LMApiResponseCallback
    ) {
        apiManager.deleteMenu(menuId, object : LMRetrofitCallback<MutableList<LMMenuResponse>> {
            override fun onResponse(
                call: Call<MutableList<LMMenuResponse>>,
                response: Response<MutableList<LMMenuResponse>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMMenuResponse>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }
}