package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMOrderResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMPendingOrderRepository {

    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createNewPendingOrder(
        tableName: String?,
        orderName: String?,
        order: String?,
        callback: LMApiResponseCallback
    ) {
        apiManager.createNewPendingOrder(tableName, orderName, order,
            object : LMRetrofitCallback<LMCommonResponse> {
                override fun onResponse(
                    call: Call<LMCommonResponse>,
                    response: Response<LMCommonResponse>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getPendingOrderOnDate(
        pendingOrderData: String,
        callback: LMApiResponseCallback
    ) {
        apiManager.getPendingOrderOnDate(pendingOrderData,
            object : LMRetrofitCallback<MutableList<LMOrderResponse>> {
                override fun onResponse(
                    call: Call<MutableList<LMOrderResponse>>,
                    response: Response<MutableList<LMOrderResponse>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMOrderResponse>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getPendingForOrder(orderName: String, callback: LMApiResponseCallback) {
        apiManager.getPendingForOrder(orderName,
            object : LMRetrofitCallback<MutableList<LMOrderResponse>> {
                override fun onResponse(
                    call: Call<MutableList<LMOrderResponse>>,
                    response: Response<MutableList<LMOrderResponse>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMOrderResponse>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getAllPendingOrderName(callback: LMApiResponseCallback) {
        apiManager.getAllPendingOrderName(object : LMRetrofitCallback<MutableList<String>> {
            override fun onResponse(
                call: Call<MutableList<String>>,
                response: Response<MutableList<String>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<String>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun deletePendingOrder(
        tableName: String?, orderName: String?,
        callback: LMApiResponseCallback
    ) {
        apiManager.deletePendingOrder(tableName, orderName,
            object : LMRetrofitCallback<LMCommonResponse> {
                override fun onResponse(
                    call: Call<LMCommonResponse>,
                    response: Response<LMCommonResponse>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getAllPendingOrderName(tableName: String, callback: LMApiResponseCallback) {
        apiManager.getAllPendingOrderName(tableName,
            object : LMRetrofitCallback<MutableList<String>> {
                override fun onResponse(
                    call: Call<MutableList<String>>,
                    response: Response<MutableList<String>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<MutableList<String>>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }
}