package com.layorz.mealee.repository

import android.text.TextUtils
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMLoginRequest
import com.layorz.mealee.network.request.LMRegisterUserRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMUserResponse
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMUserRepository {
    private const val ACCESS_TOKEN = "access_token"
    private const val REFRESH_TOKEN = "refresh_token"


    fun loginUser(
        userEmailId: String,
        password: String,
        callback: LMApiResponseCallback
    ) {
        LMApplication.getInstance().apply {
            getApiManager().login(LMLoginRequest(userEmailId, password),
                object : LMRetrofitCallback<LMUserResponse> {
                    override fun onResponse(
                        call: Call<LMUserResponse>,
                        response: Response<LMUserResponse>
                    ) {
                        if (response.isSuccessful) {
                            with(getPreference()) {
                                userData = response.body()
                                authToken = response.body()?.accessToken
                                refreshToken = response.body()?.refreshToken
                                subscriptionStatus=response.body()!!.subscriptionStatus
                            }
                            callback.onSuccess(response.body())
                        } else {
                            callback.onError(LMErrorHandle.parseError(response, false))
                        }
                    }

                    override fun onFailure(call: Call<LMUserResponse>, throwable: Throwable) {
                        callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                    }

                    override fun onNoNetwork() {
                        callback.onNoNetwork()
                    }
                })
        }
    }

    fun logout(callback: LMApiResponseCallback) {
        LMApplication.getInstance().apply {
            getApiManager().logoutUser(object : LMRetrofitCallback<LMCommonResponse> {
                override fun onResponse(
                    call: Call<LMCommonResponse>,
                    response: Response<LMCommonResponse>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
        }
    }

    fun setTokens(headers: Headers) {
        val token: String? = headers[ACCESS_TOKEN]
        val refreshToken: String? = headers[REFRESH_TOKEN]
        if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(refreshToken)) {
            LMApplication.getInstance().getPreference().apply {
                authToken = token
                this.refreshToken = refreshToken
            }
        }
    }

    fun registerUser(registerUser: LMRegisterUserRequest, callback: LMApiResponseCallback) {
        LMApplication.getInstance().apply {
            getApiManager().registerUser(registerUser, object : LMRetrofitCallback<LMUserResponse> {
                override fun onResponse(
                    call: Call<LMUserResponse>,
                    response: Response<LMUserResponse>
                ) {
                    if (response.isSuccessful) {
                        with(getPreference()) {
                            userData = response.body()
                            authToken = response.body()?.accessToken
                            refreshToken = response.body()?.refreshToken
                        }
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, false))
                    }
                }

                override fun onFailure(call: Call<LMUserResponse>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
        }
    }
}