package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMCreateOrderRequest
import com.layorz.mealee.network.request.LMOrderDateRequest
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.network.response.LMCompleteOrderResponse
import com.layorz.mealee.network.response.LMOrderResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMOrderRepository {
    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createOrderDetail(
        orderDetail: LMCreateOrderRequest,
        callback: LMApiResponseCallback
    ) {
        apiManager.createOrderDetail(orderDetail, object : LMRetrofitCallback<LMCompleteOrderResponse> {
            override fun onResponse(
                call: Call<LMCompleteOrderResponse>,
                response: Response<LMCompleteOrderResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMCompleteOrderResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getOrderAvailable(
        orderDate: String,
        callback: LMApiResponseCallback
    ) {
        apiManager.getOrderAvailable(orderDate,
            object : LMRetrofitCallback<MutableList<LMOrderResponse>> {
                override fun onResponse(
                    call: Call<MutableList<LMOrderResponse>>,
                    response: Response<MutableList<LMOrderResponse>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(
                    call: Call<MutableList<LMOrderResponse>>,
                    throwable: Throwable
                ) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }
}