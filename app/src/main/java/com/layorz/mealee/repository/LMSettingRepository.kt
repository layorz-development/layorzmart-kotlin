package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.model.LMPlaceList
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMSettingDetail
import com.layorz.mealee.network.response.LMAppVersionResponse
import com.layorz.mealee.network.response.LMCommonResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMSettingRepository {
    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createSettingTime(
        settingTimeDetail: LMSettingDetail?,
        callback: LMApiResponseCallback
    ) {
        apiManager.createSettingTime(settingTimeDetail,
            object : LMRetrofitCallback<LMCommonResponse> {
                override fun onResponse(
                    call: Call<LMCommonResponse>,
                    response: Response<LMCommonResponse>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getSettingTime(
        callback: LMApiResponseCallback
    ) {
        apiManager.getSettingTime(object : LMRetrofitCallback<LMSettingDetail> {
            override fun onResponse(
                call: Call<LMSettingDetail>,
                response: Response<LMSettingDetail>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMSettingDetail>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getApiVersion(callback: LMApiResponseCallback) {
        apiManager.getAppVersion(object : LMRetrofitCallback<LMAppVersionResponse> {
            override fun onResponse(
                call: Call<LMAppVersionResponse>,
                response: Response<LMAppVersionResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMAppVersionResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getCountryList(callback: LMApiResponseCallback) {
        apiManager.getCountryList(object : LMRetrofitCallback<MutableList<LMPlaceList>> {
            override fun onResponse(
                call: Call<MutableList<LMPlaceList>>,
                response: Response<MutableList<LMPlaceList>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMPlaceList>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getStateList(countryId: Int?, callback: LMApiResponseCallback) {
        apiManager.getStateList(countryId, object : LMRetrofitCallback<MutableList<LMPlaceList>> {
            override fun onResponse(
                call: Call<MutableList<LMPlaceList>>,
                response: Response<MutableList<LMPlaceList>>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<MutableList<LMPlaceList>>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }

    fun getCitiesList(countryId: Int?, stateId: Int?, callback: LMApiResponseCallback) {
        apiManager.getCitiesList(
            countryId,
            stateId,
            object : LMRetrofitCallback<MutableList<LMPlaceList>> {
                override fun onResponse(
                    call: Call<MutableList<LMPlaceList>>,
                    response: Response<MutableList<LMPlaceList>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<MutableList<LMPlaceList>>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }

    fun getSubCitiesList(
        countryId: Int?,
        stateId: Int?,
        cityId: Int?,
        callback: LMApiResponseCallback
    ) {
        apiManager.getSubCitiesList(
            countryId,
            stateId,
            cityId,
            object : LMRetrofitCallback<MutableList<LMPlaceList>> {
                override fun onResponse(
                    call: Call<MutableList<LMPlaceList>>,
                    response: Response<MutableList<LMPlaceList>>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(LMErrorHandle.parseError(response, true))
                    }
                }

                override fun onFailure(call: Call<MutableList<LMPlaceList>>, throwable: Throwable) {
                    callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
                }

                override fun onNoNetwork() {
                    callback.onNoNetwork()
                }
            })
    }
}