package com.layorz.mealee.repository

import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.callbacks.LMRetrofitCallback
import com.layorz.mealee.network.errorHandle.LMErrorHandle
import com.layorz.mealee.network.request.LMFeedbackRequest
import com.layorz.mealee.network.response.LMCommonResponse
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

object LMFeedbackRepository {
    private val apiManager by lazy {
        LMApplication.getInstance().getApiManager()
    }

    fun createFeedBack(
        feedbackData: LMFeedbackRequest,
        callback: LMApiResponseCallback
    ) {
        apiManager.createFeedBack(feedbackData, object : LMRetrofitCallback<LMCommonResponse> {
            override fun onResponse(
                call: Call<LMCommonResponse>,
                response: Response<LMCommonResponse>
            ) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body())
                } else {
                    callback.onError(LMErrorHandle.parseError(response, true))
                }
            }

            override fun onFailure(call: Call<LMCommonResponse>, throwable: Throwable) {
                callback.onFailure((if (throwable is UnknownHostException) LMConstant.UNKNOWN_HOST_EXCEPTION else throwable.message.toString()))
            }

            override fun onNoNetwork() {
                callback.onNoNetwork()
            }
        })
    }
}