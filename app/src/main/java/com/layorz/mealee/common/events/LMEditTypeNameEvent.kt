package com.layorz.mealee.common.events

class LMEditTypeNameEvent(
    var id: Int? = null,
    var name: String? = null
)