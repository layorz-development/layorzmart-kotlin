package com.layorz.mealee.common

import com.google.firebase.messaging.FirebaseMessagingService
import com.layorz.mealee.common.utils.LMLog

class LMPushNotification : FirebaseMessagingService() {
    private var TAG = LMPushNotification::class.java.simpleName

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        LMLog.d(TAG, newToken)
    }
}