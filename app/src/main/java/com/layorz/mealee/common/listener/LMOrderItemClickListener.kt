package com.layorz.mealee.common.listener

interface LMOrderItemClickListener {
    fun orderClick(orderName: String)
}