package com.layorz.mealee.common.callbacks

import androidx.fragment.app.Fragment

interface LMReplaceCurrentFragmentCallback {
    fun replaceCurrent(changeFragment: Fragment, containerId: Int, backStack: Boolean)
}