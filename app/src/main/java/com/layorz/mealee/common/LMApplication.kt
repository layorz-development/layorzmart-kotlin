package com.layorz.mealee.common

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.widget.Toast
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.layorz.mealee.R
import com.layorz.mealee.common.events.LMNetworkStateChange
import com.layorz.mealee.common.model.LMS3Credentials
import com.layorz.mealee.common.utils.*
import com.layorz.mealee.network.manager.LMApiManager
import com.layorz.mealee.report.util.LMAlarmReportGeneration
import org.greenrobot.eventbus.EventBus
import java.util.*

class LMApplication : Application() {
    private lateinit var preferences: LMPreferences
    private lateinit var apiManager: LMApiManager
    private val connectivityManager by lazy { getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    private val dailyNotificationManager by lazy { getSystemService(Context.ALARM_SERVICE) as AlarmManager }
    private val pdfgenerator by lazy { LMPdfUtil() }
    private val alarmIntent by lazy {
        PendingIntent.getBroadcast(
            this,
            0,
            Intent(this, LMAlarmReportGeneration::class.java),
            0
        )
    }
    private lateinit var transferUtility: TransferUtility
    private lateinit var amazonS3Client: AmazonS3Client
    private lateinit var bucketName: String
    private lateinit var folderName: String

    companion object {
        private lateinit var applicationInstance: LMApplication
        private val TAG = LMApplication::class.java.simpleName
        private var isNetworkConnected: Boolean = false

        fun getInstance(): LMApplication = applicationInstance

        fun isNetworkConnected(): Boolean = isNetworkConnected
    }

    override fun onCreate() {
        super.onCreate()
        initInstance()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(
            LMLocaleUtil.onAttach(
                base,
                LMConstant.LMLanguages.DEFAULT_LANGUAGE
            )
        )
    }

    private fun initInstance() {
        applicationInstance = this
        LMLifeCycleUtils.init(this)
        apiManager = LMApiManager()
        preferences = LMPreferences(this)
        startNetworkListener()
        if (preferences.installedDate == null) {
            preferences.installedDate = Date()
        }
        preferences.userData?.s3Credentials?.let {
            setTransferUtility(it)
        }
    }

    fun setTransferUtility(sheetDetailResponse: LMS3Credentials) {
        sheetDetailResponse.apply {
            amazonS3Client = AmazonS3Client(
                BasicAWSCredentials(
                    accessKey,
                    secretKey
                ), Region.getRegion(Regions.fromName(regionName))
            )
            bucketName?.let {
                this@LMApplication.bucketName = it
            }
            folderName?.let {
                this@LMApplication.folderName = it
            }
            transferUtility = TransferUtility.builder().s3Client(amazonS3Client)
                .context(this@LMApplication)
                .build()
        }
    }

    private fun startNetworkListener() {
        try {
            val builder = NetworkRequest.Builder()
            connectivityManager.registerNetworkCallback(builder.build(),
                object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        isNetworkConnected = true
                        EventBus.getDefault().post(LMNetworkStateChange(true))
                        LMLog.d(
                            "startNetworkListener",
                            "isNetworkChanged : $isNetworkConnected"
                        )
                    }

                    override fun onLost(network: Network) {
                        isNetworkConnected = false
                        EventBus.getDefault().post(LMNetworkStateChange(false))
                        LMLog.d(
                            "startNetworkListener",
                            "isNetworkChanged : $isNetworkConnected"
                        )
                    }
                })
        } catch (exception: Exception) {
            LMLog.e(
                TAG,
                "checkNetworkConnection: Exception: " + exception.localizedMessage
            )
        }
    }

    fun getPreference() = preferences

    fun getApiManager() = apiManager

    fun getAlarmManager() = dailyNotificationManager

    fun getAlarmPendingIntent(): PendingIntent = alarmIntent

    fun getPdfManage() = pdfgenerator

    fun getS3UploadManager() = if (::transferUtility.isInitialized) transferUtility else null

    fun getAwsS3Manager() = amazonS3Client

    fun getBucketName() = "$bucketName/$folderName"
}