package com.layorz.mealee.common

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import com.google.gson.Gson
import com.layorz.mealee.BuildConfig
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.network.response.LMUserResponse
import java.util.*

class LMPreferences(context: Context) {
    private val sharedPreference: SharedPreferences by lazy {
        context.getSharedPreferences(BuildConfig.SHARED_PERFERENCE, Context.MODE_PRIVATE)
    }

    companion object {
        private const val KEY_AUTH_TOKEN = "auth_token"
        private const val KEY_REFRESH_TOKEN = "refresh_token"
        private const val KEY_USER_DATA = "login_user_data"
        private const val REPORT_GENERATION_TIME = "report_generation_time"
        private const val KEY_EMAIL = "email"
        private const val LAST_SALES_REPORT_GENERATED_DATE = "last_sales_report_generated_date"
        private const val APP_INSTALLED_DATE = "app_installed_date"
        private const val KEY_LAST_VERSION_ALERT = "last_version_alert"
        private const val SUBSCRIPTION="subscription"
    }

    var authToken: String?
        get() = sharedPreference.getString(KEY_AUTH_TOKEN, null)
        set(atToken) = sharedPreference.edit().putString(KEY_AUTH_TOKEN, atToken).apply()

    var subscriptionStatus:String?
        get() = sharedPreference.getString(SUBSCRIPTION,null)
        set(value) = sharedPreference.edit().putString(SUBSCRIPTION,value).apply()

    var refreshToken: String?
        get() = sharedPreference.getString(KEY_USER_DATA, null)
        set(rfToken) = sharedPreference.edit().putString(KEY_USER_DATA, rfToken).apply()

    var userData: LMUserResponse?
        get() = Gson().fromJson(
            sharedPreference.getString(KEY_REFRESH_TOKEN, null),
            LMUserResponse::class.java
        )
        set(loginDetail) = sharedPreference.edit()
            .putString(KEY_REFRESH_TOKEN, Gson().toJson(loginDetail)).apply()

    var reportGenerationTime: String?
        get() = sharedPreference.getString(REPORT_GENERATION_TIME, null)
        set(timing) = sharedPreference.edit().putString(REPORT_GENERATION_TIME, timing).apply()

    var emailId: String
        get() = String(Base64.decode(sharedPreference.getString(KEY_EMAIL, ""), Base64.NO_WRAP))
        set(emailId) = sharedPreference.edit().putString(
            KEY_EMAIL, Base64.encodeToString(
                emailId.toByteArray(),
                Base64.NO_WRAP
            )
        ).apply()

    var lastSalesReportGeneratedDate: Date?
        get() = sharedPreference.getString(
            LAST_SALES_REPORT_GENERATED_DATE,
            null
        )?.let { LMUtilise.localData.parse(it) }
        set(date) = sharedPreference.edit()
            .putString(LAST_SALES_REPORT_GENERATED_DATE,
                date?.let { LMUtilise.localData.format(it) }).apply()

    var installedDate: Date?
        get() = sharedPreference.getString(
            APP_INSTALLED_DATE,
            null
        )?.let { LMUtilise.localData.parse(it) }
        set(date) = sharedPreference.edit()
            .putString(APP_INSTALLED_DATE,
                date?.let { LMUtilise.localData.format(it) }).apply()

    var lastUpdateDate: String?
        get() = sharedPreference.getString(KEY_LAST_VERSION_ALERT, null)
        set(alertDate) = sharedPreference.edit().putString(KEY_LAST_VERSION_ALERT, alertDate).apply()

    fun clearData() {
        val emailDetail = emailId
        val appInstalledDate = installedDate
        sharedPreference.edit().clear().apply()
        emailId = emailDetail
        installedDate = appInstalledDate
    }
}