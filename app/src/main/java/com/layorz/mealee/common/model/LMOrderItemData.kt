package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMOrderItemData {
    @SerializedName("item_id")
    @Expose
    var itemId = 0

    @SerializedName("item_name")
    @Expose
    var itemName: String? = null

    @SerializedName("category_id")
    @Expose
    var categoryId: Int = 0

    @SerializedName("type_id")
    @Expose
    var typeId: Int = 0

    @SerializedName("quantity")
    @Expose
    var quantity: Int = 0

    @SerializedName("price")
    @Expose
    var price: Int = 0

    @SerializedName("amount")
    @Expose
    var amount = 0f
}