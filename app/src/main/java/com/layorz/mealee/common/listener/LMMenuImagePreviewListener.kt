package com.layorz.mealee.common.listener

interface LMMenuImagePreviewListener {
    fun imageUrl(url: String)
}