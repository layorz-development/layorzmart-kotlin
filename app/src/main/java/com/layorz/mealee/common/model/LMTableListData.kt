package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class LMTableListData {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("table_name")
    @Expose
    var tableName: String? = null

    var isSelected: Boolean = false
}