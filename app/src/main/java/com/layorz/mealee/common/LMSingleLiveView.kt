package com.layorz.mealee.common

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.layorz.mealee.common.utils.LMLog
import java.util.concurrent.atomic.AtomicBoolean

class LMSingleLiveView<T> : MutableLiveData<T>() {
    private val TAG = LMSingleLiveView::class.java.simpleName
    private val pending by lazy { AtomicBoolean(false) }

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        if (hasActiveObservers()) {
            LMLog.w(TAG, "Multiple observers registered but only one will be notified of changes.")
        }
        super.observe(owner, Observer { t ->
            if (pending.compareAndSet(true, false)) {
                observer.onChanged(t)
            }
        })
    }

    @MainThread
    override fun setValue(t: T?) {
        pending.set(true)
        super.setValue(t)
    }

    @MainThread
    fun call() {
        value = null
    }
}