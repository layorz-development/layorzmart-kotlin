package com.layorz.mealee.common.listener

import com.layorz.mealee.network.response.LMMenuResponse

interface LMMenuItemClickListener {
    fun menuItem(
        menuClicked: LMMenuResponse,
        editButtonClicked: Boolean = false,
        deleteButtonClicked: Boolean = false
    )

    fun statusChanged(menuClicked: LMMenuResponse, position: Int)
}