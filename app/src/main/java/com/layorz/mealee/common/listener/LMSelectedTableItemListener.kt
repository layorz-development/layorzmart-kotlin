package com.layorz.mealee.common.listener

import com.layorz.mealee.common.model.LMTableListData

interface LMSelectedTableItemListener {
    fun selectTable(
        tableDetail: LMTableListData,
        adapterPosition: Int
    )
}