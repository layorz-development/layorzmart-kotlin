package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMSettingTimeData(
    @SerializedName("from")
    @Expose
    var from: String? = null,

    @SerializedName("to")
    @Expose
    var to: String? = null
) {

}