package com.layorz.mealee.common.model

class LMOrderDetail {
    var orderItemList: MutableList<LMOrderItemData>? = null
    var orderName: String? = null
    var tableName: String? = null
    var billNo: String? = null
    var billType: String? = null
    var subTotal: String? = null
    var discount: String? = null
    var total: String? = null
    var discountApplied: Boolean = false
}