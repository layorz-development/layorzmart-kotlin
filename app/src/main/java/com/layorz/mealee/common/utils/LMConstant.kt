package com.layorz.mealee.common.utils

import androidx.annotation.StringDef

object LMConstant {
    const val ALERT_POSITIVE_BUTTON = "alertButtonPositive"
    const val ALERT_NEGATIVE_BUTTON = "alertButtonNegative"
    const val UNKNOWN_HOST_EXCEPTION: String = "Unable to reach server"
    const val VIEW_MENU = "view_menu"
    const val CREATE_MENU = "create_menu"
    const val UPDATE_MENU = "update_menu"
    const val DELETE_MENU = "delete_menu"
    const val CREATE_PENDING_ORDER = "create_pending_order"
    const val UPDATE_PENDING_ORDER = "update_pending_order"
    const val DELETE_PENDING_ORDER = "delete_pending_order"
    const val CREATE_ORDER = "create_order"
    const val CREATE_FILTER="create_menu_filter"
    const val UPDATE_FILTER="edit_menu_filter"
    const val DELETE_FILTER="delete_menu_filter"
    const val GENERATE_SALES_REPORT = "generate_sales_report"
    const val CHANGE_MENU_SETTING = "change_menu_settings"
    const val CHANGE_REPORT_SETTING = "change_report_settings"
    const val CHANGE_LANGUAGE_SETTING = "change_language_settings"
    const val POST_FEEDBACK = "post_feedback"
    const val VIEW_ORDER = "view_order"
    const val ADMIN = "admin"
    const val CASHIER = "cashier"
    const val SERVANT = "servant"

    const val KEY_ERRORS = "errors"
    const val HTTPS = "https:"

    @StringDef()
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class LMLanguages {
        companion object {
            const val DEFAULT_LANGUAGE = "en"
            const val TAMIL_LANGUAGE = "ta"
        }
    }

    @StringDef()
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class LMAppVersion {
        companion object {
            const val NORMAL_UPDATE = "update"
            const val FORCE_UPDATE = "force"
            const val NONE = "none"
        }
    }
}