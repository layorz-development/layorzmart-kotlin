package com.layorz.mealee.common.listener

import com.layorz.mealee.common.model.LMMenuFilterData

interface LMCategoryMenuListener {
    fun categoryMenu(categoryDetail: LMMenuFilterData, position: Int)
}