package com.layorz.mealee.common.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import com.layorz.mealee.common.LMApplication
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object LMUtilise {
    private val TAG = LMUtilise::class.java.simpleName
    private val railwayTime by lazy { SimpleDateFormat("HH:mm a", Locale.ENGLISH) }
    private val hoursTime by lazy { SimpleDateFormat("hh", Locale.ENGLISH) }
    private val minutesTime by lazy { SimpleDateFormat("MM", Locale.ENGLISH) }
    val localTime by lazy { SimpleDateFormat("hh:mm a", Locale.ENGLISH) }
    val localData by lazy { SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH) }
    val priceFormat by lazy { DecimalFormat(".00") }
    val appVersionLastDeferredAt by lazy {
        SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss",
            Locale.ENGLISH
        )
    }

    fun hideKeyboard(currentScreen: Activity) {
        try {
            currentScreen.currentFocus?.let {
                (currentScreen.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
                    hideSoftInputFromWindow(
                        it.windowToken,
                        InputMethodManager.HIDE_NOT_ALWAYS
                    )
                }
            }
        } catch (exception: Exception) {
            LMLog.e(TAG, "hideKeyboard:", exception)
        }
    }

    fun numberOfOptionPerColumns(context: Context?): Int {
        var noOfColumns = 0
        if (context != null) {
            val displayMetrics = context.resources.displayMetrics
            if (displayMetrics != null) {
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                noOfColumns = (dpWidth / 175).toInt()
            }
            LMLog.d(TAG, "calculateNumberOfColumns: $noOfColumns")
        }
        return noOfColumns
    }

    fun numberOfMenuInList(context: Context?, viewWidth: Int): Int {
        val displayMetrics = context?.resources?.displayMetrics
        if (displayMetrics != null) {
            val dpWidth = viewWidth / displayMetrics.density
            if (viewWidth > 0) {
                return (dpWidth / 250).toInt()
            }
        }
        return 1
    }

    fun getLocalTime(railwayTimeData: String?): String? {
        try {
            railwayTimeData?.let {
                railwayTime.parse(it)?.let { date ->
                    return localTime.format(date)
                }
            }
        } catch (exception: Exception) {
            LMLog.e(TAG, "get LocalTime", exception)
        }
        return localTime.format(Calendar.getInstance().time)
    }

    fun getHourData(time: String?): Int {
        try {
            time?.let { timeData ->
                localTime.parse(timeData)?.let {
                    return hoursTime.format(it).toInt()
                }
            }
        } catch (exception: Exception) {
            LMLog.e(TAG, "get hourData", exception)
        }
        return hoursTime.format(Calendar.getInstance().time).toInt()
    }

    fun getMinuteData(time: String?): Int {
        try {
            time?.let { timeData ->
                localTime.parse(timeData)?.let {
                    return minutesTime.format(it).toInt()
                }
            }
        } catch (exception: Exception) {
            LMLog.e(TAG, "get MinuteData", exception)
        }
        return minutesTime.format(Calendar.getInstance().time).toInt()
    }

    fun getDayFormat(date: Int, month: Int, year: Int) =
        String.format("%02d", date) + "-" + String.format("%02d", month + 1) + "-" + year.toString()

    fun isOneDayCompleted(): Boolean {
        return LMApplication.getInstance().getPreference().lastUpdateDate?.let { last ->
            try {
                appVersionLastDeferredAt.parse(last)?.let {
                    val dateCurrent = Date()
                    val diff = dateCurrent.time - it.time
                    val diffHours = diff / (60 * 60 * 1000)
                    diffHours >= 24
                }
            } catch (exception: Exception) {
                LMLog.e(TAG, "isOneDaysCompleted: " + exception.message, exception)
                true
            }
        } ?: true
    }
}