package com.layorz.mealee.common.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle

object LMNavigation {

    fun <T : Activity> switchActivity(
        activity: Activity,
        destination: Class<in T>,
        bundle: Bundle? = null,
        flags: IntArray? = null
    ) {
        activity.startActivity(getIntent(activity, destination, bundle, flags))
    }

    fun <T : Activity> switchActivityForResult(
        activity: Activity,
        destination: Class<T>,
        requestCode: Int,
        bundle: Bundle?= null,
        flags: IntArray? = null
    ) {
        activity.startActivityForResult(
            getIntent(activity, destination, bundle, flags),
            requestCode
        )
    }

    private fun <T : Activity> getIntent(
        activity: Activity,
        destination: Class<in T>,
        bundle: Bundle?,
        flags: IntArray?
    ): Intent? {
        val launchIntent = Intent(activity, destination)
        if (flags != null) {
            for (flag in flags) {
                launchIntent.addFlags(flag)
            }
        }
        if (bundle != null) {
            launchIntent.putExtras(bundle)
        }
        return launchIntent
    }
}