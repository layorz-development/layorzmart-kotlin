package com.layorz.mealee.common.listener

import com.layorz.mealee.network.response.LMOrderResponse

interface LMPrintTableOrderItem {
    fun printTableOrder(tableOrder: LMOrderResponse)
}