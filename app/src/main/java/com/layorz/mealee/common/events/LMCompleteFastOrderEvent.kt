package com.layorz.mealee.common.events

import com.layorz.mealee.network.request.LMCreateFastOrderRequest
import com.layorz.mealee.network.request.LMCreateOrderRequest

class LMCompleteFastOrderEvent (
    var orderDetails:LMCreateOrderRequest,
    var canPrintOrder:Boolean,
    var Order:ArrayList<LMCreateFastOrderRequest>
)