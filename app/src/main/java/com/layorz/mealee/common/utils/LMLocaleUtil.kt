package com.layorz.mealee.common.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.preference.PreferenceManager
import java.util.*

object LMLocaleUtil {
    private const val SELECTED_LANGUAGE = "Locale.Helper.Selected.Language"

    fun onAttach(context: Context?) =
        setLocale(context, getPersistedData(context, Locale.getDefault().language))

    fun onAttach(
        context: Context?,
        defaultLanguage: String
    ) = setLocale(context, getPersistedData(context, defaultLanguage))

    fun getLanguage(context: Context): String =
        getPersistedData(context, Locale.getDefault().language) ?: Locale.getDefault().language

    /**
     * To set the locale and update the persistent data.
     *
     * @param context  The context.
     * @param language The selected language.
     */
    fun setLocale(
        context: Context?,
        language: String?
    ): Context? {
        persist(context, language)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        }
        return updateResourcesLegacy(context, language)
    }

    private fun updateResources(context: Context?, language: String?): Context? {
        val locale = Locale(language)
        Locale.setDefault(locale)
        return (context?.resources?.configuration)?.apply {
            setLocale(locale)
            setLayoutDirection(locale)
        }?.let { context.createConfigurationContext(it) }
    }

    /**
     * To get the stored selected language.
     *
     * @param context         The context.
     * @param defaultLanguage The stored language.
     */
    private fun getPersistedData(
        context: Context?,
        defaultLanguage: String
    ) = PreferenceManager.getDefaultSharedPreferences(context)
        .getString(SELECTED_LANGUAGE, defaultLanguage)

    /**
     * To store the selected language.
     *
     * @param context  The context.
     * @param language The selected language.
     */
    private fun persist(
        context: Context?,
        language: String?
    ) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()?.apply {
            putString(SELECTED_LANGUAGE, language)
            apply()
        }
    }

    /**
     * To update the resources for selected language.
     *
     * @param context  The context.
     * @param language The selected language.
     */
    private fun updateResourcesLegacy(
        context: Context?,
        language: String?
    ): Context? {
        Locale(language).apply {
            Locale.setDefault(this)
            context?.resources?.let {
                val configuration: Configuration = it.configuration
                configuration.locale = this
                configuration.setLayoutDirection(this)
                it.updateConfiguration(configuration, it.displayMetrics)
            }
        }
        return context
    }
}