package com.layorz.mealee.common

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.layorz.mealee.BuildConfig
import com.layorz.mealee.R
import com.layorz.mealee.common.callbacks.LMPopCurrentFragmentCallback
import com.layorz.mealee.common.callbacks.LMProgressBarCallback
import com.layorz.mealee.common.callbacks.LMReplaceCurrentFragmentCallback
import com.layorz.mealee.common.callbacks.LMSetToolbarCallBack
import com.layorz.mealee.common.events.*
import com.layorz.mealee.common.utils.*
import com.layorz.mealee.login.LMLoginActivity
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMAppVersionResponse
import com.layorz.mealee.other.LMSubscriptionActivity
import com.layorz.mealee.repository.LMSettingRepository
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.lm_base_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

open class LMActivity : AppCompatActivity(), LMPopCurrentFragmentCallback, LMProgressBarCallback,
    LMReplaceCurrentFragmentCallback, LMSetToolbarCallBack {
    private val TAG = LMActivity::class.java.simpleName
    private var isProgressShowing = false
    private var relativeLayout: RelativeLayout? = null
    private var mWindow: Window? = null
    private lateinit var appVersionData: LMAppVersionResponse

    private val lifeCycleListener by lazy {
        object : LMLifeCycleUtils.Listener {
            override fun onBecameForeground() {
                LMLog.d(TAG, "onBecameForeground")
                checkApiVersion()
            }

            override fun onBecameBackground() {
                LMLog.d(TAG, "onBecameBackground")
            }
        }
    }

    companion object {
        private const val APP_CHECK = "app_check"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LMLifeCycleUtils.get(this).addListener(lifeCycleListener)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE or WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LMLocaleUtil.onAttach(base))
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        LMLifeCycleUtils.get(this).removeListener(lifeCycleListener)
    }

    override fun setToolbar(toolbarTitle: String, isHomeScreenNeed: Boolean) {
        setSupportActionBar(tl_base_toolbar as Toolbar)
        supportActionBar?.apply {
            title = toolbarTitle
            setDisplayHomeAsUpEnabled(isHomeScreenNeed)
            setDisplayShowHomeEnabled(isHomeScreenNeed)
        }
    }

    override fun updateProgressBar(visibleMode: Boolean) {
        if (visibleMode) {
            if (!isProgressShowing) {
                isProgressShowing = true
                mWindow = window
                mWindow?.setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                )
                relativeLayout = RelativeLayout(this)
                val progressBar = ProgressBar(this)
                progressBar.isIndeterminate = true
                val paramsForProgressBar = RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                )
                paramsForProgressBar.addRule(RelativeLayout.CENTER_IN_PARENT)
                relativeLayout?.addView(progressBar, paramsForProgressBar)
                val paramsForRelativeLayout = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
                )
                relativeLayout?.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.colorProgressBar)
                )
                paramsForRelativeLayout.gravity = Gravity.CENTER
                addContentView(relativeLayout, paramsForRelativeLayout)
                Blurry.with(this)
                    .async()
                    .onto(relativeLayout)
            }
        } else {
            isProgressShowing = false
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            (relativeLayout?.parent as? ViewGroup?)?.removeView(relativeLayout)
        }
    }

    override fun replaceCurrent(changeFragment: Fragment, containerId: Int, backStack: Boolean) {
        this.supportFragmentManager.beginTransaction().apply {
            LMUtilise.hideKeyboard(currentScreen = this@LMActivity)
            replace(containerId, changeFragment)
            if (backStack) {
                addToBackStack(null)
            }
            commit()
        }
    }

    override fun popFragment() {
        updateProgressBar(false)
        this.supportFragmentManager.apply {
            if (backStackEntryCount > 0) {
                popBackStack()
            } else {
                LMUtilise.hideKeyboard(currentScreen = this@LMActivity)
                finish()
            }
        }
    }

    override fun onBackPressed() {
        updateProgressBar(false)
        super.onBackPressed()
    }

    protected open fun onPositiveButtonClick(alertKey: String?) {
    }

    protected open fun onNegativeButtonClick(alertKey: String) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAlertButtonClickedEvent(alertButtonClickEvent: LMAlertButtonClick) {
        val alertKey: String = alertButtonClickEvent.alertKey
        val buttonType: String = alertButtonClickEvent.buttonType
        if (LMConstant.ALERT_POSITIVE_BUTTON == buttonType) {
            if (alertKey == APP_CHECK) {
                EventBus.getDefault().removeStickyEvent(appVersionData)
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=${BuildConfig.APPLICATION_ID}")
                        )
                    )
                } catch (exception: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
                        )
                    )
                }
            } else {
                onPositiveButtonClick(alertKey)
            }
        } else if (LMConstant.ALERT_NEGATIVE_BUTTON == buttonType) {
            if (alertKey == APP_CHECK) {
                LMApplication.getInstance().getPreference().lastUpdateDate =
                    LMUtilise.appVersionLastDeferredAt.format(Date())
                EventBus.getDefault().removeStickyEvent(appVersionData)
            } else {
                onNegativeButtonClick(alertKey)
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLogoutUser(logoutUser: LMLogoutUserEvent) {
        LMApplication.getInstance().apply {
            getApiManager().cancelAllRequests()
            getPreference().clearData()
        }
        LMNavigation.switchActivity(
            this,
            LMLoginActivity::class.java,
            flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSubscriptionEnded(subscriptionEnd: LMSubscriptionEndEvent) {
        LMApplication.getInstance().apply {
            getApiManager().cancelAllRequests()
        }
        LMNavigation.switchActivity(
            this,
            LMSubscriptionActivity::class.java,
            flags = intArrayOf(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun showProgressEvent(progressVisible: LMShowProgressEvent) {
        updateProgressBar(progressVisible.visibility)
    }

    private fun checkApiVersion() {
        LMSettingRepository.getApiVersion(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                (data as? LMAppVersionResponse)?.let { it ->
                    EventBus.getDefault().postSticky(it)
                }
            }

            override fun <T> onError(errorMessage: T) {}

            override fun onFailure(failureMessage: String?) {}

            override fun onNoNetwork() {
                Toast.makeText(this@LMActivity, getString(R.string.no_network), Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun networkChange(networkChange: LMNetworkStateChange) {
        if (networkChange.isNetworkAvailable) {
            checkApiVersion()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun appVersion(appVersion: LMAppVersionResponse) {
        val appVersionType = appVersion.appStatus == LMConstant.LMAppVersion.NORMAL_UPDATE
        if (appVersion.appStatus != LMConstant.LMAppVersion.NONE &&
            ((appVersionType && LMUtilise.isOneDayCompleted()) ||
                    (appVersion.appStatus == LMConstant.LMAppVersion.FORCE_UPDATE))
        ) {
            appVersionData = appVersion
            LMAlertUtil.showAlertDialog(
                fragmentActivity = this,
                message = appVersion.message
                    ?: getString(if (appVersionType) R.string.normal_update else R.string.force_update),
                title = getString(R.string.info),
                positiveButtonText = getString(R.string.update),
                negativeButtonText = getString(R.string.later),
                isCancelNeeded = appVersionType,
                alertKey = APP_CHECK
            )
        }
    }
}