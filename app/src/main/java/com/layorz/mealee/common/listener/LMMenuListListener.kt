package com.layorz.mealee.common.listener

import com.layorz.mealee.common.model.LMMenuItemData

interface LMMenuListListener {
    fun menuList(clickedItem: LMMenuItemData)
}