package com.layorz.mealee.common.callbacks

interface LMPopCurrentFragmentCallback {
    fun popFragment()
}