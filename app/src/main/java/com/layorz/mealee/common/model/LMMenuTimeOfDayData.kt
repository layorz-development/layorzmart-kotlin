package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMMenuTimeOfDayData {
    @SerializedName("night")
    @Expose
    var night: Boolean = false

    @SerializedName("evening")
    @Expose
    var evening: Boolean = false

    @SerializedName("morning")
    @Expose
    var morning: Boolean = false

    @SerializedName("afternoon")
    @Expose
    var afternoon: Boolean = false
}