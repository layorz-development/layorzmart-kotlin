package com.layorz.mealee.common

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.layorz.mealee.R
import com.layorz.mealee.common.events.LMAlertButtonClick
import com.layorz.mealee.common.utils.LMConstant
import org.greenrobot.eventbus.EventBus


class LMAlertDialogFragment : DialogFragment() {

    private lateinit var mContext: Context

    companion object {
        private const val BUNDLE_KEY_TITLE = "title"
        private const val BUNDLE_KEY_MESSAGE = "message"
        private const val BUNDLE_KEY_POSITIVE_BUTTON_TEXT = "positiveButtonText"
        private const val BUNDLE_KEY_NEGATIVE_BUTTON_TEXT = "negativeButtonText"
        private const val BUNDLE_KEY_IS_CANCEL_NEEDED = "isCancelNeeded"
        private const val BUNDLE_KEY_ALERT_KEY = "alertKey"

        fun newInstance(
            title: String?,
            message: String,
            isCancelNeeded: Boolean,
            negativeButtonText: String?,
            positiveButtonText: String?,
            alertKey: String?
        ) = LMAlertDialogFragment().apply {
            arguments = bundleOf(
                BUNDLE_KEY_TITLE to title,
                BUNDLE_KEY_MESSAGE to message,
                BUNDLE_KEY_IS_CANCEL_NEEDED to isCancelNeeded,
                BUNDLE_KEY_NEGATIVE_BUTTON_TEXT to negativeButtonText,
                BUNDLE_KEY_POSITIVE_BUTTON_TEXT to positiveButtonText,
                BUNDLE_KEY_ALERT_KEY to alertKey
            )
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog =
        AlertDialog.Builder(context).apply {
            val arguments = arguments
            arguments?.apply {
                setTitle(getString(BUNDLE_KEY_TITLE, ""))
                setMessage(getString(BUNDLE_KEY_MESSAGE, ""))
                val positiveButtonText = getString(
                    BUNDLE_KEY_POSITIVE_BUTTON_TEXT,
                    ""
                )
                val positiveButtonTitle =
                    if (TextUtils.isEmpty(positiveButtonText)) getString(R.string.ok) else positiveButtonText
                setPositiveButton(
                    positiveButtonTitle
                ) { _, _ ->
                    EventBus.getDefault().post(
                        LMAlertButtonClick(
                            LMConstant.ALERT_POSITIVE_BUTTON,
                            getString(BUNDLE_KEY_ALERT_KEY, "")
                        )
                    )
                }

                val isCancelButtonNeeded = getBoolean(
                    BUNDLE_KEY_IS_CANCEL_NEEDED,
                    false
                )
                if (isCancelButtonNeeded) {
                    val negativeButtonText = getString(
                        BUNDLE_KEY_NEGATIVE_BUTTON_TEXT,
                        ""
                    )
                    val negativeButtonTitle =
                        if (TextUtils.isEmpty(negativeButtonText)) getString(R.string.cancel) else negativeButtonText
                    setNegativeButton(negativeButtonTitle) { _, _ ->
                        EventBus.getDefault().post(
                            LMAlertButtonClick(
                                LMConstant.ALERT_NEGATIVE_BUTTON,
                                getString(BUNDLE_KEY_ALERT_KEY, "")
                            )
                        )
                    }
                }
                isCancelable = false
            }

        }.create()
}