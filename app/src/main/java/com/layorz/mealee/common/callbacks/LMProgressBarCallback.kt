package com.layorz.mealee.common.callbacks

interface LMProgressBarCallback {
    fun updateProgressBar(visibleMode: Boolean)
}