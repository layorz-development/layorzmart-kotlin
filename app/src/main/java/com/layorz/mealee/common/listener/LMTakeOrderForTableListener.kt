package com.layorz.mealee.common.listener

import com.layorz.mealee.network.response.LMOrderResponse

interface LMTakeOrderForTableListener {
    fun takeOrderForTable(tableName: String)

    fun completeTableOrder(tableOrder: LMOrderResponse)
}