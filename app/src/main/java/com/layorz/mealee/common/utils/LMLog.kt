package com.layorz.mealee.common.utils

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.layorz.mealee.BuildConfig

object LMLog {
    fun v(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.v(tag, message)
    }

    fun d(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message)
    }

    fun i(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.i(tag, message)
    }

    fun w(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.w(tag, message)
    }

    fun e(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.e(tag, message)
    }

    fun e(tag: String?, message: String, throwable: Throwable) {
        val exceptionDetails = throwable.javaClass.simpleName + " - " + throwable.message
        FirebaseCrashlytics.getInstance().apply {
            log("$tag : Exception type - $exceptionDetails, log message: $message")
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
        Log.e(tag, message)
    }
}