package com.layorz.mealee.common

import android.content.Context
import androidx.fragment.app.Fragment
import com.layorz.mealee.common.callbacks.*
import com.layorz.mealee.common.events.LMAlertButtonClick
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.common.utils.LMLog
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class LMFragment : Fragment() {
    private val TAG: String = LMFragment::class.java.simpleName
    private var isProgressBarVisibility = false
    private var progressBar: LMProgressBarCallback? = null
    private var replaceCurrentFragment: LMReplaceCurrentFragmentCallback? = null
    private var setToolbar: LMSetToolbarCallBack? = null
    private var dashBoardTitle: LMDashboardTitleCallback? = null
    private var popCurrentFragment: LMPopCurrentFragmentCallback? = null

    protected lateinit var currentContext: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
        try {
            progressBar = context as? LMProgressBarCallback?
            replaceCurrentFragment = context as? LMReplaceCurrentFragmentCallback?
            setToolbar = context as? LMSetToolbarCallBack?
            dashBoardTitle = context as? LMDashboardTitleCallback
            popCurrentFragment = context as? LMPopCurrentFragmentCallback?
        } catch (exception: ClassCastException) {
            LMLog.e(TAG, "onAttach: ClassCastException:", exception)
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    protected fun updateProgress(shouldShowProgress: Boolean) {
        progressBar?.updateProgressBar(shouldShowProgress)
    }

    protected fun popFragment() {
        popCurrentFragment?.popFragment()
    }

    protected fun replaceFragment(
        fragmentToReplace: Fragment,
        containerId: Int,
        addToBackStack: Boolean
    ) {
        replaceCurrentFragment?.replaceCurrent(fragmentToReplace, containerId, addToBackStack)
    }

    protected fun setToolbar(title: String, isHomeUpNeed: Boolean = true) {
        setToolbar?.setToolbar(title, isHomeUpNeed)
    }

    protected fun setDashboardTitle(dashboardName: String) {
        dashBoardTitle?.dashBoardTitle(dashboardName)
    }

    protected open fun onPositiveButtonClick(alertKey: String?) {
    }

    protected open fun onNegativeButtonClick(alertKey: String) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAlertButtonClickedEvent(alertButtonClickEvent: LMAlertButtonClick) {
        val alertKey: String = alertButtonClickEvent.alertKey
        val buttonType: String = alertButtonClickEvent.buttonType
        if (LMConstant.ALERT_POSITIVE_BUTTON == buttonType) {
            onPositiveButtonClick(alertKey)
        } else if (LMConstant.ALERT_NEGATIVE_BUTTON == buttonType) {
            onNegativeButtonClick(alertKey)
        }
    }
}