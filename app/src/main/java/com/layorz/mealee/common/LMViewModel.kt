package com.layorz.mealee.common

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.R

open class LMViewModel(application: Application) : AndroidViewModel(application) {
    protected val currentViewModel: Context = application
    val apiResponseMessage: LMSingleLiveView<String> = LMSingleLiveView()
    val isProgressShowing by lazy { MutableLiveData<Boolean>() }
    val isPullToRefresh by lazy { MutableLiveData<Boolean>() }
    val apiResponseInId by lazy { MutableLiveData<Int>() }

    protected fun handleApiResponse(response: String?) {
        isProgressShowing.value = false
        isPullToRefresh.value = false
        if (response != null) {
            apiResponseMessage.value = response
        } else {
            apiResponseInId.value = R.string.something_went_wrong
        }
    }

    protected fun handleApiResponse(response: Int?) {
        isProgressShowing.value = false
        isPullToRefresh.value = false
        if (response != null) {
            apiResponseInId.value = response
        } else {
            apiResponseInId.value = R.string.something_went_wrong
        }
    }

    protected fun showNoNetworkToast() {
        isProgressShowing.value = false
        isPullToRefresh.value = false
        apiResponseInId.value = R.string.no_network
    }
}