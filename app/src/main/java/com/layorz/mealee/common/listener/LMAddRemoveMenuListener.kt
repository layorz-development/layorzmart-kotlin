package com.layorz.mealee.common.listener

import com.layorz.mealee.network.response.LMMenuResponse

interface LMAddRemoveMenuListener {
    fun addRemoveMenu(
        menuItem: LMMenuResponse,
        adapterPosition: Int
    )
}