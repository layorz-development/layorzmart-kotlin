package com.layorz.mealee.common

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.PermUtil
import com.layorz.mealee.BuildConfig
import com.layorz.mealee.R
import com.layorz.mealee.common.model.LMOrderDetail
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMLog
import com.layorz.mealee.network.response.LMSalesReportResponse
import java.io.File
import java.util.*

abstract class LMPermissionFragment : LMFragment() {
    private var orderDetail: LMOrderDetail? = null
    private var task: String? = null
    private var summaryReport: LMSalesReportResponse? = null
    private var date: String? = null
    private var isCreateNormalBill = false

    companion object {
        private val TAG = LMPermissionFragment::class.java.simpleName
        private const val REQUEST_CODE_CREATE_BILL = 1000
        private const val REQUEST_CODE_SALES_REPORT = 1002
        private const val REQUEST_CODE_PERMISSION_SETTINGS = 1004
        private const val REQUEST_CODE_IMAGE_PICK = 1006
        private const val PERMISSION_ALERT_TAG = "permission_alert_tag"
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(
                        this,
                        Options.init().setRequestCode(REQUEST_CODE_IMAGE_PICK).setCount(1)
                    )
                } else {
                    Toast.makeText(
                        currentContext,
                        "Approve permissions to open ImagePicker",
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
            REQUEST_CODE_CREATE_BILL, REQUEST_CODE_SALES_REPORT -> {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) && checkStoragePermission()) {
                    LMAlertUtil.showAlertDialog(
                        fragmentActivity = currentContext as FragmentActivity,
                        message = getString(R.string.enable_storage_permission),
                        title = resources.getString(R.string.info),
                        isCancelNeeded = true,
                        positiveButtonText = getString(R.string.settings),
                        negativeButtonText = getString(R.string.cancel),
                        alertKey = PERMISSION_ALERT_TAG
                    )
                } else {
                    if (requestCode == REQUEST_CODE_CREATE_BILL) {
                        createBill(orderDetail)
                    } else {
                        createDailySalesBill(task, summaryReport, date)
                    }
                    completeFragement()
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_PERMISSION_SETTINGS -> {
                if (isCreateNormalBill) {
                    createBill(orderDetail)
                } else {
                    createDailySalesBill(task, summaryReport, date)
                }
                completeFragement()
            }
            REQUEST_CODE_IMAGE_PICK -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.apply {
                        val imageName = File(this[0])
                        if (!imageName.name.toLowerCase(Locale.getDefault()).endsWith("gif")) {
                            imageUrl(imageName.absolutePath)
                        }
                    }
                }
            }
        }
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == PERMISSION_ALERT_TAG) {
            try {
                startActivityForResult(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                    ), REQUEST_CODE_PERMISSION_SETTINGS
                )
            } catch (exception: Exception) {
                LMLog.e(TAG, "Move to settings page Error : " + exception.message)
                startActivityForResult(
                    Intent(Settings.ACTION_SETTINGS),
                    REQUEST_CODE_PERMISSION_SETTINGS
                )
            }
        }
    }

    protected fun createBill(orderDetail: LMOrderDetail?) {
        orderDetail?.let {
            this.orderDetail = orderDetail
            isCreateNormalBill = true
            if (getStoragePermission(REQUEST_CODE_CREATE_BILL)) {
                LMApplication.getInstance().getPdfManage().createBill(currentContext, orderDetail)
            }
        }
    }

    protected fun createDailySalesBill(
        task: String?,
        summaryReport: LMSalesReportResponse?,
        date: String?
    ) {
        summaryReport?.let {
            task?.let {
                date?.let {
                    this.task = task
                    this.summaryReport = summaryReport
                    this.date = date
                    isCreateNormalBill = false
                    if (getStoragePermission(REQUEST_CODE_SALES_REPORT)) {
                        LMApplication.getInstance().getPdfManage().createDailySalesBill(
                            task,
                            currentContext,
                            summaryReport,
                            date
                        )
                    }
                }
            }
        }
    }

    protected fun pickImage() {
        Pix.start(this, Options.init().setRequestCode(REQUEST_CODE_IMAGE_PICK).setCount(1))
    }

    private fun getStoragePermission(requestCode: Int): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkStoragePermission()) {
                requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    requestCode
                )
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    private fun checkStoragePermission() = ContextCompat.checkSelfPermission(
        currentContext,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) != PackageManager.PERMISSION_GRANTED

    abstract fun imageUrl(imagePath: String)

    abstract fun completeFragement()
}