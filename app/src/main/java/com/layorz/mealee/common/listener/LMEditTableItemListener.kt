package com.layorz.mealee.common.listener

import com.layorz.mealee.common.model.LMOrderItemData

interface LMEditTableItemListener {
    fun editTableItem(
        parentPosition: Int,
        orderItemData: LMOrderItemData
    )
}