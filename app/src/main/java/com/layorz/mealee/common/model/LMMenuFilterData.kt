package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMMenuFilterData {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    var nameId: Int? = null
    var isSelected = false
}