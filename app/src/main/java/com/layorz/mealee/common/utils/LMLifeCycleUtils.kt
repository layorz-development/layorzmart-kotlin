package com.layorz.mealee.common.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.Handler
import java.util.concurrent.CopyOnWriteArrayList

class LMLifeCycleUtils : Application.ActivityLifecycleCallbacks {
    private val CHECK_DELAY: Long = 500
    private val TAG = LMLifeCycleUtils::class.java.name
    private val handler = Handler()
    private val listeners: MutableList<Listener> = CopyOnWriteArrayList()
    private var foreground = false
    private var paused = true
    private var check: Runnable? = null

    companion object {
        private lateinit var instance: LMLifeCycleUtils

        fun init(application: Application): LMLifeCycleUtils {
            if (!::instance.isInitialized) {
                instance = LMLifeCycleUtils()
                application.registerActivityLifecycleCallbacks(instance)
            }
            return instance
        }

        fun get(application: Application): LMLifeCycleUtils? {
            if (!::instance.isInitialized) {
                init(application)
            }
            return instance
        }

        fun get(ctx: Context): LMLifeCycleUtils {
            if (!::instance.isInitialized) {
                val appCtx = ctx.applicationContext
                if (appCtx is Application) {
                    init(appCtx)
                }
                throw IllegalStateException("Foreground is not initialised and " + "cannot obtain the Application object")
            }
            return instance
        }

        fun get(): LMLifeCycleUtils {
            checkNotNull(instance) { "Foreground is not initialised - invoke " + "at least once with parameterised init/get" }
            return instance
        }
    }

    fun isForeground(): Boolean = foreground

    fun isBackground(): Boolean = !foreground

    fun addListener(listener: Listener) {
        listeners.add(listener)
    }

    fun removeListener(listener: Listener) {
        listeners.remove(listener)
    }

    override fun onActivityResumed(activity: Activity) {
        paused = false
        val wasBackground = !foreground
        foreground = true
        check?.let { handler.removeCallbacks(it) }
        if (wasBackground) {
            LMLog.i(TAG, "went foreground")
            for (l in listeners) {
                try {
                    l.onBecameForeground()
                } catch (exc: Exception) {
                    LMLog.e(TAG, "Listener threw exception! $exc")
                }
            }
        }
    }

    override fun onActivityPaused(activity: Activity) {
        paused = true
        check?.let { handler.removeCallbacks(it) }
        check = Runnable {
            if (foreground && paused) {
                foreground = false
                LMLog.i(TAG, "went background")
                for (l in listeners) {
                    try {
                        l.onBecameBackground()
                    } catch (exc: Exception) {
                        LMLog.e(TAG, "Listener threw exception! $exc")
                    }
                }
            }
        }
        check?.let { handler.postDelayed(it, CHECK_DELAY) }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}

    override fun onActivityStarted(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {}

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

    override fun onActivityDestroyed(activity: Activity) {}

    interface Listener {
        fun onBecameForeground()

        fun onBecameBackground()
    }
}