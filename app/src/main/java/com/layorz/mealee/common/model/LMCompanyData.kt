package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LMCompanyData {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("address1")
    @Expose
    var address1: String? = null

    @SerializedName("address2")
    @Expose
    var address2: String? = null

    @SerializedName("city")
    @Expose
    var city: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("country")
    @Expose
    var country: String? = null

    @SerializedName("phone_no")
    @Expose
    var phoneNo: String? = null
}