package com.layorz.mealee.common.model

import com.layorz.mealee.network.response.LMOrderResponse

class LMOrderCompleteDetail {
    var orderName: String? = null
    var orderDetail: MutableList<LMOrderResponse>? = null
    var isExpanded = false
}