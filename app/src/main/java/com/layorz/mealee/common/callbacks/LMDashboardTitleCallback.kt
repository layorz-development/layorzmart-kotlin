package com.layorz.mealee.common.callbacks

interface LMDashboardTitleCallback {
    fun dashBoardTitle(dashBoardName: String)
}