package com.layorz.mealee.common.callbacks

interface LMSetToolbarCallBack {
    fun setToolbar(toolbarTitle: String, isHomeScreenNeed: Boolean = true)
}