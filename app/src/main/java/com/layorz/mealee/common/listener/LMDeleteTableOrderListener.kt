package com.layorz.mealee.common.listener

interface LMDeleteTableOrderListener {
    fun deleteTableOrder(tableName: String, orderName: String)
}