package com.layorz.mealee.common.listener

import com.layorz.mealee.network.response.LMOrderResponse

interface LMSelectTableOrderListener {
    fun selectTableOrder(
        tableDetail: LMOrderResponse,
        adapterPosition: Int
    )
}