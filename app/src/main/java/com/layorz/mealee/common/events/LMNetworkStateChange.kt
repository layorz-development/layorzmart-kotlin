package com.layorz.mealee.common.events

class LMNetworkStateChange (val isNetworkAvailable: Boolean)