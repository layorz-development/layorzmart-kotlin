package com.layorz.mealee.common.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.layorz.mealee.common.LMAlertDialogFragment

object LMAlertUtil {
    private val TAG: String = LMAlertUtil::class.java.simpleName
    private const val ALERT_DIALOG_TAG = "alert_dialog"
    private const val APP_VERSION_DIALOG_TAG = "app_version_dialog"

    /**
     * To show alert dialog with given title and message along with their buttons
     *
     * @param title              The title to be displayed in the alert
     * @param message            The message to be displayed in the alert
     * @param isCancelNeeded     boolean to specify whether negative(Cancel) button is needed or not
     * @param negativeButtonText The text for the negative button
     * @param positiveButtonText The text for the positive button
     * @param fragmentActivity   The instance of the fragmentActivity needed to remove the existing dialog & to show this dialog
     */
    fun showAlertDialog(
        fragmentActivity: FragmentActivity,
        message: String,
        title: String? = null,
        isCancelNeeded: Boolean = false,
        positiveButtonText: String? = null,
        negativeButtonText: String? = null,
        alertKey: String? = null,
        dialogTag: String = ALERT_DIALOG_TAG
    ) {
        try {
            removeExistingDialog(fragmentActivity)
            removeExistingDialog(fragmentActivity, APP_VERSION_DIALOG_TAG)
            val alertDialogFragment: LMAlertDialogFragment = LMAlertDialogFragment.newInstance(
                title,
                message,
                isCancelNeeded,
                negativeButtonText,
                positiveButtonText,
                alertKey
            )
            alertDialogFragment.show(fragmentActivity.supportFragmentManager, dialogTag)
        } catch (exception: Exception) {
            LMLog.e(TAG, "showAlertDialog: Caught exception: ", exception)
        }
    }

    /**
     * To remove the existing dialog shown
     *
     * @param fragmentActivity The instance of the fragmentActivity needed to get the fragmentManager
     */
    fun removeExistingDialog(fragmentActivity: FragmentActivity,dialogTag: String = ALERT_DIALOG_TAG) {
        try {
            val fragmentManager: FragmentManager = fragmentActivity.supportFragmentManager
            val fragment: Fragment? = fragmentManager.findFragmentByTag(dialogTag)
            fragment?.let { fragmentManager.beginTransaction().remove(it).commit() }
        } catch (exception: Exception) {
            LMLog.e(TAG, "removeExistingDialog: Caught exception: ", exception)
        }
    }
}