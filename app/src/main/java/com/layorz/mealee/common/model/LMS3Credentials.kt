package com.layorz.mealee.common.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LMS3Credentials {
    @SerializedName("access_key")
    @Expose
    var accessKey: String? = null

    @SerializedName("secret_key")
    @Expose
    var secretKey: String? = null

    @SerializedName("region")
    @Expose
    var regionName: String? = null

    @SerializedName("bucket")
    @Expose
    var bucketName: String? = null

    @SerializedName("folder")
    @Expose
    var folderName: String? = null
}