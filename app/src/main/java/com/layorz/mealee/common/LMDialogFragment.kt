package com.layorz.mealee.common

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.layorz.mealee.common.events.LMShowProgressEvent
import org.greenrobot.eventbus.EventBus

open class LMDialogFragment : DialogFragment() {
    protected lateinit var currentContext: Context

    companion object {
        private val TAG = LMDialogFragment::class.java.simpleName
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
    }

    override fun onDestroyView() {
        dialog?.setDismissMessage(null);
        super.onDestroyView()
    }

    protected fun updateProgress(shouldShowProgress: Boolean) {
        EventBus.getDefault().post(LMShowProgressEvent().apply {
            visibility = shouldShowProgress
        })
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        updateProgress(false)
    }
}