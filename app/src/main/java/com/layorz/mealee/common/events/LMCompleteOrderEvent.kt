package com.layorz.mealee.common.events

import com.layorz.mealee.network.response.LMOrderResponse

class LMCompleteOrderEvent(
    val isCompletingOneTable: Boolean,
    val canPrintOrder: Boolean,
    val orderDetail: LMOrderResponse
) {
}