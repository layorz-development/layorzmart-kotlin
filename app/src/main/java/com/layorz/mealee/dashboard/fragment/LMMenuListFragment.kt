package com.layorz.mealee.dashboard.fragment

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.layorz.mealee.R
import com.layorz.mealee.common.LMFragment
import com.layorz.mealee.common.listener.LMMenuListListener
import com.layorz.mealee.common.model.LMMenuItemData
import com.layorz.mealee.common.utils.LMUtilise
import com.layorz.mealee.dashboard.adapter.LMMenuListAdapter
import kotlinx.android.synthetic.main.lm_fragment_menulist.*

class LMMenuListFragment : LMFragment(),
    LMMenuListListener {
    private var menuListListener: LMMenuListListener? = null

    companion object {
        fun newInstance() =
            LMMenuListFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        menuListListener = context as LMMenuListListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lm_fragment_menulist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setToolbar(getString(R.string.order_manage), resources.configuration.orientation==Configuration.ORIENTATION_PORTRAIT)
    }

    private fun initView() {
        rv_menu_item.apply {
            layoutManager = GridLayoutManager(
                currentContext,
                LMUtilise.numberOfOptionPerColumns(currentContext)
            )
            adapter = LMMenuListAdapter(
                getMenuData(),
                this@LMMenuListFragment
            )
        }
    }

    private fun getMenuData() = mutableListOf<LMMenuItemData>().apply {
        add(LMMenuItemData(R.drawable.ic_item_show_menu))
        add(LMMenuItemData(R.drawable.ic_item_take_order))
        add(LMMenuItemData(R.drawable.ic_item_pending_order))
        add(LMMenuItemData(R.drawable.ic_item_complete_order))
        add(LMMenuItemData(R.drawable.ic_item_show_order_item))
        add(LMMenuItemData(R.drawable.ic_item_setting_order))
    }

    override fun menuList(clickedItem: LMMenuItemData) {
        menuListListener?.menuList(clickedItem)
    }
}
