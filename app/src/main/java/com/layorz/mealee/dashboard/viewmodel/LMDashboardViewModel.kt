package com.layorz.mealee.dashboard.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.layorz.mealee.common.LMViewModel
import com.layorz.mealee.network.callbacks.LMApiResponseCallback
import com.layorz.mealee.network.response.LMCommonResponse
import com.layorz.mealee.repository.LMUserRepository

class LMDashboardViewModel(application: Application) : LMViewModel(application) {
    val logoutUser by lazy {
        MutableLiveData<String>()
    }

    fun logoutUser() {
        isProgressShowing.value = true
        LMUserRepository.logout(object : LMApiResponseCallback {
            override fun <T> onSuccess(data: T) {
                isProgressShowing.value = false
                logoutUser.value = (data as? LMCommonResponse)?.message
            }

            override fun <T> onError(errorMessage: T) {
                handleApiResponse(errorMessage as? String)
            }

            override fun onFailure(failureMessage: String?) {
                handleApiResponse(failureMessage)
            }

            override fun onNoNetwork() {
                showNoNetworkToast()
            }
        })
    }
}