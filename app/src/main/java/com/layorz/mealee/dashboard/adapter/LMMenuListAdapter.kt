package com.layorz.mealee.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.layorz.mealee.R
import com.layorz.mealee.common.listener.LMMenuListListener
import com.layorz.mealee.common.model.LMMenuItemData
import kotlinx.android.synthetic.main.lm_item_order_management.view.*

class LMMenuListAdapter(
    private val menuList: MutableList<LMMenuItemData>,
    private val menuClickListener: LMMenuListListener
) : RecyclerView.Adapter<LMMenuListAdapter.LMMenuListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LMMenuListViewHolder =
        LMMenuListViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.lm_item_order_management, parent, false), menuClickListener
        )

    override fun getItemCount() = menuList.size

    override fun onBindViewHolder(holder: LMMenuListViewHolder, position: Int) {
        holder.apply {
            menuList[adapterPosition].apply {
                menuIcon.setImageResource(menuItemIcon)
                mcvMenuItem.setOnClickListener {
                    menuClickListener.menuList(this)
                }
            }
        }
    }

    class LMMenuListViewHolder(
        itemView: View,
        val menuClickListener: LMMenuListListener
    ) : RecyclerView.ViewHolder(itemView) {
        val mcvMenuItem: MaterialCardView = itemView.mcv_menu_item
        val menuIcon: ImageView = itemView.iv_menu_icon
    }
}