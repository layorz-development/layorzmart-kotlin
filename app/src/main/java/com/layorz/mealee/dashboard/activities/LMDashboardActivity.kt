package com.layorz.mealee.dashboard.activities

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.navigation.NavigationView
import com.layorz.mealee.R
import com.layorz.mealee.common.LMActivity
import com.layorz.mealee.common.LMApplication
import com.layorz.mealee.common.callbacks.LMDashboardTitleCallback
import com.layorz.mealee.common.events.LMBackPressEvent
import com.layorz.mealee.common.events.LMLogoutUserEvent
import com.layorz.mealee.common.listener.LMMenuListListener
import com.layorz.mealee.common.model.LMMenuItemData
import com.layorz.mealee.common.utils.LMAlertUtil
import com.layorz.mealee.common.utils.LMConstant
import com.layorz.mealee.dashboard.fragment.LMMenuListFragment
import com.layorz.mealee.dashboard.viewmodel.LMDashboardViewModel
import com.layorz.mealee.menu.fragment.LMMenuCategoriesFragment
import com.layorz.mealee.menu.fragment.LMMenuAvailableFragment
import com.layorz.mealee.menu.fragment.LMMenuTypesFragment
import com.layorz.mealee.order.completeorder.LMCompleteOrderFragment
import com.layorz.mealee.order.fastorder.LMFastOrderFragment
import com.layorz.mealee.other.LMFeedbackFragment
import com.layorz.mealee.other.LMSettingFragment
import com.layorz.mealee.other.LMWebsiteFragment
import com.layorz.mealee.report.allorder.LMAllOrderFragment
import com.layorz.mealee.report.pendingorder.LMPendingOrderFragment
import com.layorz.mealee.report.salesreport.LMSalesReportFragment
import com.layorz.mealee.table.LMCreateUpdateTableFragment
import com.layorz.mealee.table.LMTableFragment
import com.ms_square.etsyblur.BlurSupport
import kotlinx.android.synthetic.main.lm_activity_dashboard.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LMDashboardActivity : LMActivity(), NavigationView.OnNavigationItemSelectedListener,
    LMMenuListListener, LMDashboardTitleCallback {
    private val actionBarDrawer by lazy {
        ActionBarDrawerToggle(this, dl_dashboard, R.string.open, R.string.close)
    }

    private val dashboardViewModel by lazy {
        ViewModelProvider(this).get(LMDashboardViewModel::class.java)
    }

    private val userData by lazy {
        LMApplication.getInstance().getPreference().userData
    }

    private val homeScreenNavigationId by lazy {
        userData?.roles?.let {
            when {
                it.contains(LMConstant.ADMIN) -> R.id.home_menu
                it.contains(LMConstant.CASHIER) -> R.id.complete_order_menu
                it.contains(LMConstant.SERVANT) -> R.id.take_order_menu
                else -> R.id.website_menu
            }
        } ?: R.id.website_menu
    }

    companion object {
        private const val ALERT_DIALOG_LOGOUT = "logout_dialog_tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lm_activity_dashboard)
        setToolbar(getString(R.string.app_name))
        if (savedInstanceState == null) {
            onNavigationItemSelected(
                nav_menu.menu.findItem(homeScreenNavigationId).setChecked(true)
            )
        }
        initView()
        checkUserPermission()
        setListener()
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        dl_dashboard.closeDrawers()
        dl_dashboard.setDrawerLockMode(if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    private fun initView() {
        actionBarDrawer.isDrawerIndicatorEnabled = true
        dl_dashboard.addDrawerListener(actionBarDrawer)
        actionBarDrawer.syncState()
        BlurSupport.addTo(dl_dashboard)
    }

    private fun checkUserPermission() {
        nav_menu.menu.apply {
            userData?.apply {
                roles?.let {
                    val isUserAdmin = it.contains(LMConstant.ADMIN)
                    findItem(R.id.home_menu).isVisible = isUserAdmin
                    findItem(R.id.report_management).isVisible =
                        isUserAdmin
                    findItem(R.id.manage_table_menu).isVisible = isUserAdmin
                }
                permissions?.let {
                    findItem(R.id.feedback_menu).isVisible = it.contains(LMConstant.POST_FEEDBACK)
                    val menuPermission =
                        (it.contains(LMConstant.VIEW_MENU) || it.contains(LMConstant.CREATE_MENU)
                                || it.contains(LMConstant.UPDATE_MENU) || it.contains(LMConstant.DELETE_MENU))
                    val orderCreateDeletePermission =
                        it.contains(LMConstant.CREATE_PENDING_ORDER) || it.contains(LMConstant.UPDATE_PENDING_ORDER)
                    val orderCompletePermission =
                        it.contains(LMConstant.CREATE_ORDER) || it.contains(LMConstant.DELETE_PENDING_ORDER)
                    val orderViewPermission = it.contains(LMConstant.VIEW_ORDER)
                    val filterPermission=it.contains(LMConstant.CREATE_FILTER)||it.contains(LMConstant.UPDATE_FILTER)||it.contains(LMConstant.DELETE_FILTER)
                    findItem(R.id.order_menu).isVisible = menuPermission
                    findItem(R.id.take_order_menu).isVisible = orderCreateDeletePermission
                    findItem(R.id.complete_order_menu).isVisible = orderCompletePermission
                    findItem(R.id.all_order_menu).isVisible = orderViewPermission
                    findItem(R.id.view_orders).isVisible=orderViewPermission
                    findItem(R.id.fast_ordering).isVisible=orderCreateDeletePermission
                    findItem(R.id.manage_types).isVisible=filterPermission
                    findItem(R.id.manage_categories).isVisible=filterPermission
                }
            }
        }
    }

    private fun setListener() {
        nav_menu.setNavigationItemSelectedListener(this)
    }

    private fun observeViewModel() {
        with(dashboardViewModel) {
            isProgressShowing.observe(this@LMDashboardActivity, Observer {
                updateProgressBar(it)
            })
            apiResponseMessage.observe(this@LMDashboardActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMDashboardActivity, it, Toast.LENGTH_SHORT).show()
                }
            })
            apiResponseInId.observe(this@LMDashboardActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMDashboardActivity, getString(it), Toast.LENGTH_SHORT)
                        .show()
                }
            })
            logoutUser.observe(this@LMDashboardActivity, Observer {
                it?.let {
                    Toast.makeText(this@LMDashboardActivity, it, Toast.LENGTH_SHORT).show()
                    EventBus.getDefault().post(LMLogoutUserEvent())
                }
            })
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val fragmentOpen = supportFragmentManager.findFragmentById(R.id.fl_dashboard)
        val navigationFragment = when (item.itemId) {
            R.id.home_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMMenuListFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMMenuListFragment.newInstance()
            }
            R.id.manage_table_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMCreateUpdateTableFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMCreateUpdateTableFragment.newInstance()
            }

            R.id.order_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMMenuAvailableFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMMenuAvailableFragment.newInstance()
            }
            R.id.manage_types->if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMMenuTypesFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMMenuTypesFragment.newInstance()
            }
            R.id.manage_categories->if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMMenuCategoriesFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
               LMMenuCategoriesFragment.newInstance()
            }
            R.id.take_order_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMTableFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMTableFragment.newInstance()
            }
            R.id.fast_order->if(fragmentOpen !=null && fragmentOpen.isVisible && fragmentOpen is LMFastOrderFragment){
                dl_dashboard.closeDrawers()
                null
            }else{
                LMFastOrderFragment.newInstance()
            }
            R.id.view_orders->if(fragmentOpen !=null && fragmentOpen.isVisible && fragmentOpen is LMAllOrderFragment){
                dl_dashboard.closeDrawers()
                null
            }else{
                LMAllOrderFragment.newInstance()
            }
            R.id.pending_order_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMPendingOrderFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMPendingOrderFragment.newInstance()
            }
            R.id.all_order_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMAllOrderFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMAllOrderFragment.newInstance()
            }
            R.id.complete_order_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMCompleteOrderFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMCompleteOrderFragment.newInstance()
            }
            R.id.sales_report_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMSalesReportFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMSalesReportFragment.newInstance()
            }
            R.id.feedback_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMFeedbackFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMFeedbackFragment.newInstance()
            }
            R.id.website_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMWebsiteFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMWebsiteFragment.newInstance()
            }
            R.id.setting_menu -> if (fragmentOpen != null && fragmentOpen.isVisible && fragmentOpen is LMSettingFragment) {
                dl_dashboard.closeDrawers()
                null
            } else {
                LMSettingFragment.newInstance()
            }
            R.id.logout_menu -> {
                LMAlertUtil.showAlertDialog(
                    fragmentActivity = this as FragmentActivity,
                    message = getString(R.string.logout_confirm_desc),
                    isCancelNeeded = true,
                    positiveButtonText = getString(R.string.logout),
                    negativeButtonText = getString(R.string.no),
                    alertKey = ALERT_DIALOG_LOGOUT
                )
                null
            }
            else -> null
        }
        navigationFragment?.let {
            dl_dashboard.closeDrawers()
            replaceCurrent(it, R.id.fl_dashboard, false)
        }
        return true
    }

    override fun onBackPressed() {
        updateProgressBar(false)
        when {
            dl_dashboard.isDrawerVisible(GravityCompat.START) -> {
                dl_dashboard.closeDrawers();
            }
            supportFragmentManager.findFragmentById(R.id.fl_dashboard) !is LMMenuListFragment -> {
                onNavigationItemSelected(
                    nav_menu.menu.findItem(homeScreenNavigationId).setChecked(true)
                )
            }
            else -> {
                super.onBackPressed();
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                dl_dashboard.openDrawer(GravityCompat.START)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun menuList(clickedItem: LMMenuItemData) {
        val navigationFragment = when (clickedItem.menuItemIcon) {
            R.drawable.ic_item_show_menu -> LMMenuAvailableFragment.newInstance()
            R.drawable.ic_item_take_order -> LMTableFragment.newInstance()
            R.drawable.ic_item_pending_order -> LMPendingOrderFragment.newInstance()
            R.drawable.ic_item_complete_order -> LMCompleteOrderFragment.newInstance()
            R.drawable.ic_item_show_order_item -> LMAllOrderFragment.newInstance()
            R.drawable.ic_item_setting_order -> LMSettingFragment.newInstance()
            else -> null
        }
        if (navigationFragment != null) {
            replaceCurrent(navigationFragment, R.id.fl_dashboard, false)
        } else {
            Toast.makeText(this, getString(R.string.yet_to_implement), Toast.LENGTH_LONG).show()
        }
    }

    override fun dashBoardTitle(dashBoardName: String) {
        //tv_dashboard_header.text = dashBoardName
    }

    override fun onPositiveButtonClick(alertKey: String?) {
        super.onPositiveButtonClick(alertKey)
        if (alertKey == ALERT_DIALOG_LOGOUT) {
            dashboardViewModel.logoutUser()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.findFragmentById(R.id.fl_dashboard)
            ?.onActivityResult(requestCode, resultCode, data)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun backPress(backPress: LMBackPressEvent) {
        onBackPressed()
    }
}